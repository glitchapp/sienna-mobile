# Sienna (forked from https://github.com/SimonLarsen/sienna)

A fork of a game by Simon Larsen.

New assets:

<img src="screenshots/screen1.avif" width=100% height=50%>
<img src="screenshots/newsc4.avif" width=100% height=50%>

Multiplayer up to 4 players:
<img src="screenshots/multiplayer.avif" width=100% height=50%>
<img src="screenshots/multiplayer2.avif" width=100% height=50%>

New shading and animations:

<img src="screenshots/newsc5.avif" width=60% height=50%>

- New in game animated entities:

<img src="screenshots/newsc6.avif" width=60% height=50%>

- New dynamic illumination:

<img src="screenshots/newsc7.avif" width=100% height=50%>
<img src="screenshots/newsc2.avif" width=100% height=50%>
<img src="screenshots/newsc3.avif" width=100% height=50%>

- Portrait and landscape mode for mobile phnes:
<img src="screenshots/newsc10.avif" width=50% height=50%>

This fork is aimed at mobiles and adds many tweaks and features aimed at Sailflsh os and mobile devices, if you intend to play this game on a desktop please use this repository instead: https://codeberg.org/glitchapp/Sienna

Added to the original game:

- Added gamepad support (navigate with thumbstick, jump with 'a'.
- Added landscape mode (recommended) and automatic device orientation detection for Sailfish Os
- Added new set of assets and backgrounds
- Added parallax effect
- Increased draw range width and height so that the tiles generation is not visible on modern mobile screen resolutions
- Added vertical parallax movement
- Added menu option to select set of assets (new and classic mode available)
- Coordinates invertion for the swipe gestures in landscape mode allowing to navigate the menu by swiping up and down as expected.

Soundtrack created with music by Rugar ("A Scent of Europe") and Centurion of war (https://opengameart.org/users/centurionofwar)

Track list:
A Scent of Europe’ by Rugar (licensed under the CC BY-NC-ND 3.0 license.)

Centurion_of_war:
push_ahead 					https://opengameart.org/content/pushing-ahead#comment-form
Upbeat Beat 				https://opengameart.org/content/upbeat-beat
Unseen 						https://opengameart.org/content/unseen
Midnight Rush				https://opengameart.org/sites/default/files/80s_midnight_rush_0.ogg
A wish to fulfill (Updated)	https://opengameart.org/content/a-wish-to-fulfill-updated
on_your_toes				https://opengameart.org/content/on-your-toes
A Path Which leads to somewhere	https://opengameart.org/content/a-path-which-leads-to-somewhere
Hail the Arbiter			https://opengameart.org/content/hail-the-arbiter
I am not Trash, or am I?	https://opengameart.org/content/i-am-not-trash-or-am-i


If you don't like the new set of assets or music tracks or if you experience any performance issues due to the parallax effects and new features, you can now select classic assets which sets the game (almost) like the original developers and graphic artist designed.



Sienna is a simple, fast-paced one-button platformer.

## Controls ##

* Press 'space' to jump or wall jump when touching a wall.
  The longer you hold the button the higher you jump.
  This is crucial to the gameplay and it will often be necessary
  to press the button for only a splitsecond.
* Press 'R' to restart from last checkpoint.
* Press 'return' to restart from the beginning of the level.

## Screenshots ##
![Walljumping](http://i.imgur.com/e5BDO.png)
![Snake](http://i.imgur.com/cJHCW.png)
![Temple](http://i.imgur.com/jf6ib.png)

Extra assets credits:

Bat sprites: Unknown_Chan https://opengameart.org/content/2d-bat-sprites license: CC-BY-SA 3.0

Bat screeches.

Licensing:
  - Creative Commons Zero (CC0)

Source:
  - By polymorpheva: https://freesound.org/s/104205/
