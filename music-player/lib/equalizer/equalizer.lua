function equalizerload(song)

	
	--love.graphics.setCaption('loveEQ')
	sound = love.sound.newSoundData(song)
	rate = sound:getSampleRate()
    channels = sound:getChannels()
	--print(rate)
    --print(channels)
	
	music = love.audio.newSource(sound)
	music:play()
	
	spectrum = {}
	gTime = 0
	
	--love.graphics.setLine(1,'smooth')
end

function equalizerupdate(dt)

	local curSample = music:tell('samples')
	--local curSample = 1
	local wave = {}
	local size = next_possible_size(2048)
	
	--local sound=music
	
    if channels == 2 then
        for i=curSample, (curSample+(size-1) / 2) do
            local sample = (sound:getSample(i * 2) + sound:getSample(i * 2 + 1)) * 0.5
            table.insert(wave,complex.new(sample,0))
        end
    else
        for i=curSample, curSample+(size-1) do
            table.insert(wave,complex.new(sound:getSample(i),0))
        end
    end
    
	local spec = fft(wave,false)
	--local reconstructed = fft(spec,true)
	
	
	function divide(list, factor)
		for i,v in ipairs(list) do
			list[i] = list[i] / factor
		end
	end
	
	--divide(reconstructed,size)
	divide(spec,size/2)
	
	spectrum = spec
end

function equalizerdraw()
	local division = 10
	for i=1, #spectrum/division do
		local height = love.graphics.getHeight()
		local width = love.graphics.getWidth()
		local dist = width / (#spectrum/division)
		
		local v = spectrum[i]
		local n = height * 2 * v:abs(),0
		--if n>50 then
        love.graphics.setColor(1,0.5,0.5,0.6)
		love.graphics.rectangle(
			'line',
			(i-1)*dist*1.5,height/1 - n,
			20,n
		)

	
		
		love.graphics.setColor(1,0.5,0.5,0.3)
		love.graphics.rectangle(
			'fill',
			(i-1)*dist*1.5,height/1 - n,
			20,n
		)
		
		
		love.graphics.setColor(1,1,1,1)

	end
end

function limit_max(v,limit)
	if v > limit then
		return limit
	end
	return v
end

function limit_min(v,limit)
	if v < limit then
		return limit
	end
	return v
end
