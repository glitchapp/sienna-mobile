
-- Add parent directory to package.path so require() can find the module
package.path = package.path .. ";../?.lua"


local lovebpm = require("music-player/lib/lovebpm/lovebpm")


function lovebpmload(trackname)

  love.audio.setVolume(.6)
  --font = love.graphics.newFont(12)
  paused = false
  pulse = 0
  -- Init new Track object and start playing
  music = lovebpm.newTrack()
    :load(trackname)
    
    :setBPM(mybpm)
    
    :setLooping(true)
    :on("beat", function(n)
      --local r, g, b = math.random(255), math.random(255), math.random(255)
      --love.graphics.setBackgroundColor(r, g, b)
      pulse = 1
    end)
   --music:on("beat", function(n) print("beat:", n) end)
    :play()
    
end


function lovebpmupdate(dt)
  music:update()
  pulse = math.max(0, pulse - dt)
end


function lovebpmkeypressed(k)
  if k == "space" then
    -- Toggle pause
    paused = not paused
    if paused then
      music:pause()
    else
      music:play()
    end
  end
end


function lovebpmdraw()
  local w, h = love.graphics.getDimensions()

  -- Draw circle
  local radius = 80 + pulse ^ 3 * 20
  love.graphics.setLineWidth(8)
  --love.graphics.setColor(255, 255, 255, 255 * 0.3)
  --love.graphics.circle("line", w / 2, h / 2, radius)

  -- Get current beat and subbeat with 4x multiplier
  beat, subbeat = music:getBeat()
 
  --print(subbeat)
--music:on("beat", function(n) print("beat:", n)
--love.graphics.setBackgroundColor(beat, subbeat, .75)
--print("beat" .. beat)
--print("subbeat" .. subbeat)
 --end)

  -- Draw 4x subbeat progress arc
  local angle1 = -math.pi / 2
  local angle2 = math.pi * 2 * subbeat - math.pi / 2
  --love.graphics.setColor(255, 255, 255)
  --love.graphics.setColor(0, 255, 0)
  --love.graphics.arc("line", "open", w / 3, h / 10, radius, angle1, angle2)

  -- Get current beat and subbeat
  --local beat, subbeat = music:getBeat()

  -- Draw current beat number
  --love.graphics.setFont(font)
  --love.graphics.printf(beat, 0, h / 2 - 30, w, "center")
  --print("beat" .. beat)
  --print("subbeat" .. subbeat)
end
