
--[[
 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

Description / resume of the functionality of each function written with assistance from ChatGPT

--]]

--[[
The function musicload() initializes various variables and loads several assets related to music.

    The playButton variable defines a button object with text, position, color, font, and other properties.
    The locksymbol variable loads an image file for a lock symbol.
    The function loads several modules related to equalizer, including equalizer.lua, luafft.lua, and complex.lua.
    The function creates two fonts, font1 and font3, with sizes 12 and 36 (3 times bigger) respectively.
    The function initializes several variables related to a CD player and its position, including cdspeed, cdplaying, cdangle, trackspositionx, trackspositiony, bcspace, author, currenttrackname, and currenttrackdesc.
    The function loads several image files related to music, including cdasset.webp and covercd.webp.
    The function initializes several string variables that are likely used as credits for the music tracks.
--]]

function musicload()

local desktopWidth, desktopHeight = love.window.getDesktopDimensions()

-- Load music equalizer
	require ("music-player/lib/equalizer/equalizer")
	require ("music-player/lib/equalizer/luafft")
	require ("music-player/lib/equalizer/complex")



locksymbol = love.graphics.newImage("music-player/assets/locki.png")
SwitchONIcon = love.graphics.newImage("music-player/assets/SwitchON160.png")
SwitchOFFIcon = love.graphics.newImage("music-player/assets/SwitchOFF160.png")

	require ("music-player/lib/equalizer/equalizer")
	require ("music-player/lib/equalizer/luafft")
	require ("music-player/lib/equalizer/complex")
	--equalizerload()
font1 = love.graphics.newFont(12) -- standard font
font2 = love.graphics.newFont(12*2) -- two times bigger
font3 = love.graphics.newFont(12*3) -- three times bigger


cdspeed = 32
cdplaying=false
cdangle = 0
trackspositionx=1000
trackspositiony=200
bcspace=80
author="none"
currenttrackname=""
currenttrackdesc=""
MyScene=6
DemoSceneOn=true


--[[The first button is a "Return" button, with the text "Return" displayed on it. It is located at coordinates (1000, 100) on the screen and has a rotation value of 0 degrees. Its size is not explicitly defined, but its sx (scale x) value is set to 1. It has not been hovered over by the mouse, so its "hovered" attribute is set to false. Its default color is white, with RGB values of (1, 1, 1), and its color when hovered over is yellowish-green, with RGB values of (1, 1, 0). It uses a font called "poorfishmiddle".

Based on the information provided in the descriptions, here is what I believe the purpose of each button to be:

    ReturnButton: This button likely serves as a way for the user to go back to a previous screen or menu.
    stopbutton: This button likely serves as a way for the user to stop a process or action.
    synthwave421kb: This button likely represents a track or audio option called "Calm Relax 1 (Synthwave_421k)" that the user can select to listen to.
    synthwave4kb: This button likely represents a track or audio option called "Calm Ambient 1 (Synthwave 4k)" that the user can select to listen to.
    aquariab: This button likely represents a track or audio option called "Aquaria" that the user can select to listen to.
--]]
--controls

playButton = {
	text = "play",
	x = desktopWidth/3,
	y = (desktopHeight/3)+200, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}


musicbackwardbutton = {
	text = "<<",
	x = (desktopWidth/3)-100,
	y = (desktopHeight/3)+450, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

musicstopbutton = {
	text = "stop",
	x = (desktopWidth/3),
	y = (desktopHeight/3)+450, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

musicforwardbutton = {
	text = ">>",
	x = (desktopWidth/3)+200,
	y = (desktopHeight/3)+450, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

volumedownbutton = {
	text = "-",
	x = (desktopWidth/3)+700,
	y = (desktopHeight/3)+400, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

volumeupbutton = {
	text = "+",
	x = (desktopWidth/3)+700,
	y = (desktopHeight/3)+300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

classictracksb = {
	text = "Classic tracks",
	x = trackspositionx-bcspace*5,
	y = trackspositiony+bcspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

PreviousDemoscene = {
text = "<<",
	x = (desktopWidth/3)-100,
	y = (desktopHeight/3)+550, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

NextDemoscene = {
text = ">>",
	x = (desktopWidth/3)+250,
	y = (desktopHeight/3)+550, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

ToggleDemoScene = {

	text = "",
	x = (desktopWidth/3)+350,
	y = (desktopHeight/3)+550, 
	r = 0,
	sx = 1,
	image = SwitchONIcon,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

	

timer=0


end

--The musicupdate(dt) function is used to update the state of the music player.
--If cdplaying is true, it will rotate the CD by decreasing the cdangle value based on cdspeed multiplied by the dt parameter which represents the time elapsed since the last update. If cdangle becomes greater than 360, it is reset to 0.
function musicupdate(dt)
		if startTimer==true then
			timer=timer+dt
		end
		volume = love.audio.getVolume(music)
		
		if timer>0.5 and not (ext==".sunvox") then
			cdplaying=true
		end
		if cdplaying == true then 
			equalizerupdate(dt)
        end
end

function love.filedropped(file)	
	filename = file:getFilename()
	ext = filename:match("%.%w+$")

if not (filename==nil) then	--make sure there's a track to play
	if ext == ".ogg" 	--audio formats 
	or ext == ".mp3"
	or ext == ".wav"
	or ext == ".flac "
	or ext == ".oga "
	or ext == ".ogv" 
	or ext == ".flac"
	then
		format="audio"
		if music then
			music:stop()
		end
		file:open("r")
		fileData = file:read("data")
		music=fileData
		cdplaying=true
		
		equalizerload(music)
		music:setVolume(1)
	end
	
	if ext == ".abc" or ext == ".mid" or ext == ".pat"	-- midi / notation formats
	then
		format="midi"
		if music then
			music:stop()
		end
		file:open("r")
		fileData = file:read("data")
		music=fileData
		
		equalizerload(music)
		music:setVolume(0.5)
	end
	
	--[[
	if ext == ".699" or ext == ".amf" or ext == ".dbm"	--tracker formats supported by love2d
		or ext == ".dmf" or ext == ".dsm" or ext == ".far"
		or ext == ".it" or ext == ".j2b" or ext == ".mdl"
		or ext == ".med" or ext == ".mod" or ext == ".mt2"
		or ext == ".mtm" or ext == ".okt" or ext == ".psm"
		or ext == ".s3m" or ext == ".stm" or ext == ".ult"
		or ext == ".ums" or ext == ".xm" 
	then
			if music then
				music:stop()
			end
			loadOpenMPT(filename)
			format="tracker"

	end
	--]]
	
	--tracker formats supported by openMPT
	if ext == ".669"
		or ext == ".amf"
		or ext == ".ams"
		or ext == ".c67"
		or ext == ".dbm"
		or ext == ".digi"
		or ext == ".dmf"
		or ext == ".dsm"
		or ext == ".dsym"
		or ext == ".dtm"
		or ext == ".far"
		or ext == ".fmt"
		or ext == ".imf"
		or ext == ".ice"
		or ext == ".j2b"
		or ext == ".m15"
		or ext == ".mdl"
		or ext == ".med"
		or ext == ".mms"
		or ext == ".mt2"
		or ext == ".mtm"
		or ext == ".mus"
		or ext == ".nst"
		or ext == ".okt"
		or ext == ".plm"
		or ext == ".psm"
		or ext == ".pt36"
		or ext == ".ptm"
		or ext == ".sfx"
		or ext == ".sfx2"
		or ext == ".st26"
		or ext == ".stk"
		or ext == ".stm"
		or ext == ".stx"
		or ext == ".stp"
		or ext == ".symmod"
		or ext == ".ult"
		or ext == ".wow"
		or ext == ".gdm"
		or ext == ".mo3"
		or ext == ".oxm"
		or ext == ".umx"
		or ext == ".xpk"
		or ext == ".ppm"
		or ext == ".mmcmp"
		or ext == ".mptm"
		or ext == ".mod"
		or ext == ".s3m"
		or ext == ".xm"
		or ext == ".it"
		then
			if music then
				music:stop()
			end
			cdplaying=false
			loadOpenMPT(filename)
			format="tracker"
			OldSchoolLoad()
		end
		
	if ext == ".sunvox" 									-- sunvox format
	then
			if music then
				music:stop()
			end
			format="sunvox"
			cdplaying=false
			mv.init() -- required at startup!
			player = assert(mv.newPlayer(music))
			player:setAutostop(true) -- songs play in loop by default
			player:play()
	end
	
	
	
	--[[
if ext == ".m3u" then
    format = "m3u"
    if music then
        music:stop()
    end

    -- Read the M3U playlist file and extract the first track
    file:open("r")
    local fileData = ""

    for line in file:lines() do
        fileData = fileData .. line .. "\n"
    end

    -- Split the playlist into lines
    local lines = {}
    for line in fileData:gmatch("[^\r\n]+") do
        table.insert(lines, line)
    end

    -- Check if there is at least one track in the playlist
    if #lines > 0 then
        -- Get the first track from the playlist
        local firstTrack = lines[1]

        -- Load and play the first track
        music = love.audio.newSource(firstTrack, "stream")
        music:setVolume(0.5)
        music:play()

        equalizerload(music)

        startTimer = true
    end
end
--]]
end
end


--The isButtonHovered(button) function takes a button object as an argument and returns true if the mouse pointer is currently hovering over the button. The function calculates the size of the button based on the font used, the sx and sy scaling factors, and the text of the button. It then checks if the mouse coordinates are within the bounds of the button, and returns true if they are.
function isButtonHovered (button)
	local font = button.font or love.graphics.getFont( )
	local width = font:getWidth(button.text)
	local height = font:getHeight( )
	local sx, sy = button.sx or 1, button.sy or button.sx or 1
	local x, y = button.x, button.y
	local w, h = width*sx, height*sy
	local mx, my = love.mouse.getPosition()
	if mx >= x and mx <= x+w
		and my >= y and my <= y+h then
		button.w, button.h = w, h
		return true
	end
	if love.mouse.isDown(1) then timer=0 end	-- restart timer counter if mouse click pressed
	return false
end

--The drawButton(button, hovered) function is used to draw a button to the screen. It takes a button object and a hovered boolean value as arguments. If hovered is true, it will set the color of the button to button.hoveredColor and draw a rectangle around it. Otherwise, it will use button.color as the color. The function then sets the font, prints the button text at the specified position, and applies the rotation and scaling specified by the button.r, button.sx, and button.sy parameters.
function drawButton (button, hovered)
	
	--love.graphics.setFont( button.font )
	if hovered then
		love.graphics.setColor(button.hoveredColor)
		love.graphics.rectangle ('line', button.x, button.y, button.w, button.h)
	else
		love.graphics.setColor(button.color)
	end
	
	-- If the button is an image
     if button.image then
        local x, y, width, height = button.x, button.y, button.image:getWidth(), button.image:getHeight()
		if hovered then									--If the button is being hovered over, 
			love.graphics.setColor(1,0.5,0.5,1)
		end
        -- Calculate the position for the text based on textPosition
        if textPosition == "top" then
            y = y - height / 2  -- Adjust the y-coordinate to be at the top of the image button
        elseif textPosition == "bottom" then
            y = y + height / 2  -- Adjust the y-coordinate to be at the bottom of the image button
        end

        -- Draw the image button
        love.graphics.draw(button.image, x, y, button.r, button.sx)
    else
        -- Handle regular buttons without images here
        love.graphics.print(button.text, button.x, button.y, button.r, button.sx)
    end

	love.graphics.print(button.text,button.x,button.y,button.r,button.sx)	--Prints the specified text on the button at the position specified by button.x and button.y with the specified rotation button.r and scale button.sx.
		
end


local desktopWidth, desktopHeight = love.window.getDesktopDimensions()
function musicdraw(dt)


	-- Music player controls
	local hovered = isButtonHovered (musicbackwardbutton)
	drawButton (musicbackwardbutton, hovered)
	if hovered and love.mouse.isDown(1) and cdplaying==true and not(aboutInfo==true)then
		if not (format=="tracker") then
			musicposition = music:tell( "seconds" )
			music:seek( musicposition-5, "seconds" )
		end
	love.timer.sleep( 0.2 )
	elseif hovered and love.mouse.isDown(1) and not(aboutInfo==true) then
		if format=="tracker" then
			modPosition=mod:get_position_seconds()
			mod:set_position_seconds(modPosition-1)
		love.timer.sleep( 0.2 )
		end
		
	end
	
	local hovered = isButtonHovered (musicstopbutton)
	drawButton (musicstopbutton, hovered)
	if hovered and love.mouse.isDown(1) and not (format=="tracker") and not(aboutInfo==true) then
		love.audio.stop()
		if cdplaying==true then
			cdplaying=false
		end
			TEsound.play("sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
			currenttrackname=""
			currenttrackdesc=""
			rate=0
			channels=0
			love.timer.sleep( 0.3 )
	elseif hovered and love.mouse.isDown(1) and format=="tracker" and not(aboutInfo==true) then
		
	end
	
	if cdplaying==true and gamestatus=="music" then
	love.graphics.setFont(poorfishsmall)
		if res=="1080p" then
			-- Track position
			love.graphics.rectangle( "fill", 500, 800, music:tell( "seconds" )*2, 25 )
			love.graphics.rectangle( "line", 500, 795, music:getDuration( "seconds" )*2, 35 )
			love.graphics.print("Playing: " .. math.floor(music:tell( "seconds" )/ 60) .. " : " .. math.floor(music:tell( "seconds" )% 60),300,900,0, 0.8, 1) 
			love.graphics.print("Duration: " .. math.floor(music:getDuration( "seconds" )/ 60) .. " : " .. math.floor(music:getDuration( "seconds" )% 60),900,900,0, 0.8, 1) 
		else
			if ext==nil then ext="Unknown" end
			-- Track position
			love.graphics.rectangle( "fill", (desktopWidth/3)-100,(desktopHeight/3)+400, music:tell( "seconds" )*2, 25 )
			love.graphics.rectangle( "line", (desktopWidth/3)-100,(desktopHeight/3)+400, music:getDuration( "seconds" )*2, 35 )
			love.graphics.print("Format: " .. ext,(desktopWidth/3)+200,(desktopHeight/3)+450,0, 0.8, 1) 
			love.graphics.print("Playing: " .. math.floor(music:tell( "seconds" )/ 60) .. " : " .. math.floor(music:tell( "seconds" )% 60),(desktopWidth/3)-100,(desktopHeight/3)+500,0, 0.8, 1) 
			love.graphics.print("Duration: " .. math.floor(music:getDuration( "seconds" )/ 60) .. " : " .. math.floor(music:getDuration( "seconds" )% 60),(desktopWidth/3)+200,(desktopHeight/3)+500,0, 0.8, 1) 

			
			--drawequalizer()			-- equalizer and volume controls are in musicequalizer.lua
			drawvolumecontrols()	
		end
		
	end
	
	local hovered = isButtonHovered (musicforwardbutton)
	drawButton (musicforwardbutton, hovered)
	if hovered and love.mouse.isDown(1) and cdplaying==true and not(aboutInfo==true) then
		if not (format=="tracker") then
			musicposition = music:tell( "seconds" )
			music:seek( musicposition+5, "seconds" )
			love.timer.sleep( 0.2 )
		end
	elseif hovered and love.mouse.isDown(1) and not(aboutInfo==true) then
			if format=="tracker" then
				modPosition=mod:get_position_seconds()
				mod:set_position_seconds(modPosition+1)
			love.timer.sleep( 0.2 )
		end
		
	end
	
	local hovered = isButtonHovered (PreviousDemoscene)
	drawButton (PreviousDemoscene, hovered)
	if hovered and love.mouse.isDown(1) and not(aboutInfo==true) then
		if format=="tracker" then
				if not (MyScene<2) then
					MyScene=MyScene-1
					SetScene(MyScene)
					love.timer.sleep( 0.2 )
				end
		end
	end
	love.graphics.print("Demoscene", (desktopWidth/3), (desktopHeight/3)+550 )
	
	local hovered = isButtonHovered (NextDemoscene)
	drawButton (NextDemoscene, hovered)
	if hovered and love.mouse.isDown(1) and not(aboutInfo==true) then
		if format=="tracker" then
			if not (MyScene>5) then
				MyScene=MyScene+1
				SetScene(MyScene)
				love.timer.sleep( 0.2 )
			end
		end
	end
	
		local hovered = hoveredImage  (ToggleDemoScene)
	drawButtonImage (ToggleDemoScene, hovered)
	if hovered and love.mouse.isDown(1) and not(aboutInfo==true) then
		if DemoSceneOn==true then
				DemoSceneOn=false
				ToggleDemoScene.image=SwitchOFFIcon
				love.timer.sleep( 0.2 )
				
		elseif DemoSceneOn==false then
				DemoSceneOn=true
				ToggleDemoScene.image=SwitchONIcon
				love.timer.sleep( 0.2 )
				
		end
		
	end
	
	
	-- Volume controls
	local hovered = isButtonHovered (volumedownbutton)
	drawButton (volumedownbutton, hovered)
	if hovered and love.mouse.isDown(1) and cdplaying==true and not(aboutInfo==true) then
		volume=volume-0.1
		love.audio.setVolume( volume )
	love.timer.sleep( 0.2 )
	end
	
	-- Volume controls
	local hovered = isButtonHovered (volumeupbutton)
	drawButton (volumeupbutton, hovered)
	if hovered and love.mouse.isDown(1) and cdplaying==true and not(aboutInfo==true) then
		volume=volume+0.1
		love.audio.setVolume( volume )
	love.timer.sleep( 0.2 )
	end
	--[[
	local hovered = hoveredImage (AboutButton)
	drawButtonImage (AboutButton, hovered)
	if hovered and (love.mouse.isDown(1)) and not(aboutInfo==true) then 
		aboutInfo=true
		
	elseif hovered then
		love.graphics.print("About music player", AboutButton.x,AboutButton.y+100,0,1)
	end
	--]]
	if aboutInfo==true then
		
	end
end


