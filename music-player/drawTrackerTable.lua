


function loadTrackerTable()

local desktopWidth, desktopHeight = love.window.getDesktopDimensions()

-- Initialize a table to store the history of played rows
local playedRows = {}
    

gridX = 50
gridY = (desktopHeight / 3) + 200

	modChannelCount = mod:get_num_channels()

	-- Define your fonts (font2 and font3) here if not already defined
trackerRow = 0
trackerSpeed = 30 -- Adjust the speed of the tracker's movement
numRows = 10 -- Number of rows to display in the grid

end

function drawTrackerTable()


-- Calculate the cell width based on the number of channels (modChannelCount)

if modChannelCount>10 then
	cellWidth = (love.graphics.getWidth() - gridX) / modChannelCount
else cellWidth=60
end
cellHeight = 20 




-- Calculate the total number of rows in the pattern
local totalRows = mod:get_pattern_num_rows(mod:get_current_pattern())

-- Calculate the starting row for the grid
local startRow = trackerRow - math.floor(numRows / 2)
if startRow < 0 then
    startRow = 0
end

-- Calculate the ending row for the grid
local endRow = startRow + numRows - 1
if endRow >= totalRows then
    endRow = totalRows - 1
    startRow = endRow - numRows + 1
end

-- Calculate the position of the tracker based on time and speed
trackerRow = mod:get_current_row()

-- Offset for scrolling rows
local rowOffset = 0
if trackerRow >= 10 then
    rowOffset = trackerRow - 10
    trackerRow = 10 -- Limit tracker row to 10
end

-- Adjust trackerRow for displaying filled rows at the bottom
trackerRow = trackerRow + rowOffset

for row = 0, numRows - 1 do
    for col = 0, modChannelCount - 1 do -- Display 16 columns
        local x = gridX + col * cellWidth
        local y = gridY + row * cellHeight

        -- Calculate the displayed row number with offset
        local displayedRow = row + rowOffset + 1

        -- Draw row numbers at the left side of the grid
        love.graphics.print(string.format("%02d", displayedRow), gridX - 30, y)

        -- Check if the current column (channel) matches the playing channel
        local isChannelPlaying = col + 1 == mod:get_current_playing_channels()

        -- Set the fill color based on whether the channel is playing
        if displayedRow == trackerRow and isChannelPlaying then
            love.graphics.setColor(1, 0, 0, 1) -- Red color for playing channel
            love.graphics.rectangle("fill", x, y, cellWidth, cellHeight)
        elseif displayedRow == trackerRow then
            love.graphics.setColor(1, 1, 1, 1) -- White color for non-playing channel
            love.graphics.rectangle("fill", x, y, cellWidth, cellHeight)
        else
            love.graphics.setColor(0.5, 0.5, 0.5, 1)
            love.graphics.rectangle("line", x, y, cellWidth, cellHeight)
        end
    
    
    end
end

-- Display the history of played rows
for _, playedRow in ipairs(playedRows) do

    local x = gridX + playedRow.col * cellWidth
    local y = gridY + (playedRow.row - rowOffset - 1) * cellHeight

    love.graphics.setColor(0, 1, 0, 0.5) -- Green color for played cells
    love.graphics.rectangle("fill", x, y, cellWidth, cellHeight)
end
end
