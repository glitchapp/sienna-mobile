

--[[
 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

loveInputFieldDraw()
This tool contains code from the following repository:
https://github.com/konsumer/lua-openmpt
Libopenmpt ffi wrapper for loading & playing mod-music file in luajit or love2d. 

On linux install required libraries with: sudo apt install luajit libopenmpt-dev

This tool also requires libwebp libraries installed:

On Linux install them with: sudo apt install love libwebp-dev

on windows Windows you need to install love-webp libraries from https://github.com/ImagicTheCat/love-webp/tree/master/dist
Copy the files inside win64 / win 32 to the folder where love is installed / where binaries are (if you installed love you'll usually find the folder at "C:/programs/love/"

The license text can be found in GPL-2.0.txt.

--]]

function love.keypressed(key, scancode, isrepeat)
	if key == "escape" or key =="q" then
			love.event.quit()
	end
end

function MusicPlayerLoad()

    -- Variables
    cdplaying = false
    gamestatus = "music"
    musicison = true
    author = nil
    currentTrack = nil
    musicFiles = {}

	--screen set up
	--love.window.setMode(1920, 1080, {resizable=true, borderless=false})

	local ddwidth, ddheight = love.window.getDesktopDimensions( display )
	width, height = love.graphics.getDimensions( )
	

	touchX, touchY = 0, 0
	-- Initialize the buttons table
	buttons = {}
	
	--variables
	cdplaying=false
	gamestatus="music"
	musicison=true
	author=nil
	
	--fonts

	poorfish = love.graphics.newFont("music-player//fonts/PoorFish/PoorFish-Regular.otf", 64) -- Thanks @hicchicc for the font
	poorfishmiddle = love.graphics.newFont("music-player//fonts/PoorFish/PoorFish-Regular.otf", 32) -- Thanks @hicchicc for the font
	poorfishsmall = love.graphics.newFont("music-player//fonts/PoorFish/PoorFish-Regular.otf", 16) -- Thanks @hicchicc for the font
 love.graphics.setFont(poorfish)
	
	--load sound assets:
	
	gui21 = love.audio.newSource( "music-player/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static" )
	gui24 = love.audio.newSource( "music-player/sounds/GUI_Sound_Effects/GUI_Sound_Effects_024.ogg","static" )
	gui29 = love.audio.newSource( "music-player/sounds/GUI_Sound_Effects/GUI_Sound_Effects_029.ogg","static" )
	gui30 = love.audio.newSource( "music-player/sounds/GUI_Sound_Effects/GUI_Sound_Effects_030.ogg","static" )
	
	--require ("About")
	--loadAbout()
	require ("music-player/drawButtonImage")
	
	-- Load external libraries
	
	tesound = require "music-player/lib/tesound"
	--local openmpt = require "openmpt"
	local lovebpm = require "music-player/lib/lovebpm/main"
	local equalizer = require "music-player/lib/equalizer/equalizer"
	local luafft = require "music-player/lib/equalizer/luafft"
	local complex = require "music-player/lib/equalizer/complex"
	

musicplayer = require ('music-player/musicplayer')		-- Load music player
musicload()
musicequalizer = require ('music-player/musicequalizer')		-- Load music player
musicequalizerload()
require ("music-player/lib/lovebpm/main")

dropMusicIcon = love.graphics.newImage("music-player/assets/dropMusicIcon.png")


    -- Load music files
    loadMusicFiles("music-player/music")
end


function loadMusicFiles(path)
    local supportedFormats = {".mp3", ".ogg", ".wav", ".mod", ".xm", ".s3m", ".it"}
    local items = love.filesystem.getDirectoryItems(path)
    
    for _, file in ipairs(items) do
        local fullPath = path .. "/" .. file
        local fileInfo = love.filesystem.getInfo(fullPath)
        
        if fileInfo and fileInfo.type == "file" then
            for _, ext in ipairs(supportedFormats) do
                if fullPath:sub(-#ext) == ext then
                    table.insert(musicFiles, fullPath)
                    -- Create button for this music track
                    createMusicButton(file, fullPath)
                end
            end
        end
    end
end

function createMusicButton(fileName, filePath)
    local button = {
        text = fileName,  -- Use file name as button text
        x = 20,  -- Adjust x position as needed
        y = #musicFiles * 100 + 260,  -- Adjust y position based on number of files
        action = function() playMusic(filePath) end  -- Set action to play music
    }
    table.insert(buttons, button)  -- Add button to the buttons table
end



--[[
		StartSongFile="music/invention4-1loop.ogg"
		format="audio"
		cdplaying=true
		equalizerload(StartSongFile)
		ext = ".ogg"
		--StartSongFile:setVolume(1)
		
--equalizerload("music/invention4-1loop.ogg")
--]]

x, y = 0 , 0
function MusicPlayerUpdate(dt)
    --mouseX, mouseY = love.mouse.getPosition()

    
    hoverIndex = nil
	
    for i, _ in ipairs(musicFiles) do
        --if mouseY >= 60 + i * 100 and mouseY <= 160 + i * 100 then
         if y >= 0 + i * 100 and y <= 20 + i * 100 then
            hoverIndex = i
            break
        end
    end

    if format == "tracker" then
        updateOpenMPT()
    else
		if cdplaying==true then
			musicupdate(dt)
		end
    end
end

-- this will play a mod file
-- run with love ./demo_love

--local OpenMPT = require "openmpt"

local bitDepth = 16
local samplingRate = 44100
local channelCount = 2
local bufferSize = 1024
local pointer = 0
local modChannelCount = 0
local modInstCount = 0
local modLength = 0
local modPosition = 0
local modTitle = ""
local modArtist = ""

mod=nil

function loadOpenMPT(filename)

	--debug statement:
	print("Filename:", filename) -- Check the value of filename
    
 mod = openmpt:new(filename)
    modChannelCount = mod:get_num_channels()
    modInstCount = mod:get_num_instruments()
    modLength = mod:get_duration_seconds()
    modTitle = mod:get_metadata("title")
    if modTitle == "" then modTitle = "Untitled" end
    modArtist = mod:get_metadata("artist")
    if modArtist == "" then modArtist = "Unknown" end
    love.window.setTitle(modTitle)

    sd = love.sound.newSoundData(bufferSize, samplingRate, bitDepth, channelCount)
    qs = love.audio.newQueueableSource(samplingRate, bitDepth, channelCount)

    require('drawTrackerSong')
    loadTrackerSong(mod)
  
end


function updateOpenMPT()

  -- pump audio from mod into buffer
  if qs:getFreeBufferCount() == 0 then return end
  mod:read_interleaved_stereo(samplingRate, bufferSize, sd:getFFIPointer())
  qs:queue(sd)
  qs:play()
  
  -- loop song
  if mod:get_position_seconds() >= modLength then
    mod:set_position_seconds(0)
  end
  
end


 
function MusicPlayerDraw(dt)
    love.graphics.print("Music Files:", 20, 20)
    for i, file in ipairs(musicFiles) do
        if i == hoverIndex then
            love.graphics.setColor(0.5, 0.5, 1)  -- Highlight color
        else
            love.graphics.setColor(1, 1, 1)    -- Default color
        end
        love.graphics.print(file, 20, 60 + i * 100)
    end

    love.graphics.setColor(1, 1, 1)  -- Reset to default color

     for i, file in ipairs(musicFiles) do
        local rectY = 60 + i * 100
        love.graphics.rectangle("line", 20, rectY, 1400, 100)  -- Adjust rectangle position and size as needed
        
        -- Highlight the rectangle if it's being hovered over
        if hoverIndex == i then
            love.graphics.setColor(0.5, 0.5, 1)
            love.graphics.rectangle("fill", 20, rectY, 1400, 100)
        end
        
        -- Reset color
        love.graphics.setColor(1, 1, 1)
        
        -- Draw file name
        love.graphics.print(file, 20, rectY)
    end

    if currentTrack then
        love.graphics.print("Now playing: " .. currentTrack, 20, height - 40)
    end
    	if cdplaying==true and format=="audio" then 
		equalizerdraw(dt)
	end
end



function love.mousepressed(x, y, button, istouch, presses)
if musicMenu==true then
    print("Mouse position: (" .. x .. ", " .. y .. ")")
    -- Adjust the y range to match the layout of your music files accurately
    for i, file in ipairs(musicFiles) do
        print("Checking file " .. i .. ": y range = [" .. (60 + i * 100) .. ", " .. (160 + i * 100) .. "]")
        if y >= 60 + i * 100 and y <= 160 + i * 100 then
            playMusic(file)
        end
    end
end
end

function love.touchpressed(id, x, y, dx, dy, pressure)
if musicMenu==true then
    if id then
        print("Touch position: (" .. x .. ", " .. y .. ")")
        -- Adjust the y range to match the layout of your music files accurately
         for i, file in ipairs(musicFiles) do
            local rectY = 0 + i * 100
            print("Checking file " .. i .. ": y range = [" .. rectY .. ", " .. (rectY + 100) .. "]")
            if y >= rectY and y <= rectY + 100 then
                playMusic(file)
            end
          end
    end
end
end








function playMusic(filename)
    currentTrack = filename
    if filename:match("%.mod$") or filename:match("%.xm$") or filename:match("%.s3m$") or filename:match("%.it$") then
        format = "tracker"
        loadOpenMPT(filename)
    else
        format = "audio"
        if love.audio.getActiveSourceCount() > 0 then
            --for _, source in ipairs(love.audio.getActiveSources()) do
                --source:stop()
                love.audio.stop()
				cdplaying=false
            --end
        end
        local source = love.audio.newSource(filename, "stream")
        --source:play()
        equalizerload(filename)
        cdplaying=true
    end
end

