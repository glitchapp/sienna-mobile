-- Function to check if a button (regular or image) is hovered
function isButtonHovered(button)
    local x, y, width, height

    -- Check if the button is an image button
    if button.image then
        x = button.x
        y = button.y
        width = button.image:getWidth()
        height = button.image:getHeight()
    else
        -- Calculate dimensions for a regular button with text
        local font = button.font or love.graphics.getFont()
        local textWidth = font:getWidth(button.text)
        local textHeight = font:getHeight()
        local sx, sy = button.sx or 1, button.sy or button.sx or 1
        x = button.x
        y = button.y
        width = textWidth * sx
        height = textHeight * sy
    end

    -- Get the current mouse position
    local mouseX, mouseY = love.mouse.getPosition()

    -- Get the touch positions (if available)
    local touches = love.touch.getTouches()

    -- Iterate over all active touches
    for _, touchID in ipairs(touches) do
        local touchX, touchY = love.touch.getPosition(touchID)
        -- Check if the touch is within the button's bounds
        if touchX >= x and touchX <= x + width and touchY >= y and touchY <= y + height then
            -- Set the button's dimensions to the calculated width and height
            button.w, button.h = width, height
            return true  -- Return true to indicate that the button is being hovered over
        end
    end

    -- Check if the mouse cursor is within the button's bounds
    if mouseX >= x and mouseX <= x + width and mouseY >= y and mouseY <= y + height then
        -- Set the button's dimensions to the calculated width and height
        button.w, button.h = width, height
        return true  -- Return true to indicate that the button is being hovered over
    end

    return false  -- Return false to indicate that the button is not being hovered over
end
