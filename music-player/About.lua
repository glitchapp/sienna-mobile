
function loadAbout()

local desktopWidth, desktopHeight = love.window.getDesktopDimensions( display )
local donateInfo, aboutInfo= false, false


WebP = require("lib/love-webp/love-webp")			-- webp library for love

AboutIcon = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("assets/AboutIcon.webp")))
zCashIcon = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("assets/donations/Zc.webp")))
zCashQrCode = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("assets/donations/ZCashQr.webp")))


AboutButton = {
	text = "",
	textPosition ="top",
	x = desktopWidth-600,
	y = desktopHeight-500, 
    sx = 1,
    image = AboutIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishmiddle,
    unlocked = true,
	}
	
		AboutHyperlinkButton = {
		text = "Repository",
		x = 800, 
		y = 1000,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
	
		AboutCloseButton = {
		text = "Close Window",
		x = 1400, 
		y = 360,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
	
	Aboutced30 = {
	text = "https://ced30.itch.io/",
		x = 800, 
		y = 550,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
	
	Aboutoldscooldemo = {
	text = "https://ced30.itch.io/oldscool-demo-lua-love2d-glsl",
		x = 800, 
		y = 590,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
	
	DonateButton = {
		text = "Donate",
		x = 1000, 
		y = 1000,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
	
	CloseDonateButton = {
		text = "Close",
		x = 1400, 
		y = 360,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
end

function drawAbout()
    if aboutInfo == true then
        love.graphics.setColor(0.2, 0.2, 0.2, 0.8)
		love.graphics.rectangle("fill", 800-25, 350, 800+50, 50)
        love.graphics.rectangle("fill", 800-25, 400, 800+50, 700)
        love.graphics.setColor(1, 1, 1)
        love.graphics.rectangle("line", 800-25, 350, 800+50, 50)
        love.graphics.rectangle("line", 800-25, 400, 800+50, 700)
        love.graphics.setFont(poorfishmiddle)
         if not (donateInfo==true) then
			love.graphics.print("About",  800-25, 360, 0, 1)
        elseif donateInfo==true then
			love.graphics.print("Donate info",  800-25, 360, 0, 1)
		end
        local maxWidth = 800
        local lineHeight = love.graphics.getFont():getHeight()

        local paragraph = "Music player <br>" ..
                          "Copyright (C) 2023 Glitchapp <br>" ..
                          ". <br>" ..
                          "Old school demo shaders by Cedric Baudouin <br>" ..
                          ". <br>" ..
						  ". <br>" ..
                          ". <br>" ..
                          "This program is free software: you can redistribute it and/or modify " ..
                          "it under the terms of the GNU General Public License as published by " ..
                          "the Free Software Foundation, either version 2 of the License, or " ..
                          "(at your option) any later version.<br>" ..
                          ". <br>" ..
                          "This program is distributed in the hope that it will be useful, " ..
                          "but WITHOUT ANY WARRANTY; without even the implied warranty of " ..
                          "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the " ..
                          "GNU General Public License for more details."
                          
                          
         local paragraphDonate ="Your support makes a world of difference! <br>" ..

							"By contributing, you are directly fueling the development of open-source software <br>" ..
							"Your generosity allows me to continue creating and sharing these exciting ventures <br>" ..
							"Your contribution makes you an integral part of these efforts.<br>" ..
							"Together, we foster creativity, share knowledge, and make our collective dreams a reality.<br>" ..

							"Thank you for being a part of this and for your invaluable support!<br>"
		 
                           

        -- Replace "<br>" with newline characters "\n"
        if not (donateInfo==true) then
			paragraph = string.gsub(paragraph, "<br>", "\n")
		elseif donateInfo==true then
			paragraph = string.gsub(paragraphDonate, "<br>", "\n")
			
			love.graphics.draw( zCashIcon,1000, 800, 0, 1)  
			love.graphics.draw( zCashQrCode,800, 800, 0, 0.5)          
		end	

        -- Split the paragraph into lines respecting the maximum width
        local lines = {}
        local currentLine = ""
        for line in paragraph:gmatch("[^\n]+") do
            local words = {}
            for word in line:gmatch("%S+") do
                table.insert(words, word)
            end

            local testLine = currentLine
            for _, word in ipairs(words) do
                local testLineWidth = love.graphics.getFont():getWidth(testLine .. word .. " ")
                if testLineWidth <= maxWidth then
                    testLine = testLine .. word .. " "
                else
                    table.insert(lines, testLine)
                    testLine = word .. " "
                end
            end

            if testLine ~= "" then
                table.insert(lines, testLine)
            end
            currentLine = ""
        end

        local x, y = 800, 410
        for _, line in ipairs(lines) do
            love.graphics.print(line, x, y)
            y = y + lineHeight
        end
    end
    
     if aboutInfo==true and not (donateInfo==true) then
     	local hovered = isButtonHovered (AboutCloseButton)
				drawButton (AboutCloseButton, hovered)
					if hovered and (love.mouse.isDown(1)) then 
						aboutInfo=false
						--love.timer.sleep( 0.3 )	
					end
    
    	local hovered = isButtonHovered (AboutHyperlinkButton)
				drawButton (AboutHyperlinkButton, hovered)
					if hovered and (love.mouse.isDown(1)) then 
						love.system.openURL("https://codeberg.org/glitchapp/Music-Player")
						love.timer.sleep( 0.3 )	
					end
			
		local hovered = isButtonHovered (Aboutced30)
				drawButton (Aboutced30, hovered)
					if hovered and (love.mouse.isDown(1)) then 
						love.system.openURL("https://ced30.itch.io/")
						love.timer.sleep( 0.3 )	
					end
					
		local hovered = isButtonHovered (Aboutoldscooldemo)
				drawButton (Aboutoldscooldemo, hovered)
					if hovered and (love.mouse.isDown(1)) then 
						love.system.openURL("https://ced30.itch.io/oldscool-demo-lua-love2d-glsl")
						love.timer.sleep( 0.3 )	
					end
					
						local hovered = isButtonHovered (DonateButton)
				drawButton (DonateButton, hovered)
					if hovered and (love.mouse.isDown(1)) then 
						donateInfo=true
						love.timer.sleep( 0.3 )	
					end
	
	elseif donateInfo==true then
					local hovered = isButtonHovered (CloseDonateButton)
					drawButton (CloseDonateButton, hovered, CloseDonateButton.text)
				if hovered and (love.mouse.isDown(1) ) then 
						aboutInfo=false
						donateInfo=false
						love.timer.sleep( 0.3 )	
				elseif hovered then
					love.graphics.print("Close donations info", DonateButton.x,DonateButton.y+100,0,1)
				end									
    end

end
