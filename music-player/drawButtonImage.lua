


-- Function to check if a button (regular or image) is hovered
function hoveredImage (button)
	  local x, y, width, height
 
        x = button.x
        y = button.y
        imagewidth = button.image:getWidth()
        imageheight = button.image:getHeight()
   

-- Get the current mouse position
    local mouseX, mouseY = love.mouse.getPosition()

  -- Check if the mouse cursor is within the button's bounds
    if (mouseX >= x and mouseX <= x + imagewidth and mouseY >= y and mouseY <= y + imageheight) then
		
          -- Set the button's dimensions to the calculated width and height
        button.w, button.h = width, height
        return true	-- Return true to indicate that the button is being hovered over
    end

       return false  -- Return false to indicate that the button is not being hovered over
end

--[[This code defines a function drawButton that takes three parameters: button, hovered, and text.
This function is used to draw a button on the screen with the specified properties.--]]

function drawButtonImage (button, hovered)	--Defines a function drawButton with three parameters: button, hovered, and text.
		
	love.graphics.setFont( button.font )	--Sets the font of the button to the font specified in button.font.
	
		local buttonw = button.image:getWidth()
        local buttonh = button.image:getHeight()
	
    if hovered then									--If the button is being hovered over, 
		love.graphics.setColor(button.hoveredColor)	--the button color is set to button.hoveredColor, 
		
		 -- Draw a rectangle around the button when hovered
        love.graphics.rectangle('line', button.x, button.y, buttonw, buttonh)
    else
        love.graphics.setColor(button.color)
    end    

    
        local x, y, width, height = button.x, button.y, button.image:getWidth(), button.image:getHeight()
		if hovered then									--If the button is being hovered over, 
			love.graphics.setColor(1,0.5,0.5,1)
		end
        -- Calculate the position for the text based on textPosition
        if textPosition == "top" then
            y = y - height / 2  -- Adjust the y-coordinate to be at the top of the image button
        elseif textPosition == "bottom" then
            y = y + height / 2  -- Adjust the y-coordinate to be at the bottom of the image button
        end

        -- Draw the image button
        love.graphics.draw(button.image, x, y, button.r, button.sx)
  
	love.graphics.print(button.text,button.x,button.y,button.r,button.sx)	--Prints the specified text on the button at the position specified by button.x and button.y with the specified rotation button.r and scale button.sx.
		
end
