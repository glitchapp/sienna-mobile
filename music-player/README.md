## Standalone Music player with support for many audio formats

![Screenshot 1](/screenshots/screenshot.webp)

This is a standalone music player with the following features:

 - Drag and drop: Drag your music to the player and it automatically play the track if supported

- Bar spectrogram visualization (channels and master)

- Vu meters (channel and master)

- Old school shader effects (by Cedric Baudouin, see credits below)

- Controls: backward, forward and volume controls.

- UI providing track duration and playing position with bars and values

- An extensive ammount of audio format supported such as:

| Format        | Extensions Supported                                       	|
|---------------|---------------------------------------------------------------|
| Audio         | .ogg, .mp3, .wav, .flac, .oga, .ogv, .flac                 	|
| MIDI/Notation | .abc, .mid, .pat                                        		|
| Tracker       | .669, .amf, .ams, .c67, .dbm, .digi, .dmf, .dsm, .dsym, .dtm,	|
|				| .far, .fmt, .imf, .ice, .j2b, .m15, .mdl, .med, .mms, .mt2,	|
|				| .mtm, .mus, .nst, .okt, .plm, .psm, .pt36, .ptm, .sfx, .sfx2, |										|
|               | .st26, .stk, .stm, .stx, .stp, .symmod, .ult, .wow, .gdm, .mo3,|
|				| .oxm, .umx, .xpk, .ppm 										|
|               | .mmcmp, .mptm, .mod, .s3m, .xm, .it                	        |
| SunVox        | .sunvox                                      		            |
| M3U Playlist  | .m3u                                              	        |


    - Audio Formats: 7 formats
    - MIDI/Notation Formats: 3 formats
    - Tracker Formats (OpenMPT): 55 formats
    - SunVox Format: 1 format

	- Total Supported Formats: 66 formats.

Tweak it to fit your needs.

## 1. install requirements

Linux: sudo apt install love libwebp-dev libopenmpt-dev

Windows: you need to install love-webp libraries from [![this repository](https://github.com/ImagicTheCat/love-webp/tree/master/dist)
Copy the files inside win64 / win 32 to the folder where love is installed / where binaries are (if you installed love you'll usually find the folder at "C:/programs/love/"

You also need openMPT libraries which you can find here: https://lib.openmpt.org/libopenmpt/
More info on lua-openmpt can be found here: https://github.com/konsumer/lua-openmpt

# Oldschool demo (background shaders)

The old school demo and shader have been created by Cedric Baudouin (https://ced30.itch.io/)

The original project can be found here: shttps://ced30.itch.io/oldscool-demo-lua-love2d-glsl

I tweaked some of the scenes and mixed them and created a fork with mixed scenes here: https://codeberg.org/glitchapp/OldScool-Demo

# License

 Copyright (C) 2023  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

The libraries on the lib folder belong to their authors and have their own licenses which can be found on their folders.

# Donations

If you enjoy this software and you want to support development and open source software please consider to donate. Any donations are greatly appreciated.

![Zcash address:](https://codeberg.org/glitchapp/pages/raw/branch/main/otherProjects/img/Zc.webp) t1h9UejxbLwJjCy1CnpbYpizScFBGts5J3N
![Zcash Qr Code:](https://codeberg.org/glitchapp/pages/raw/branch/main/otherProjects/img/ZCashQr160.webp)
