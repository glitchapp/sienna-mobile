


  
  function loadTrackerSong(mod)
	
	--mod = OpenMPT:new(filename)
	trackerRow = 0
	trackerSpeed = 30 -- Adjust the speed of the tracker's movement
	numRows = 10 -- Number of rows to display in the grid
  
  modChannelCount = mod:get_num_channels()
  modInstCount = mod:get_num_instruments()
  modLength = mod:get_duration_seconds()
  modTitle = mod:get_metadata("title")
  if modTitle == "" then modTitle = "Untitled" end
  modArtist = mod:get_metadata("artist")
  if modArtist == "" then modArtist = "Unknown" end
  love.window.setTitle(modTitle)



  
  end
  
function drawTrackerSong()
    love.graphics.setFont(font3)
    love.graphics.print(modTitle .. " by " .. modArtist, 10, 10)
    love.graphics.setFont(font2)
    love.graphics.print("Speed: " .. mod:get_current_speed(), 10, 40)
    love.graphics.print("Tempo: " .. mod:get_current_tempo(), 10, 70)
    love.graphics.print("Current Pattern: " .. mod:get_current_pattern() .. " / " .. mod:get_num_patterns(), 10, 100)
    love.graphics.print("Channels: " .. modChannelCount, 10, 130)
    love.graphics.print("Instruments: " .. modInstCount, 10, 160)
    love.graphics.print("Position (s): " .. math.floor(mod:get_position_seconds() % 60) .. " / " .. modLength, 10, 190)
    --love.graphics.print("Estimated bpm: " .. mod:get_current_estimated_bpm(), 10, 220)
    love.graphics.print("Samples: " .. mod:get_num_samples(), 400, 40)
    love.graphics.print("Current row: " .. mod:get_current_row() .. " / " .. mod:get_pattern_num_rows(mod:get_current_pattern()), 400, 70)

-- Split and print supported extensions by openMPT
--[[
	-- Split a string into a table using a delimiter
function str_split(input, delimiter)
    local result = {}
    for match in (input..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(result, match)
    end
    return result
end

-- Show supported extensions:
local supportedExtensions = mod:supported_extensions()
for i, extension in ipairs(supportedExtensions) do
    love.graphics.print(extension, 400, 100 + i * 20)
	--print(extension)
end
--]]



    if format == "tracker" then
        love.graphics.print("Playing: " .. math.floor(mod:get_position_seconds() % 60), (desktopWidth / 3) - 100, (desktopHeight / 3) + 500, 0, 0.8, 1)
        love.graphics.print(math.floor((mod:get_position_seconds() * 60) % 60), (desktopWidth / 3) + 50, (desktopHeight / 3) + 500, 0, 0.8, 1)
        -- Draw position bars
        love.graphics.rectangle("fill", (desktopWidth / 3) - 100, (desktopHeight / 3) + 400, math.floor(mod:get_position_seconds() % 60) * 2, 25)
        love.graphics.rectangle("line", (desktopWidth / 3) - 100, (desktopHeight / 3) + 400, modLength * 2, 35)
    end


drawTrackerTable()

-- Draw the tracker grid
local gridX = 50
local gridY = (desktopHeight / 3) + 200




-- Define the VU meter parameters
local vuMeterWidth = 20
local vuMeterHeight = 100
local vuMeterXSpacing = 30 -- Spacing between VU meters
local vuMeterY = 600

	
        --love.graphics.rectangle("fill", gridX + (channel * cellWidth), 600, cellWidth-10, -100 * vu)
        --love.graphics.print(string.format(" %02d", channel + 1), gridX +  (channel * cellWidth), 650)
    
	
      -- Draw the VU meter background for this channel
        love.graphics.setColor(0.1, 0.1, 0.1,0.5)
		love.graphics.rectangle("fill", gridX-10 , gridY-300, gridX + (modChannelCount * cellWidth), vuMeterHeight*2.5)
 

    for channel = 0, (modChannelCount - 1) do
        local vu = mod:get_current_channel_vu_mono(channel)
        local vuMeterX = 100 + channel * vuMeterXSpacing


        -- Calculate the length of the VU meter line based on the audio volume
        local lineLength = vuMeterHeight * vu

		--draw bar
		--love.graphics.setColor(1, 1, 1) -- Color the line (e.g., red)
		--love.graphics.rectangle("fill", gridX + (channel * cellWidth), 600, cellWidth-10, -100 * vu)

        -- Draw the VU meter line
        --love.graphics.setColor(1, 0, 0) -- Color the line (e.g., red)
        
         -- Create a color gradient based on the VU level (for example, from green to red)
        local r, g, b = getColorForVUMeter(vu)

        
        love.graphics.setLineWidth(5) -- Set line width
        --love.graphics.line(gridX +cellWidth/2  + (channel * cellWidth), 600,gridX+cellWidth/2 + (channel * cellWidth), vuMeterY - lineLength)
        
        
-- Define the number of rectangles and the height of each color zone
local numRectangles = vuMeterHeight / 5
local redZoneHeight = math.floor(numRectangles / 3)
local yellowZoneHeight = math.floor(2 * numRectangles / 3)

for i = 1, numRectangles do
    -- Calculate the position of each rectangle
    local x1 = gridX - 25 + cellWidth / 2 + (channel * cellWidth)
    local y1 = 600 - i * 5  -- Adjust the height of the rectangles
    local x2 = x1
    local y2 = y1 - 5  -- Adjust the height of the rectangles

    -- Calculate the color for this specific rectangle based on its position
    local rectR, rectG, rectB

    if i <= redZoneHeight then
        -- Lower part of the VU meter (green)
        rectR = 0
        rectG = 1
        rectB = 0
    elseif i <= yellowZoneHeight then
        -- Middle part of the VU meter (yellow)
        rectR = 1
        rectG = 1
        rectB = 0
    else
        -- Upper part of the VU meter (red)
        rectR = 1
        rectG = 0
        rectB = 0
    end

    -- Set the color for this specific rectangle
    love.graphics.setColor(rectR, rectG, rectB)

    -- Draw each rectangle based on the VU level
    if i <= (numRectangles * vu) then
        love.graphics.rectangle("fill", x1, y1, cellWidth - 10, 1)
    end
end




        
         --pointers
         
         -- vu meter backgrounds
         
         -- VU meter parameters
local vuMeterX = 400 -- X-coordinate of the center of the VU meter
local vuMeterY = 400 -- Y-coordinate of the center of the VU meter
local vuMeterRadius = 25 -- Radius of the VU meter
local vuMeterStartAngle = math.rad(210) -- Start angle of the inverted arc (e.g., 210 degrees)
local vuMeterEndAngle = math.rad(330) -- End angle of the inverted arc (e.g., 330 degrees)



-- Set the maximum length of the pointer
local maxPointerLength = 25  -- Adjust this to your desired length

-- Calculate the angle of the pointer based on the VU level
local angle = math.rad(45 + vu * 90) -- 0 to 90 degrees

	local vu = mod:get_current_channel_vu_mono(channel)
    local vuMeterX = gridX + cellWidth / 2 + channel * cellWidth

    -- Calculate the length of the VU meter line based on the audio volume
    local lineLength = maxPointerLength * vu

    -- Draw the VU meter pointer line
    
    love.graphics.setLineWidth(5) -- Set line width

    -- Calculate the endpoint (x2, y2) for the pointer
    local x2 = vuMeterX - maxPointerLength * math.cos(angle)
    local y2 = vuMeterY - maxPointerLength * math.sin(angle)

    -- Draw the VU meter background
love.graphics.setColor(0.2, 0.2, 0.2) -- Set the color for the background
love.graphics.arc("line", vuMeterX, vuMeterY+50, vuMeterRadius, vuMeterStartAngle, vuMeterEndAngle, 100)
love.graphics.setColor(1, 0, 0) -- Color the line (red)

    -- Draw the pointer line, aligning it with the VU meter background
    love.graphics.line(vuMeterX, vuMeterY+50, x2, y2+50)
       
    end
	DrawMasterVuMeter()
	drawMasterVuBar()
end

function drawTrackerTable()

-- Initialize a table to store the history of played rows
local playedRows = {}
    
-- Draw the tracker grid
local gridX = 50
local gridY = (desktopHeight / 3) + 200


-- Calculate the cell width based on the number of channels (modChannelCount)
if modChannelCount>10 then
	cellWidth = (love.graphics.getWidth() - gridX) / modChannelCount
else cellWidth=60
end
cellHeight = 20 




-- Calculate the total number of rows in the pattern
local totalRows = mod:get_pattern_num_rows(mod:get_current_pattern())

-- Calculate the starting row for the grid
local startRow = trackerRow - math.floor(numRows / 2)
if startRow < 0 then
    startRow = 0
end

-- Calculate the ending row for the grid
local endRow = startRow + numRows - 1
if endRow >= totalRows then
    endRow = totalRows - 1
    startRow = endRow - numRows + 1
end

-- Calculate the position of the tracker based on time and speed
trackerRow = mod:get_current_row()

-- Offset for scrolling rows
local rowOffset = 0
if trackerRow >= 10 then
    rowOffset = trackerRow - 10
    trackerRow = 10 -- Limit tracker row to 10
end

-- Adjust trackerRow for displaying filled rows at the bottom
trackerRow = trackerRow + rowOffset

for row = 0, numRows - 1 do
    for col = 0, modChannelCount - 1 do -- Display 16 columns
        local x = gridX + col * cellWidth
        local y = gridY + row * cellHeight

        -- Calculate the displayed row number with offset
        local displayedRow = row + rowOffset + 1

        -- Draw row numbers at the left side of the grid
        love.graphics.print(string.format("%02d", displayedRow), gridX - 30, y)

        -- Check if the current column (channel) matches the playing channel
        local isChannelPlaying = col + 1 == mod:get_current_playing_channels()

        -- Set the fill color based on whether the channel is playing
        if displayedRow == trackerRow and isChannelPlaying then
            love.graphics.setColor(1, 0, 0, 1) -- Red color for playing channel
            love.graphics.rectangle("fill", x, y, cellWidth, cellHeight)
        elseif displayedRow == trackerRow then
            love.graphics.setColor(1, 1, 1, 1) -- White color for non-playing channel
            love.graphics.rectangle("fill", x, y, cellWidth, cellHeight)
        else
            love.graphics.setColor(0.5, 0.5, 0.5, 1)
            love.graphics.rectangle("line", x, y, cellWidth, cellHeight)
        end
    
    
    end
end

-- Display the history of played rows
for _, playedRow in ipairs(playedRows) do

    local x = gridX + playedRow.col * cellWidth
    local y = gridY + (playedRow.row - rowOffset - 1) * cellHeight

    love.graphics.setColor(0, 1, 0, 0.5) -- Green color for played cells
    love.graphics.rectangle("fill", x, y, cellWidth, cellHeight)
end
end

function drawMasterVuBar()
    -- Define the VU meter parameters
    local vuMeterWidth = 20
    local vuMeterHeight = 100
    local vuMeterXSpacing = 30 -- Spacing between VU meters
    local vuMeterY = 200

    -- Define the number of rectangles and the height of each color zone
    local numRectangles = vuMeterHeight / 5
    local redZoneHeight = math.floor(numRectangles / 3)
    local yellowZoneHeight = math.floor(2 * numRectangles / 3)

    -- Calculate the middle level by averaging VU levels of all channels
    local totalVU = 0
    for channel = 0, (modChannelCount - 1) do
        totalVU = totalVU + mod:get_current_channel_vu_mono(channel)
    end
    local middleLevel = totalVU / modChannelCount

    -- Calculate the position of the master VU bar
    local x1 = 850
    local y1 = 200
    local x2 = x1
    local y2 = y1 - vuMeterHeight

    -- Set the color for the master VU bar
    love.graphics.setColor(0.1, 0.1, 0.1)

    -- Draw the master VU bar
    --love.graphics.rectangle("fill", x1, y1, vuMeterWidth, -vuMeterHeight)

    -- Draw the color zones for the master VU bar
    for i = 1, numRectangles do
        -- Calculate the color for this specific rectangle based on its position
        local rectR, rectG, rectB

        if i <= redZoneHeight then
            -- Lower part of the VU meter (green)
            rectR = 0
            rectG = 1
            rectB = 0
        elseif i <= yellowZoneHeight then
            -- Middle part of the VU meter (yellow)
            rectR = 1
            rectG = 1
            rectB = 0
        else
            -- Upper part of the VU meter (red)
            rectR = 1
            rectG = 0
            rectB = 0
        end

        -- Set the color for this specific rectangle
        love.graphics.setColor(rectR, rectG, rectB)

        -- Draw each rectangle based on the middle level
        if i <= (numRectangles * middleLevel) then
            love.graphics.rectangle("fill", x1, y1 - i * 5, vuMeterWidth, 5)
        end
    end
end



function DrawMasterVuMeter()
-- Calculate the middle level by averaging VU levels of all channels
local totalVU = 0
for channel = 0, (modChannelCount - 1) do
    totalVU = totalVU + mod:get_current_channel_vu_mono(channel)
end
local middleLevel = totalVU / modChannelCount

-- Draw the master VU meter bar
local masterBarHeight = 100
local masterBarWidth = 20
local masterBarX = 1000
local masterBarY = 200
love.graphics.setColor(1,1,1)
love.graphics.print("Master", masterBarX-50, masterBarY-150)
love.graphics.setColor(0.1, 0.1, 0.1)
love.graphics.rectangle("fill", masterBarX, masterBarY, masterBarWidth, -masterBarHeight)

-- Calculate the angle for the VU meter pointer based on the middle level
local maxPointerLength = 100  -- Adjust this to your desired length
local pointerAngle = math.rad(45 + (middleLevel * 90))  -- 45 to -45 degrees

-- Define the VU meter parameters
    local vuMeterX = masterBarX  	-- same X-coordinate as the master bar
    local vuMeterY = masterBarY-50  -- Y-coordinate for the VU meter
    local vuMeterRadius = 100
    local vuMeterStartAngle = math.rad(210)
    local vuMeterEndAngle = math.rad(330)

    -- Draw the VU meter background
love.graphics.setColor(0.2, 0.2, 0.2) -- Set the color for the background
love.graphics.arc("line", vuMeterX, vuMeterY+50, vuMeterRadius, vuMeterStartAngle, vuMeterEndAngle, 100)
love.graphics.setColor(1, 0, 0) -- Color the line (red)

-- Calculate the VU meter background arc parameters (assuming you use vuMeterRadius, vuMeterStartAngle, and vuMeterEndAngle)
local arcRadius = vuMeterRadius + 20  -- Adjust the radius based on your layout
local arcStartAngle = vuMeterStartAngle
local arcEndAngle = vuMeterEndAngle
local arcCenterX = vuMeterX
local arcCenterY = vuMeterY + 50  -- Adjust the vertical position based on your layout


-- Define the typical VU meter values
local vuValues = {"-20", "-10", "-5", "-3", "+1", "-+3"}

-- Calculate the angle step for positioning the labels along the arc
local numLabels = #vuValues
local angleStep = (arcEndAngle - arcStartAngle) / (numLabels - 1)

-- Print typical VU meter values along the arc
love.graphics.setColor(1, 1, 1) -- Set the color for the text
for i, value in ipairs(vuValues) do
    local labelAngle = arcStartAngle + (i - 1) * angleStep
    local labelX = arcCenterX + arcRadius * math.cos(labelAngle) - 10
    local labelY = arcCenterY + arcRadius * math.sin(labelAngle)
    love.graphics.setFont(poorfishsmall)
    love.graphics.print(value, labelX, labelY)
end
love.graphics.setFont(poorfish)
-- Draw the VU meter pointer
local pointerX1 = masterBarX + masterBarWidth / 2
local pointerY1 = masterBarY
local pointerX2 = pointerX1 - maxPointerLength * math.cos(pointerAngle)
local pointerY2 = pointerY1 - maxPointerLength * math.sin(pointerAngle)

love.graphics.setColor(1, 0, 0)
love.graphics.setLineWidth(5)
love.graphics.line(pointerX1, pointerY1, pointerX2, pointerY2)


end

function getColorForVUMeter(vu)
    -- Define the middle level where it's yellow
    local middleLevel = 0.5

    -- Calculate the color based on the VU level
    local r, g, b

    if vu <= middleLevel then
        -- Green to yellow (increasing red component)
        r = vu / middleLevel
        g = 1
        b = 0
    else
        -- Yellow to red (decreasing green and increasing red components)
        r = 1
        g = 1 - (vu - middleLevel) / (1 - middleLevel)
        b = 0
    end

    return r, g, b
end
