local shapes = {}

function shapes:newCircle(pX, pY, pR)
  local c = {}
  c.oX = pX
  c.oY = pY
  c.x = pX
  c.y = pY
  c.oR = pR
  c.r = pR
  c.c = {1, 1, 1, 1}
  c.fill = "fill"
  c.scale_x = 1
  c.scale_y = 1
  c.state   = "none"
  
  function c:update(time)
    
  end
  
  function c:draw()
    
  end
  
  return c
end

function shapes:newBall(pX, pY, pR)
  local b = self:newCircle(pX, pY, pR)
  b.addCounter  = 0
  b.angle       = 0
  b.maxTail     = 380
  b.tail        = {}  
  b.shader      = love.graphics.newShader("Modules/Shaders/shd_Ball.fs")
  b.state       = "shape_nephroid2"
  b.shapes      = {"shape_nephroid2", "shape_astroid","shape_flower", "shape_x", "shape_eight", "shape_nephroid1"}
  b.shapeCounter = 0
  b.shapeIndex   = 0
  b.texture      = love.graphics.newImage("Modules/Images/spr_ball.png")
  b.r            = b.texture:getWidth()
  b.time         = 0
  b.scale_x      = 1
  b.scale_y      = 1
  b.coef         = 0
  
  function b:initialize()
    
    
  end
  
  function b:lerp(initial_value, target_value, speed)
    local result = (1-speed) * initial_value + speed*target_value
    return result
  end
  
  function b:update(pTime)
    local time = pTime
    
    -- process shapes ------
    self.shapeCounter = b.shapeCounter+1
    if b.shapeCounter%500==0 then
      b.shapeIndex = b.shapeIndex+1
      if b.shapeIndex>#b.shapes then
        b.shapeIndex=1        
      end
      b.state = b.shapes[b.shapeIndex]
    end
    
    -- change alpha    
    if(time>13) then
      self.coef = self:lerp(self.coef, 1, 0.01)      
    end
    
    -- process tail ----
    table.insert(self.tail, {x=self.x, y=self.y, a=self.angle, sx=self.scale_x, sy=self.scale_y, r=self.r, self.c})    
    if #self.tail>self.maxTail then
      table.remove(self.tail, 1)
    end
    
    if self.state == "shape_eight" then    
      local scale = (2 / (3 - math.cos(2*time))*250);
      local newX = self.oX + scale * math.cos(time);
      local newY = self.oY + scale * math.sin(2*time) / 2;
      self.x = self:lerp(self.x, newX, 0.1)
      self.y = self:lerp(self.y, newY, 0.1)
      local new_sx = (math.sin(-time) / 2)+0.5
      local new_sy = new_sx
      self.scale_x = self:lerp(self.scale_x, new_sx, 0.1)
      self.scale_y = self:lerp(self.scale_y, new_sy, 0.1)
      
    elseif self.state == "shape_x" then      
      local scale = (2*math.sin(time) / (3 - math.cos(2*time))*400);
      local newX = self.oX + scale * math.cos(time);
      local newY = self.oY + scale * math.sin(2*time) / 2;
      self.x = self:lerp(self.x, newX, 0.1)
      self.y = self:lerp(self.y, newY, 0.1)
      self.scale_x = self:lerp(self.scale_x, 0.25, 0.1)
      self.scale_y = self:lerp(self.scale_y, 0.25, 0.1)
    
    elseif self.state == "shape_flower" then    
      local scale = 4
      local newX = self.oX + (math.cos(time * scale) * math.cos(time))*100;
      local newY = self.oY + (math.cos(time * scale) * math.sin(time))*100;
      self.x = self:lerp(self.x, newX, 0.1)
      self.y = self:lerp(self.y, newY, 0.1)
      self.scale_x = self:lerp(self.scale_x, 0.1, 0.1)
      self.scale_y = self:lerp(self.scale_y, 0.1, 0.1)
      
    elseif self.state == "shape_astroid" then    
      local scale = 64
      local newX = self.oX + scale* (math.cos(time)*math.cos(time)*math.cos(time));
      local newY = self.oY + scale* (math.sin(time)*math.sin(time)*math.sin(time));
      self.x = self:lerp(self.x, newX, 0.1)
      self.y = self:lerp(self.y, newY, 0.1)
      self.scale_x = self:lerp(self.scale_x, 0.25, 0.1)
      self.scale_y = self:lerp(self.scale_y, 0.25, 0.1)
    
    elseif self.state == "shape_nephroid1" then    
      local scale = 48
      local newX = self.oX + scale* (3*math.cos(time)-2*math.cos(3*time));
      local newY = self.oY + scale* (2*math.sin(3*time));
      self.x = self:lerp(self.x, newX, 0.1)
      self.y = self:lerp(self.y, newY, 0.1)
      self.scale_x = self:lerp(self.scale_x, 0.2, 0.1)
      self.scale_y = self:lerp(self.scale_y, 0.2, 0.1)
      
    elseif self.state == "shape_nephroid2" then    
      local scale = 32
      local newX = self.oX + scale* (3*math.cos(time)-2*(math.cos(time) * math.cos(time) * math.cos(time)))*2;
      local newY = self.oY + scale* (2*(math.sin(time) * math.sin(time) * math.sin(time)));
      self.x = self:lerp(self.x, newX, 0.1)
      self.y = self:lerp(self.y, newY, 0.1)
      local new_sx = (math.sin(-time*2) / 2)+0.5
      local new_sy = new_sx
      self.scale_x = self:lerp(self.scale_x, new_sx, 0.1)
      self.scale_y = self:lerp(self.scale_y, new_sy, 0.1)
      
    end
        
    b.time=pTime*2
  end
  
  function b:draw()
    
    local c = {(math.cos(b.time)/2)+0.7,
              (math.cos(45+b.time)/2)+0.7,
              (math.cos(90+b.time)/2)+0.7,
              1}
    
    local offset = 1
    
    for i=#self.tail-1, 1, -1 do
      if i>1 and i%19==0 then
        
        love.graphics.setColor(0,0,0,0.5*self.coef)
        love.graphics.draw(self.texture, self.tail[i].x-offset, self.tail[i].y+offset, self.tail[i].a, self.tail[i].sx, self.tail[i].sy, self.tail[i].r/2, self.tail[i].r/2)
        
        love.graphics.setColor(1,1,1,1*self.coef)
        love.graphics.draw(self.texture, self.tail[i].x, self.tail[i].y, self.tail[i].a, self.tail[i].sx, self.tail[i].sy, self.tail[i].r/2, self.tail[i].r/2)
        
      end
    end
    love.graphics.setColor(0,0,0,0.5*self.coef)
    love.graphics.draw(self.texture, self.x-offset, self.y+offset, self.angle, self.scale_x, self.scale_y, self.r/2, self.r/2)
    
    love.graphics.setColor(1,1,1,1*self.coef)
    love.graphics.draw(self.texture, self.x, self.y, self.angle, self.scale_x, self.scale_y, self.r/2, self.r/2)
  end  
  
  return b
end


return shapes