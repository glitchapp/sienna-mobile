local lib = {}

function lib:lerp(initial_value, target_value, speed)
	local result = (1-speed) * initial_value + speed*target_value
	return result
end

-- base text ------------------------------
function lib:newText(pString, pFont, pX, pY)
  local t = {}
        t.x     = pX
        t.y     = pY
        t.str   = pString
        t.font  = pFont
        t.charW = t.font:getWidth(" ")
        t.charH = t.font:getHeight(" ")
        t.r     = 0
        t.speed = 1
        t.time  = 0
        
        t.screenW = screenWidth
        t.screenH = screenHeight
        
    local l    = string.len(pString)
        t.strArray = {}  
  for i=1, l do    
    table.insert(t.strArray, pString:sub(i, i))
  end  
  
  t.width  = #t.strArray * t.charW
  t.height = #t.strArray * t.charH
  t.centered = true
    
  function t:update(dt)    
    
  end

  function t:draw()
    love.graphics.setFont(self.font)
    if self.centered then
      for i=1, #self.strArray do
        local w = #self.strArray*self.charW
        local x = (self.x+(i*self.charW))-w/2
        local y = self.y-self.charH/2
        
        love.graphics.setColor(0, 0, 0, 0.7)
        love.graphics.print(self.strArray[i], x+2, y+2)
        
        love.graphics.setColor(1, 1, 1, 1)
        love.graphics.print(self.strArray[i], x, y)
      end
    else
      for i=1, #self.strArray do
        local x = self.x+(i*self.charW)
        
        love.graphics.setColor(0, 0, 0, 0.7)
        love.graphics.print(self.strArray[i], x+2, self.y+2)
        
        love.graphics.setColor(1, 1, 1, 1)
        love.graphics.print(self.strArray[i], x, self.y)
      end
    end
  end
  
  return t
end

function lib:newRibbonText(pString, pFont, pX, pY)
  local t = lib:newText(pString, pFont, pX, pY)
  t.speed = 2
  
  function t:update(dt)
    self.x = self.x-self.speed
    if self.x + (#self.strArray*self.charW) < 0 then
      self.x = self.screenW
    end
    
    self.time = self.time + dt
  end
  
  function t:draw()    
    love.graphics.setFont(self.font)    
    for i=1, #self.strArray do
      local w = #self.strArray*self.charW
      local x = (self.x+(i*self.charW))
      local y = self.y-self.charH/2
      
      if x + self.charW > 0 and x-self.charW < self.screenW then
        local scale_y = math.sin(self.time-(i*0.5)/3.14159)
        love.graphics.setColor(0, 0, 0, 0.5)
        love.graphics.print(self.strArray[i], x+2, y+2, self.r, 1, scale_y, self.charW/2, self.charH/2)
          
        if scale_y < 0 then love.graphics.setColor(0.25, 0.25, 0.25, 1) else love.graphics.setColor(1, 1, 1, 1) end
        love.graphics.print(self.strArray[i], x, y, self.r, 1, scale_y, self.charW/2, self.charH/2)
      end
      
    end
  end
  
  return t
end

function lib:newScroller(pString, pFont, pX, pY)
  local t = lib:newRibbonText(pString, pFont, pX, pY)
        t.surface = love.graphics.newCanvas(t.screenW, t.screenH)
        t.surface:setFilter("nearest", "nearest", 16)
        t.speed = 1
  
  function t:update(dt)
    self.x = self.x-self.speed
    if self.x + (#self.strArray*self.charW) < 0 then
      self.x = self.screenW
    end
    
    self.time = self.time + 5
  end
  
  function t:draw()    
    love.graphics.setFont(self.font)
    
    love.graphics.push()
    love.graphics.setCanvas(self.surface)
    love.graphics.clear()
    love.graphics.scale(0.5, 0.5)
    
      for i=1, #self.strArray do
        local w = #self.strArray*self.charW
        local x = (self.x+(i*self.charW))
        local y = self.y-self.charH/2
        local yy = y + ( ((math.sin( math.rad(self.time + (i*self.charH*2))/2)+0.5) ) * self.charH)
        local letter_angle = 0-math.sin( math.rad(self.time/2+i+180) )/3.14159
        
        if x + self.charW > 0 and x-self.charW < self.screenW then      
          love.graphics.setColor(0, 0, 0, 0.5)
          love.graphics.print(self.strArray[i], x+2, yy+2, letter_angle, 1, 1, self.charW/2, self.charH/2)
            
          love.graphics.setColor(1, 1, 1, 1)
          love.graphics.print(self.strArray[i], x, yy, letter_angle, 1, 1, self.charW/2, self.charH/2)
        end        
      end
      
    love.graphics.setCanvas()
    love.graphics.pop()
    
    love.graphics.draw(self.surface, 0, 0)
  end
  
  return t
end

function lib:newSineScroller(pString, pFont, pX, pY)
  local t = lib:newScroller(pString, pFont, pX, pY)
        t.alpha = 0
        t.speed = 2
        t.state = "none"
        t.shader_sine = love.graphics.newShader("OldSchoolDemo/Modules/Shaders/shd_Wave_horizontal.fs")
  
  function t:update(dt)
    self.x = self.x-self.speed
    if self.x + (#self.strArray*self.charW) < 0 then
      self.x = self.screenW
    end
    
    self.time = self.time + dt
  end
  
  function t:draw()    
    love.graphics.setFont(self.font)
    
    love.graphics.push()
    love.graphics.setCanvas(t.surface)
    love.graphics.clear()
    love.graphics.scale(0.5, 0.5)
    
      for i=1, #self.strArray do
        local w = #self.strArray*self.charW
        local x = (self.x+(i*self.charW))
        local y = self.y-self.charH/2
        
        if x + self.charW > 0 and x-self.charW < self.screenW then      
          love.graphics.setColor(0, 0, 0, 0.5)
          love.graphics.print(self.strArray[i], x+2, y+2, 0, 1, 1, self.charW/2, self.charH/2)
            
          love.graphics.setColor(1, 1, 1, 1)
          love.graphics.print(self.strArray[i], x, y, 0, 1, 1, self.charW/2, self.charH/2)
        end        
      end
      
    love.graphics.setCanvas()
    love.graphics.pop()    
    
    love.graphics.setShader(self.shader_sine)
      self.shader_sine:send("time", self.time*10)
      self.shader_sine:send("displacement", self.charH/4)
      love.graphics.draw(t.surface, 0, 0)
    love.graphics.setShader()
  end
  
  return t
end

function lib:newHorizontalSine(pString, pFont, pX, pY)
  local t = lib:newSineScroller(pString, pFont, pX, pY)
        t.scale_x = 1
        t.scale_y = 1
        t.shader_sine = love.graphics.newShader("OldSchoolDemo/Modules/Shaders/shd_Wave_horizontal_rotoZoom.fs")
  
  function t:update(dt)
    self.x = self.x-self.speed
    if self.x + (#self.strArray*self.charW) < 0 then
      self.x = self.screenW
    end
    
    if self.state == "show" then
      self.alpha = lib:lerp(self.alpha, 1, 0.1)
    end
    
    if self.state == "hide" then
      self.alpha = lib:lerp(self.alpha, 0, 0.1)
    end
    
    self.time = self.time + dt
  end
  
  function t:draw()
    
    if self.alpha > 0 then
    
    love.graphics.setFont(self.font)
    
    love.graphics.push()
    love.graphics.setCanvas(self.surface)
    love.graphics.clear()
    love.graphics.scale(0.5, 0.5)
    
      for i=1, #self.strArray do
        local w = #self.strArray*self.charW
        local x = (self.x+(i*self.charW))
        local y = self.y-self.charH/2
        
        if x + self.charW > 0 and x-self.charW < self.screenW then      
          
          love.graphics.setColor(0, 0, 0, 0.5*self.alpha)
          love.graphics.print(self.strArray[i], x+2, y+2, 0, 1, self.scale_y, self.charW/2, self.charH/2)
          
          if self.scale_y < 0 then love.graphics.setColor(0.25, 0.25, 0.25, 1*self.alpha) else love.graphics.setColor(1, 1, 1, 1*self.alpha) end
          love.graphics.print(self.strArray[i], x, y, 0, 1, self.scale_y, self.charW/2, self.charH/2)
        end        
      end
      
    love.graphics.setCanvas()
    love.graphics.pop()    
    
    
    love.graphics.setShader(self.shader_sine)
      self.shader_sine:send("time", self.time*5)
      love.graphics.draw(self.surface, 0, 0)
    love.graphics.setShader()
    
    end
  end
  
  return t
end

function lib:newFlashText(pStrings, pFont, pX, pY)
  local t = {}
        t.x             = pX
        t.y             = pY
        t.str           = pStrings
        t.currentStr    = nil
        t.font          = pFont
        t.charW         = t.font:getWidth(" ")
        t.charH         = t.font:getHeight(" ")
        t.currentWidth  = 0
        t.currentHeight = 0
        t.time          = 0
        t.timer         = 0.16
        t.index         = 1
        t.surface       = love.graphics.newCanvas(screenWidth, screenHeight)
        t.surface:setFilter("nearest", "nearest", 16)
        t.alpha         = 0
        t.state         = "none"
  
  function t:setState(pState)
    self.state = pState
  end
  
  function t:setStr()
    self.currentStr    = self.strArray.str[self.index]
    self.currentWidth  = self.strArray.width[self.index]
    self.currentHeight = self.strArray.height[self.index]
  end
  
  function t:initialize()    
    local l              = #self.str
    
    self.strArray        = {}
    self.strArray.str    = {}
    self.strArray.width  = {}
    self.strArray.height = {}
    
    for i=1, l do 
      table.insert(self.strArray.str,    self.str[i])
      table.insert(self.strArray.width,  string.len(self.str[i])*self.charW)
      table.insert(self.strArray.height, self.charH)
    end    
    self:setStr()
  end
  
  -------------------------------------------
    t:initialize()
  -------------------------------------------
  
  function t:update(dt)    
    self.time = self.time + dt
    if self.time>self.timer then
      self.time = 0
      self.index = self.index + 1
      if self.index > #self.strArray.str then
        self.index = 1
      end
      self:setStr()
    end
    
    if self.state == "show" then
      self.alpha = lib:lerp(self.alpha, 1, 0.1)
    end
    
    if self.state == "hide" then
      self.alpha = lib:lerp(self.alpha, 0, 0.1)
    end
  end
  
  function t:draw()
    
    if self.alpha > 0 then
    
      love.graphics.setFont(self.font)
      
      love.graphics.push()
        love.graphics.setCanvas(self.surface)
          love.graphics.clear()
          love.graphics.scale(0.5, 0.5)
          
            love.graphics.setColor(0, 0, 0, 0.5)
            love.graphics.print(self.currentStr, self.x - (self.currentWidth/2) + 2 + (self.charW/2), self.y - (self.currentHeight/2) + 2, 0, 1, 1, self.charW/2, self.charH/2)
            
            love.graphics.setColor(1, 1, 1, 1)
            love.graphics.print(self.currentStr, self.x - (self.currentWidth/2)+ (self.charW/2), self.y - (self.currentHeight/2), 0, 1, 1, self.charW/2, self.charH/2)
            
        love.graphics.setCanvas()
      love.graphics.pop()    
      
      love.graphics.setColor(1, 1, 1, self.alpha)
      love.graphics.draw(self.surface, 0, 0)
    
    end
  end

  return t
end

function lib:newVerticalScrollText(pStrings, pFont, pX, pY)
  local t = {}
        t.x             = pX
        t.y             = pY
        t.str           = pStrings
        t.font          = pFont
        t.charW         = t.font:getWidth(" ")
        t.charH         = t.font:getHeight(" ")
        t.currentWidth  = 0
        t.currentHeight = 0
        t.time          = 0
        t.timer         = 0.16
        t.index         = 1
        t.surface       = love.graphics.newCanvas(screenWidth, screenHeight)
        t.surface:setFilter("nearest", "nearest", 16)
        t.alpha         = 0
        t.state         = "show"
        t.speed         = -0.5
        t.screenHeight  = screenHeight
        t.shader        = love.graphics.newShader("Modules/Shaders/shd_Wave_horizontal_alpha_vertical.fs")
  
  function t:setState(pState)
    self.state = pState
  end
  
  function t:initialize()    
    local l              = #self.str
    
    self.strArray        = {}
    self.strArray.str    = {}
    self.strArray.width  = {}
    self.strArray.height = {}
    
    for i=1, l do 
      table.insert(self.strArray.str,    self.str[i])
      table.insert(self.strArray.width,  string.len(self.strArray.str[i])*self.charW)
      table.insert(self.strArray.height, self.charH)
    end    
  end
  
  -------------------------------------------
    t:initialize()
  -------------------------------------------
  
  function t:update(dt)    
    self.time = self.time + dt
    
    self.y = self.y + self.speed
    
    
    if self.state == "show" then
      self.alpha = lib:lerp(self.alpha, 1, 0.1)
    end
    
    if self.state == "hide" then
      self.alpha = lib:lerp(self.alpha, 0, 0.1)
    end
  end
  
  function t:draw()
    
    if self.alpha > 0 then
      love.graphics.setFont(self.font)
      
      love.graphics.push()
        love.graphics.setCanvas(self.surface)
          love.graphics.clear()
          love.graphics.scale(0.5, 0.5)          
            for i=1, #self.strArray.str do
              local h = #self.strArray.str * self.charH
              local x = self.x - (self.strArray.width[i]/2) + (self.charW/2)
              local y = self.y + (i*self.charH)
              
              if y >= 0-self.charH and y <= self.screenHeight+self.charH then      
                love.graphics.setColor(0, 0, 0, 0.5)
                love.graphics.print(self.strArray.str[i], x+2, y+2, 0, 1, 1, self.charW/2, self.charH/2)
                  
                love.graphics.setColor(1, 1, 1, 1)
                love.graphics.print(self.strArray.str[i], x, y, 0, 1, 1, self.charW/2, self.charH/2)
              end        
            end          
        love.graphics.setCanvas()
      love.graphics.pop()
      
      love.graphics.setShader(self.shader)
        self.shader:send("time", self.time)
        love.graphics.draw(self.surface, 0, 0)
      love.graphics.setShader()
    end
  end

  return t
end

return lib
