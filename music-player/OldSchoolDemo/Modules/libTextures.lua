local libTextures={}

-- functions
function libTextures:newTextureData(pStr)
  local data={}  
  data.averageColors={}
  data.colors={}
  
  local textureData=love.image.newImageData(pStr)
  data.texture=love.graphics.newImage(textureData)
  data.textureW=data.texture:getWidth()
  data.textureH=data.texture:getHeight()

  for y=0,data.textureH-1 do
    for x=0,data.textureW-1 do
      local r,g,b,a=textureData:getPixel(x,y)
      local averageColor=(r+g+b)/3      
      table.insert(data.averageColors, averageColor)
      table.insert(data.colors, {r,g,b,a})
    end
  end  
  return data
end

function libTextures:newParallax(pStringArray, pX, pY)
  local p={}  
  for i=1, #pStringArray do
    local l={}
          l.img=love.graphics.newImage(pStringArray[i])
          l.x=pX
          l.y=pY
          l.offset_x=0
          l.offset_y=l.img:getHeight()/2
          l.w=l.img:getWidth()
          l.h=l.img:getHeight()
    table.insert(p,l)
  end  
  return p
end
  
return libTextures