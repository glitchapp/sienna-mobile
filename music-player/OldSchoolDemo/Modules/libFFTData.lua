local lib = {}

	local modChannelCount = mod:get_num_channels()
   --local vu = mod:get_current_channel_vu_mono(channel)
   
  function lib:newData(pMusic, pMusicData)
    local v      = {}
    
          v.music      = pMusic
          v.musicData  = pMusicData
          v.sampleRate = 1
          v.channels   = modChannelCount
          v.spectrum   = {}
          v.gTime      = 0
    
    function v:update(dt)
      local curSample = self.music:tell('samples')
      local wave = {}
      local size = next_possible_size(512)
        if self.channels == 2 then
            for i=curSample, (curSample+(size-1) / 2) do
                local sample = (self.musicData:getSample(i * 2) + self.musicData:getSample(i * 2 + 1)) * 0.5
                table.insert(wave,complex.new(sample,0))
            end
        else
            for i=curSample, curSample+(size-1) do
                table.insert(wave,complex.new(self.musicData:getSample(i),0))
            end
        end
        
      local spec = fft(wave,false)      
      
      function divide(list, factor)
        for i,v in ipairs(list) do
          list[i] = list[i] / factor
        end
      end
      
      divide(spec,size/2)
      
      self.spectrum = spec
    end
    
    return v
  end

return lib
