vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{  
  vec4 tex_color = Texel(texture, texture_coords);
  
  vec2 center = vec2(0.7, 0.3);
  
  float d           = distance(texture_coords, center);
  float m           = smoothstep(0, 0.6, d);  //1.8
  //tex_color.a       = m*(m* sin(texture_coords.y));
  
  //tex_color.r = (sin(texture_coords.y+texture_coords.x)/2)+0.5;
  //tex_color.g = (sin(texture_coords.x)/2)+0.5;
  //tex_color.b = (sin(texture_coords.y)/2)+0.5;
  
  return tex_color;
}