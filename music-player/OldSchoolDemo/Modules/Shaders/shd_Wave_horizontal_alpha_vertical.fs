extern float time         = 0;
extern float displacement = 32;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{  
  //texture_coords.y += (sin(texture_coords.x*displacement+(time/4))*0.005);
  texture_coords.x += (sin(texture_coords.y*displacement+(time/2))*0.01);
  vec4 tex_color = Texel(texture, texture_coords);
  
  if(tex_color.a != 0.0)
  {
    float top  = 0.2;
    float bottom = 0.8;
    
    if(texture_coords.y>bottom)
    {
      
      tex_color.a = 1-distance(vec2(0, texture_coords.y), vec2(0, bottom))*8;
    }
    if(texture_coords.y<top)
    {
      tex_color.a = 1-distance(vec2(0, texture_coords.y), vec2(0, top))*8;
    }
  }
  
  return tex_color;  
}