vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{    
  vec4 tex_color = Texel(texture, texture_coords);
  if(tex_color.a != 0.0)
  {
    float d = distance(texture_coords, vec2(0.5, 0.5));
    tex_color.a /= 1-d;
  }
  
  return tex_color;  
}