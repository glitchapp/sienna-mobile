# Old school demo

![Screenshot 1](/screenshots/screenshot.webp)

This is a fork of an old school demo style written in lua / lov2d / glsl by the award winning and great developer Cedric Baudouin alias ced 30

The original site of the project can be found here: https://ced30.itch.io/oldscool-demo-lua-love2d-glsl

I found the demo very interesting and decided to give it a try and I rapidly found myself tweaking it and mixing some of its components (I couldn't resist).

What you will found is new scenes which are basically composed by a mix of several shaders , objects, waving texts and effects.

Surprisingly they work well together and there's not even performance issues besides the many elements running simultaneously.

I hope you enjoy it as much as I did tweaking it, since the license is "do what you want with it" :)" feel free to fork it and tweak it and do not forget to credit the author in any case!


