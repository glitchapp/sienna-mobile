local scene = {}

scene.isLoaded   = false
scene.isFinished = false

-- modules
local libSprites = require("OldSchoolDemo/Modules/libSprites")
local libText    = require("OldSchoolDemo/Modules/libText")

-- scene specific declarations
local width=0
local height=0
local helpers=require("OldSchoolDemo/Modules/libHelpers")
local font=nil
local texture = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("OldSchoolDemo/Scenes/Scene_RotoZoom/Images/spr_Tiles074_2K_Color.webp")))
local texture2 = love.graphics.newImage("OldSchoolDemo/Scenes/Scene_RotoZoom/Images/spr_rototex.png")

local tex_w   = texture:getWidth()
local tex_h   = texture:getHeight()

local tex2_w   = texture2:getWidth()
local tex2_h   = texture2:getHeight()

local shader_rotoZoom   = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_RotoZoom/Shaders/shd_RotoZoom_normal.fs")
local shader_rotoZoom_D = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_RotoZoom/Shaders/shd_RotoZoom_trippy.fs")
local time = 0
local currentTime = 0

-- scroller
local str1         = "...< obey > ... < conform > ... < buy > ... < consume > ... < submit > ... < watch tv > ... < stay asleep > ... < no thought > ... < do not question authority > ... < marry > ... < reproduce > ... < work 8 hours > ... < eat > ..."
local str2         = {"< obey >", "< conform >", "< buy >", "< consume >", "< submit >", "< watch tv >", "< stay asleep >", "< no thought >", "< marry > ", "< reproduce >", "< work 8 hours >", "< eat >"}
local textScroller = {}
local textFlasher  = {}

local music = nil

function scene:initialize()
  width  = screenWidth
  height = screenHeight
  font   = love.graphics.newImageFont('OldSchoolDemo/Scenes/Scene_RotoZoom/SpriteFonts/spr_fontOldSchool_32_30.png',' abcdefghijklmnopqrstuvwxyz0123456789?=+-/%(),.é#@$^<>')
  
  textScroller = libText:newHorizontalSine(str1, font, width, height*2.9/3)
  textFlasher  = libText:newFlashText(str2, font, width/2, height*0.11)
  
  --music = love.audio.newSource("OldSchoolDemo/Scenes/Scene_RotoZoom/Music/BrainTraceDesign-RecoverTheSonics.mp3", "stream")
  --music:setLooping(false)
  --music:play()
end

function scene:update(dt)
 
  -- top
  if currentTime == 51    then textFlasher.state = "show" end
  if currentTime == 92    then textFlasher.state = "hide" end
  if currentTime == 107   then textFlasher.state = "show" end
  if currentTime == 117.2 then textFlasher.state = "hide" end
  if currentTime == 128   then textFlasher.state = "show" end
  if currentTime == 138   then textFlasher.state = "hide" end
  if currentTime == 148   then textFlasher.state = "show" end
  
  -- bottom
  if currentTime == 10    then textScroller.state = "show" end
  if currentTime == 107   then textScroller.state = "hide" end
  if currentTime == 117.2 then textScroller.state = "show" end
  if currentTime == 128   then textScroller.state = "hide" end
  if currentTime == 189   then textScroller.state = "show" end
  
  if currentTime>5 then
    textScroller:update(dt)
  end
  textFlasher:update(dt)
  --currentTime=round(music:tell("seconds"), 1)
  currentTime=math.floor(mod:get_position_seconds() % 60)
  time = time+(dt) 
  
  -- change scene ---------------
  --if not music:isPlaying() then self.isFinished = true end
end


function scene:draw()
  
  love.graphics.setShader(shader_rotoZoom)
    shader_rotoZoom:send("time", math.rad(time))
    love.graphics.draw(texture, width/2-tex_w/2, height/2-tex_h/2)
  love.graphics.setShader()
  
  love.graphics.setColor(1,1,1,1)
  love.graphics.setShader(shader_rotoZoom)
    shader_rotoZoom:send("time", math.rad(time*10))    
    love.graphics.draw(texture2, width/2-tex2_w/2, height/2-tex2_h/2)
  love.graphics.setShader()  
  
  textFlasher:draw()
  
  textScroller:draw()
  
  if currentTime == 51 or currentTime == 107 or currentTime == 128 or currentTime == 148 then
    love.graphics.setColor(1,1,1,0.7)
    love.graphics.rectangle("fill", 0, 0, width, height)
  end
end

return scene
