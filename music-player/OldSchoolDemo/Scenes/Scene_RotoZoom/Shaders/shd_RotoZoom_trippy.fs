float zoom = 8.0;
float offset = 2.5;
extern float time=0;

vec4 effect( vec4 color, Image texture, vec2 t_c, vec2 screen_coords )
{   
  float z  = cos(time)+1.5;
  float dx = cos(time + sin(t_c.x))*z;
  float dy = sin(time + cos(t_c.y/4))*z;
  
  t_c *= 2.0;
  t_c *= sin(time);
  
  float s = (1.2 + cos((t_c.x / 16.0) + (t_c.y / 32.0) + time)) / 2.0;
  t_c.x+= dx*s;
  t_c.y+= dy*s;
  
  vec2 newCoords = vec2(mod(t_c.x+dx, 1.0), mod(t_c.y+dy, 1.0));
  
  
  //uv.y += cos (uv.y)+sin (uv.x);
  //uv.x += iTime/250.;
  
  
  
  vec4 tex_color = Texel(texture, newCoords);
  
  
  return tex_color;  
}