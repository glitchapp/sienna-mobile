local scene = {}

scene.isLoaded   = false
scene.isFinished = false

-- scene specific declarations
local width   = 0
local height  = 0
local helpers = require("OldSchoolDemo/Modules/libHelpers")
local font    = nil

-- canvas -------------------------
local surf_loading = nil

-- textures ----------------------------------
local floppy = love.graphics.newImage("OldSchoolDemo/Scenes/Scene_Intro/Images/spr_floppy.png")

local music = nil

-- strings -------------------------------------
local str_Array = {"    loading    ", "    loading .  ", "    loading .. ", "    loading ..."}
local index     = 1
local time      = 0
local timer     = 0
local currentStr = ""
local fx        = nil

function scene:initialize()
  width=screenWidth
  height=screenHeight
  font= love.graphics.newImageFont('OldSchoolDemo/Scenes/Scene_Intro/Spritefonts/spr_Kromasky.png',' abcdefghijklmnopqrstuvwxyz0123456789!?:;,è./+%ç@à#')
  love.graphics.setFont(font)
  
  currentStr = str_Array[1]  
  fw=font:getWidth(currentStr)
  
  -- init canvas -------------------------------------------------------------------------
  surf_loading = love.graphics.newCanvas(width, height)
  surf_loading:setFilter("nearest", "nearest", 16)
  
  -- init loading screen ----------------------------------------------------------------- 
  
  
  --music = love.audio.newSource("OldSchoolDemo/Scenes/Scene_Intro/Music/Sun-Skidtro2.mp3", "stream")
  --music:setLooping(false)
  --music:play()
end


function scene:update(dt)
  
  time = time + dt
  if time > 1 then
    time = 0
    index = index+1
    if index > #str_Array then index = 1 end
    currentStr = str_Array[index]
  end
  
  -- change scene ---------------
  --if not music:isPlaying() then self.isFinished = true end
end


function scene:draw()
  love.graphics.push()
  love.graphics.setCanvas(surf_loading)
    love.graphics.scale(0.5, 0.5)
    love.graphics.clear()
    love.graphics.setColor(0, 0, 0, 1)
      love.graphics.rectangle("fill", 0, 0, width, height)
    love.graphics.setColor(1, 1, 1, 1)
      love.graphics.draw(floppy, (width/2)-(floppy:getWidth()/2), (height/2)-(floppy:getHeight()/2))
    love.graphics.setFont(font)
      love.graphics.print(currentStr, width/2 - fw/2, height*2/3)
  love.graphics.setCanvas()
  love.graphics.pop()
  
  love.graphics.draw(surf_loading, 0, 0)
end

return scene
