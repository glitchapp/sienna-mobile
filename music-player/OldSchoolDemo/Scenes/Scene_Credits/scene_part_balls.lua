local scene_part = {}

scene_part.isLoaded   = false
scene_part.isFinished = false

-- modules --------------------------------------------
local shapes = require("OldSchoolDemo/Objects/Shapes")

-- scene specific declarations ------------------------
local width=0
local height=0
local font=nil

-- canvas ----------------------------------------------
local surf_balls = nil

-- balls -----------------------------------------------
local ball_x = 0
local ball_y = 0
local ball = {}

-- angles ----------------------------------------------
local time = 0

function scene_part:initialize()
  width     = screenWidth
  height    = screenHeight
  
  -- load canvas --------------------------------------
  surf_balls = love.graphics.newCanvas(width, height)
  
  -- init canvas --------------------------------------
  love.graphics.setCanvas(surf_balls)
    love.graphics.setColor(0,0,0,0)
    love.graphics.rectangle("fill",0,0,width,height)
  love.graphics.setCanvas()
  
  ball_x = width/2
  ball_y = height/3
  ball = shapes:newBall(ball_x, ball_y, 16)
end




function scene_part:update(dt)
  ball:update(time)  
  time = time + (dt)
end


function scene_part:draw()
  
  
  -- draw to canvas ----------------------------------
  love.graphics.push()
    love.graphics.setCanvas(surf_balls)
    love.graphics.clear()
    love.graphics.scale(0.5, 0.5)  
      love.graphics.setColor(1,1,1,1)
      ball:draw()
    love.graphics.setCanvas()
  love.graphics.pop()
 
  love.graphics.draw(surf_balls, 0, 0)
  
  
  love.graphics.setColor(1,1,1,1)
end

return scene_part
