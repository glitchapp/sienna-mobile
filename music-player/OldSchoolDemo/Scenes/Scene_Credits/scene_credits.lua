local scene = {}

scene.isLoaded   = false
scene.isFinished = false

-- modules ----------------------------------
local shapes     = require("OldSchoolDemo/Objects/Shapes")
local libSprites = require("OldSchoolDemo/Modules/libSprites")
local libLights  = require("OldSchoolDemo/Modules/libLights")
local turrican   = require("OldSchoolDemo/Scenes/Scene_Credits/scene_part_turrican")
local balls      = require("OldSchoolDemo/Scenes/Scene_Credits/scene_part_balls")
local libText    = require("OldSchoolDemo/Modules/libText")

-- scene specific declarations
local width   = 0
local height  = 0
local helpers = require("OldSchoolDemo/Modules/libHelpers")
local font    = nil

-- shaders
local shader_BG      = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Credits/Shaders/shd_BG_credits.fs")
local shader_ground2 = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Credits/Shaders/shd_Ground_lighting.fs")
local shader_lights  = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Credits/Shaders/shd_Lights.fs")
local shader_pulse   = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Credits/Shaders/shd_PulseCircle.fs")
local shader_borders = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Credits/Shaders/shd_Borders.fs")
local shader_copper  = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Credits/Shaders/shd_CopperBars.fs")

-- canvases
local surf_BG      = nil
local surf_black   = nil
local surf_middle  = nil
local surf_borders = nil

-- lights
local light1 = {x=0,y=0,r=0}

-- scroller ---------------------------------------
local sineScroller = nil
local scroller = nil
local textToScroll = {" !!! credits !!!", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ... musics ...", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " loading screen:", " ", "sun - skidtro1", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " scene 1:", " ", " 4mat - anarchy menu", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " scene 2:", " ", " jogeirliljedahl - HybrisLight", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " scene 3:", " ", " braintrace design -", "  recover the sonics", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " scene 4:", " ", " fred - ilyad", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " scene 5:", " ", " romeo knight -", " enigma gun (ced edit)", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " scene 6:", " ", " emax - secret gardens", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ... graphics ...", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " scene1, parallax:", " ", " scoopex", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " electronic arts", " for the turrican character", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " special thanks to", " ", " gamecodeur and javidx9", " ", " for their incredible work,", " ", " and community!", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " thats all folks,", " thank you for watching !!!"}

-- angles
local time = 0

function scene:initialize()
  -- init screen -------------------------------------------------
  width  = screenWidth
  height = screenHeight
  font   = love.graphics.newImageFont('OldSchoolDemo/Scenes/Scene_Credits/SpriteFonts/spr_Kromasky_edit.png',' abcdefghijklmnopqrstuvwxyz0123456789!?:;,è./+%ç@à#-+¨µ')
  
  -- create canvases
  surf_BG = love.graphics.newCanvas(width,height)
  surf_BG:setFilter("nearest", "nearest")
  
  surf_black = love.graphics.newCanvas(width,height)
  surf_black:setFilter("nearest", "nearest")
  
  surf_middle = love.graphics.newCanvas(screenWidth, screenHeight)
  surf_middle:setFilter("nearest", "nearest")
  
  surf_borders = love.graphics.newCanvas(screenWidth, screenHeight)
  surf_borders:setFilter("nearest", "nearest")
  
  -- init canvases -----------------------------------------------  
  love.graphics.setCanvas(surf_BG)
    love.graphics.setColor(0,0,0,0)
    love.graphics.rectangle("fill",0,0,width,height)
  love.graphics.setCanvas()
  
  love.graphics.setCanvas(surf_black)
    love.graphics.setColor(0,0,0,1)
    love.graphics.rectangle("fill",0,0,width,height)
  love.graphics.setCanvas()  
  
  love.graphics.setCanvas(surf_middle)
    love.graphics.setColor(0, 0, 0, 0)
  love.graphics.setCanvas()
  
  love.graphics.setCanvas(surf_borders)
    love.graphics.setColor(0, 0, 0, 0)
  love.graphics.setCanvas()
  
  -- init scene_parts---------------------------------------------
  turrican:initialize()
  balls:initialize()
  
  scroller = libText:newVerticalScrollText(textToScroll, font, width/2, height)
  
  -- init music ---------------------------------------------------
  --music = love.audio.newSource("OldSchoolDemo/Scenes/Scene_Credits/Music/Emax-SecretGardens_edited.mp3", "stream")
  --music:setLooping(false)
  --music:play()
end

function scene:update(dt)
  
  turrican:update(dt)
  
  balls:update(dt)
  
  light1 = {  x = (width/2)  + (64 * math.cos(math.rad(time))) + (32 * -math.sin(math.rad(time)/2)),
              y = (height/2) + (64 * math.sin(math.rad(time))),
              r = 0.6}
  
  time = time + 1
  
  scroller:update(dt)
    
  -- change scene ---------------
  if not music:isPlaying() then self.isFinished = true end
end


function scene:draw()
    
  love.graphics.setShader(shader_copper)
    shader_copper:send("time", time)
    love.graphics.draw(surf_middle, 0, 0)
  love.graphics.setShader()

  
  love.graphics.setShader(shader_BG)
    love.graphics.draw(surf_BG, 0, 0)
  love.graphics.setShader() 
  
  -- turrican -----------------------------------------------------  
  turrican:draw()  
  
  
  balls:draw()
  
  
  -- draw masks ----------------------------------------------------------------------
  love.graphics.setColor(0,0,0,1)
  love.graphics.rectangle("fill", 0, 0, width*0.1, height)
  love.graphics.rectangle("fill", width*0.9, 0, width*0.1, height)
  
 
  scroller:draw()
end

return scene
