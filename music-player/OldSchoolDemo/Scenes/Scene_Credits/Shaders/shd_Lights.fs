extern vec2 light_coords;
extern float radius;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{ 
  vec2 normLight    = light_coords / love_ScreenSize.xy*2;
  texture_coords.x *= love_ScreenSize.x / love_ScreenSize.y;
  texture_coords.x -= 0.5;
  vec4 tex_color    = Texel(texture, normLight);
  float d           = distance(texture_coords, normLight);
  float attenuation = 5000/pow(d, 2);
  float m           = smoothstep(0, radius, d);
  float v = 1-m-attenuation/1000;
  tex_color += v;
  tex_color.a = m;
  
  return tex_color;
}