vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{   
  vec4 tex_color    = Texel(texture, texture_coords);
  float d           = distance(texture_coords, vec2(0.5, 0.5));
  float m           = smoothstep(0, 0.6, d)*1.2;  //1.8
  tex_color.a       = 1-m*(m* sin(texture_coords.y));
  
  if(tex_color.r==0 && tex_color.g==0 && tex_color.b==0)
  {
    tex_color.a = 0;
  }
  
  return tex_color;
}