extern float time;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{  
  vec4 tex_color = vec4(0.0, 0.0, 0.0, 1.0);
  float h2  = 0.8;
  float h3 = h2+0.025+((cos(time/10)/2)+0.5)*0.01;
  
  //float h3  = h2+0.052;
  
  //float h2  = 0.768;
  //float h3  = 0.96;
  //float h3  = 0.82;
  
  float pi  = 3.14159;
  float t   = time/16;
  if(texture_coords.y>=h3)
  {    
    //tex_color.r =     mod( (texture_coords.x   * 8) + texture_coords.y*sin(texture_coords.y * 60 + t) + t / 1.8, 1.0);
    //tex_color.g = 1 - mod( (texture_coords.x   * 8) + texture_coords.y*sin(texture_coords.y * 60 + t) + t / 1.8, 1.0);
    tex_color.b =     mod( (texture_coords.x/2 * 8) + texture_coords.y*sin(texture_coords.y * 60 + t) + t / 1.8, 1.0);
    //tex_color.a = texture_coords.y*0.1;    
    
  }
  else if(texture_coords.y>h2)
  {
    tex_color.g = mod(((sin(texture_coords.y-h2)/2)+0.5), 0.1);
    tex_color.b = 1.0-sin(texture_coords.y-h2)*(pi*7);
  }
  
  float d           = distance(texture_coords, vec2(0.5, 0.5));
  float m           = smoothstep(0, 0.6, d)*1.2;
  tex_color.a       = 1-m*(m* sin(texture_coords.y));
  
  if(tex_color.r<=0 && tex_color.g<=0 && tex_color.b<=0)
  {
    tex_color.a = 0;
  }
  
  return tex_color;
}