//extern float time = 0;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{    
  //float t=time/10;
  vec4 tex_color = Texel(texture, texture_coords);
  
  float h  = 0.2;
  float h1 = 0.57;
  float h2 = h1+0.2;
  
  if(texture_coords.y>h1 && texture_coords.y<h2)
  {
    tex_color.b = cos(1- h1-texture_coords.y*8);
    tex_color.g = cos(1- h1-texture_coords.y*16);
    tex_color.a = tex_color.b;
  }
  else if(texture_coords.y<h)
  {  
    tex_color.r = 1-sin(1-texture_coords.y*8);
    tex_color.g = sin(1-texture_coords.y*16);
    tex_color.b = 1-texture_coords.y*6;
    tex_color.a = tex_color.b;
    
    float d = distance(vec2(texture_coords.x, 0), vec2(0.5, h-0.05));
    float m = smoothstep(tex_color.a, 0, d);
    tex_color.a = 2*m;
    tex_color.r -= tex_color.a;
  }
  
  
  
  return tex_color;
}