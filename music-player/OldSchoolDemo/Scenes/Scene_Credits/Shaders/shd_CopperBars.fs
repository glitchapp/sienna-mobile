extern float time = 0;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{    
  float t=time/100;
  vec4 tex_color = Texel(texture, texture_coords);
  
  float h  = 0.2;
  float h1 = 0.55;
  float h2 = h1+0.18;
  float pi = 3.14159;
  
  
  
  if(texture_coords.y<h2)
  {
    tex_color.g = (sin ( cos( sin(texture_coords.y+texture_coords.y)*90+t)))/2.0+0.5;
    tex_color.b = 1-(sin ( cos( sin(texture_coords.y+texture_coords.y*10-sin(t/2))*60+t)))/2.0+0.5;
    tex_color.a = sin(texture_coords.y);
    
    float d = distance(vec2(texture_coords.x, 0), vec2(0.5, 0.5))/1.5;
    float m = smoothstep(tex_color.a, 0, d);
    tex_color.a = m;
    
  }  
  
  return tex_color;
}