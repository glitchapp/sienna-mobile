extern float time;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{    
  vec4 tex_color = Texel(texture, texture_coords);
  
  if(texture_coords.y<0.70)
  {
  
  float d = distance(texture_coords, vec2(0.5, 0.35))*200*sin(time/1000);
  float c = sin((time/10)-d);
  float m = smoothstep(0, c, d)*1.2;
  float a = (sin ( cos( (texture_coords.y/d)*110) ) )/8;
  
  tex_color = vec4(c+sin(time/100 - m), c, m, 1-d/16); //c+sin(time/100 - m)
  
  }
  
  return tex_color;
}