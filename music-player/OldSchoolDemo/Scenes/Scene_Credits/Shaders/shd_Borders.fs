extern float time;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{    
  vec4 tex_color = Texel(texture, texture_coords);
  
  if(texture_coords.x < 0.1)
  {
    tex_color.g = (sin ( cos( (texture_coords.y*10)*90+(time/100))))/2.0;
    tex_color.a = (sin(texture_coords.y*4));
  }
  else if(texture_coords.x > 0.9)
  {
    tex_color.g = (sin ( cos( (texture_coords.y*10)*90+(time/100))))/2.0;
    tex_color.a = (sin(texture_coords.y*4));
  }
  
  return tex_color;
}