extern float time = 0;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{  
  float t = time/100.0;  
  texture_coords.x = mod(texture_coords.x + t, 1.0);
  vec4 tex_color = Texel(texture, texture_coords);
  
  return tex_color;  
}