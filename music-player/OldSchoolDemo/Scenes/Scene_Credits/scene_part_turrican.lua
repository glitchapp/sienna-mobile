local scene_part = {}

scene_part.isLoaded   = false
scene_part.isFinished = false

-- modules ------------------------------------
local libSprites = require("OldSchoolDemo/Modules/libSprites")

-- scene specific declarations
local width  = 0
local height = 0

-- shaders ---------------------------------------
local shader_ground  = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Credits/Shaders/shd_MovingGround.fs")
local shader_ground2 = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Credits/Shaders/shd_Ground_lighting.fs")
local shader_water   = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Credits/Shaders/shd_Running_water.fs")

local surf_sprites = nil
local surf_water   = nil

-- sprites ------------------------------------------
local sprite_scale = 2
local lstSprites   = {}
local turrican     = nil
local tur_x        = 0
local tur_y        = 0

local enemy        = nil
local enemy_x      = 0
local enemy_y      = 0

-- textures ------------------------------------------------------
local texture_ground = love.graphics.newImage("OldSchoolDemo/Scenes/Scene_Credits/Images/spr_ground.png")
local groundX        = 0
local groundY        = 0
local ground_w       = texture_ground:getWidth()
local ground_h       = texture_ground:getHeight()

-- angles ---------------------------------------------------------
local time = 0

function scene_part:initialize()
  -- init screen -------------------------------------------------
  width=screenWidth
  height=screenHeight
  
  -- create canvases
  surf_water  = love.graphics.newCanvas(width,height)
  surf_water:setFilter("nearest", "nearest")
  
  surf_sprites       = love.graphics.newCanvas(width,height)
  surf_sprites:setFilter("nearest", "nearest")
  
  -- init canvases -----------------------------------------------
  love.graphics.setCanvas(surf_sprites)
    love.graphics.setColor(0,0,0,0)
    love.graphics.rectangle("fill",0,0,width,height)
  love.graphics.setCanvas()
  
  love.graphics.setCanvas(surf_water)
    love.graphics.setColor(0,0,0,0)
    love.graphics.rectangle("fill",0,0,width,height)
  love.graphics.setCanvas()
  
  -- init sprites -------------------------------------------------
  local turricanSize = 48
  tur_x = width/2
  tur_y = height*2/3
  turrican = libSprites:newSprite(tur_x, tur_y)
  turrican.scale_x=sprite_scale
  turrican.scale_y=sprite_scale
  turrican:addAnimation("run", turricanSize, turricanSize, "OldSchoolDemo/Scenes/Scene_Credits/Images/spr_turrican1.png")
  table.insert(lstSprites, turrican)
  
  enemy_x = width*2.4/3
  enemy_y = height*1.4/3
  enemy = libSprites:newSprite(enemy_x, enemy_y)
  enemy.scale_x=sprite_scale
  enemy.scale_y=sprite_scale
  enemy:addAnimation("run", turricanSize, turricanSize, "OldSchoolDemo/Scenes/Scene_Credits/Images/spr_turrican2.png")
  --table.insert(lstSprites, enemy)
  
  -- init textures
  groundX = tur_x
  groundY = tur_y+(turricanSize*sprite_scale)*0.5+4
end

function scene_part:update(dt)
  
  -- update sprites
  local sprLen = #lstSprites  
  if sprLen>0 then
    for i=1, sprLen do
      local s = lstSprites[i]      
      s:update(dt)
    end
  end
  
  enemy.y = enemy_y + (8*math.sin(time*3) + 16*math.cos(time*7))
  
  time = time + dt
end


function scene_part:draw()
    
  -- sprites ----------------------------------------------------
  love.graphics.push()
  love.graphics.scale(0.5, 0.5)  
  love.graphics.setCanvas(surf_sprites)  
  love.graphics.clear()
  
    -- draw ground to surface -----------------------------------------------
    love.graphics.setShader(shader_ground)
      shader_ground:send("time", time*31.25)
      love.graphics.draw(texture_ground, groundX, groundY, 0, sprite_scale, sprite_scale, ground_w/2, ground_h/2)
    love.graphics.setShader()
    
    -- draw sprites to surface ---------------------------------------------------
    local sprLen = #lstSprites
    if sprLen>0 then
      for i=1, sprLen do
        local s = lstSprites[i]
        s:draw()
      end
    end
    
    love.graphics.setCanvas()
  love.graphics.pop() 
  
  -- draw ground surface ----------------------------------------------------------------
  love.graphics.setShader(shader_ground2)
    love.graphics.draw(surf_sprites, 0, 0)
  love.graphics.setShader()  
  
  -- draw running water to surface ------------------------------------------------------
  love.graphics.setShader(shader_water)
      shader_water:send("time", time*31.25)
      love.graphics.draw(surf_water,0,0)
  love.graphics.setShader()
  
  love.graphics.setColor(1,1,1,1)
end

return scene_part
