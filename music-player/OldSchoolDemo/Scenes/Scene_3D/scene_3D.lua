local scene = {}

scene.isLoaded   = false
scene.isFinished = false

-- canvases ---------------------------------
local surf_bg    = nil
local surf_stars = nil

-- libs -------------------------------------
local lib3D = require("OldSchoolDemo/Modules/lib3D")

-- fade -------------------------------------
local fade    = nil

-- scene specific declarations --------------
local width   = 0
local height  = 0
local helpers = require("OldSchoolDemo/Modules/libHelpers")
local font    = nil

require("OldSchoolDemo/LuaFFT/luafft")

-- shaders ----------------------------------
local shader_bg      = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_3D/Shaders/shd_BG_scene3D.fs")
local shader_bg_grid = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_3D/Shaders/shd_BG_scene3D_grid.fs")
local shader_bg_vizu = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_3D/Shaders/shd_BG_scene3D_vizu.fs")
local shader_stars   = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_3D/Shaders/shd_Starfield.fs")

-- objects ----------------------------------
local cube1 = {}
local grid1 = {}
local vizu  = {}

-- state -------------------------------------
local state = "cube"

-- time --------------------------------------
local time        = 0
local currentTime = 0

-- music 
local music = nil
local musicData = nil

function scene:initialize()
  width=screenWidth
  height=screenHeight
  font= love.graphics.newImageFont('OldSchoolDemo/Scenes/Scene_3D/SpriteFonts/spr_Kromasky.png',' abcdefghijklmnopqrstuvwxyz0123456789!?:;,è./+%ç@à#')
  
  -- load canvases ----------------------------------------------
  surf_bg = love.graphics.newCanvas(width, height)
  surf_bg:setFilter("linear", "linear", 16)
  
  surf_stars = love.graphics.newCanvas(width, height)
  surf_stars:setFilter("linear", "linear", 16)
  
  -- init canvases -----------------------------------------------  
  love.graphics.setCanvas(surf_bg)
    love.graphics.setColor(0,0,0,1)
    love.graphics.rectangle("fill",0,0,width,height)
  love.graphics.setCanvas()
  
  --musicData = love.sound.newSoundData("OldSchoolDemo/Scenes/Scene_3D/Music/Romeo_Knight-Enigma Gun_Ced_edit.mp3")
  --music     = love.audio.newSource(musicData, "stream")
  --music:setLooping(false)
  --music:play()   
  
  grid1 = lib3D:newTextureGrid(80, 80, width, height, "OldSchoolDemo/Scenes/Scene_3D/images/spr_heightmap.png")
  cube1 = lib3D:newCube(width, height)
  vizu  = lib3D:newAudioVisualizer(500, 8,  width, height, music, musicData)
  fade  = require("OldSchoolDemo/Modules/libFade"):newFade(width, height)
  
end

function round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end

function scene:update(dt)
  
  if state == "cube" then
    
    if currentTime==42 then
      cube1.time  = 0
      cube1.state = "zoom" 
    end
    
    if currentTime==52.7 then fade:start("out", "in") end
    if currentTime==53 then
      time = 0
      state = "grid"
    end
    
    cube1:update(dt)
  end
  
  if state == "grid" then
    
    if currentTime==91.7 then fade:start("out", "in") end
    if currentTime==92 then
      time = 0
      state = "vizu"
    end
    
    grid1:update(dt)
  end
  
  if state == "vizu" then
    
    if currentTime==145.7 then fade:start("out", "in") end
    if currentTime==146 then
      time = 0
      state = "cube"
    end
    
    vizu:update(dt)
  end
  
  --currentTime=round(music:tell("seconds"), 1)
  
  currentTime = math.floor(mod:get_position_seconds() % 60)
  time = time + 1
  fade:update()
  
  -- change scene ---------------
  --if not music:isPlaying() then self.isFinished = true end
end

function scene:drawBG1()
  love.graphics.push()
    love.graphics.setCanvas(surf_bg)
      love.graphics.clear()
      love.graphics.scale(0.5, 0.5)      
      love.graphics.setColor(0, 0, 0, 1)      
        love.graphics.rectangle("fill", 0, 0, width, height)      
    love.graphics.setCanvas()
  love.graphics.pop()  
  
  love.graphics.setShader(shader_bg)
    love.graphics.draw(surf_bg, 0, 0)
  love.graphics.setShader()
end

function scene:drawBG2()
  love.graphics.push()
    love.graphics.setCanvas(surf_bg)
      love.graphics.clear()
      love.graphics.scale(0.5, 0.5)      
      love.graphics.setColor(0, 0, 0, 1)      
        love.graphics.rectangle("fill", 0, 0, width, height)      
    love.graphics.setCanvas()
  love.graphics.pop()  
  
  love.graphics.setShader(shader_bg_grid)
    shader_bg_grid:send("time", time)
    love.graphics.draw(surf_bg, 0, 0)
  love.graphics.setShader()
end

function scene:drawBG3()
  love.graphics.push()
    love.graphics.setCanvas(surf_bg)
      love.graphics.clear()
      love.graphics.scale(0.5, 0.5)      
      love.graphics.setColor(0, 0, 0, 1)      
        love.graphics.rectangle("fill", 0, 0, width, height)      
    love.graphics.setCanvas()
  love.graphics.pop()  
  
  love.graphics.setShader(shader_bg_vizu)
    shader_bg_vizu:send("time", time)
    love.graphics.draw(surf_bg, 0, 0)
  love.graphics.setShader()
end

function scene:draw() 
  
  if state == "cube" then
    
   scene:drawBG1()
   
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.setShader(shader_stars)
      shader_stars:send("time", time/2)
      shader_stars:send("resolution", {1, 1})
      love.graphics.draw(surf_stars, 0, 0)
    love.graphics.setShader()
    
    cube1:draw()  
    
    if math.abs(cube1.zoom) <= 2 then
      love.graphics.setColor(1, 1, 1, 1)
      love.graphics.rectangle("fill", 0, 0, width, height)
    end
     
  elseif state == "grid" then
    
    scene:drawBG2()
    
    grid1:draw()
    
  elseif state == "vizu" then
    
    scene:drawBG3()
    
    vizu:draw()
    
  end
  
  fade:draw()
end

return scene
