extern float time;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{  
  float t = time/50.0;
  float t2 = 45.0;
  
  vec4 tex_color = Texel(texture, texture_coords);
  
  float d = distance(texture_coords, vec2(0.5, 0.5))/2.0;
  
  tex_color.b  = mod((d*t2+sin(t2/3.0)) - t/3, 1.0);
  tex_color.g  = mod((d*t2/5*5+sin(t2/3.0)) - t/3, 0.5);
  tex_color.r  = 1-(tex_color.b);  
  
  tex_color.a = d*2;
  
  return tex_color;  
}