float time = 1200;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{  
  vec4 tex_color = Texel(texture, texture_coords);
  
  float d = distance(texture_coords, vec2(0.5, 0.45))/2;
  tex_color.a *= 1-smoothstep(tex_color.a, tex_color.r/d, d*(sin(time*0.1)/2)+0.5);
  tex_color.r  = d/2;
  tex_color.b  = d;
  
  return tex_color;  
}