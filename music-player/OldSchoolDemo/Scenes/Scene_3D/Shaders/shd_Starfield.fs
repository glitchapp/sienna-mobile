uniform float time;
uniform vec2 resolution;

float h(float i){
	return fract(pow(3., sqrt(i/2.)));
}

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{
	vec2 p = (texture_coords.xy*2.-resolution);
	
	float a = floor(degrees(4.+atan(p.y,p.x))*2.)/4.;
	
	float d = pow(2.,-10.*fract(0.1*time*(h(a+.5)*-.1-.1)-h(a)*1000.));
	
	if(abs(length(p)-d*length(resolution)) < d*0.35)
  {
		return vec4(d*(h(a+.5)*3.));
	}else
  {
		return vec4(0.);
	}
}