vec4 resultCol;
extern vec2 stepSize;
extern float time;

vec4 effect( vec4 col, Image texture, vec2 texturePos, vec2 screenPos )
{
	// get color of pixels:
	number alpha = 4*texture2D( texture, texturePos ).a;
	alpha -= texture2D( texture, texturePos + vec2( stepSize.x, 0.0f ) ).a;
	alpha -= texture2D( texture, texturePos + vec2( -stepSize.x, 0.0f ) ).a;
	alpha -= texture2D( texture, texturePos + vec2( 0.0f, stepSize.y ) ).a;
	alpha -= texture2D( texture, texturePos + vec2( 0.0f, -stepSize.y ) ).a;

	// calculate resulting color
  float r = (sin(time/100)/1.2)+0.5;
  float g = 1.0-(sin(time/100)/1.8)+0.5;
  float b = 0.25;
	resultCol = vec4( r, 0, 0, alpha );
	// return color for current pixel
	return resultCol;
}