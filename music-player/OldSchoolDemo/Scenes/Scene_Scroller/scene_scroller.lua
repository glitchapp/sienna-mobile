local scene = {}

scene.isLoaded   = false
scene.isFinished = false

-- scene specific declarations
local width       = 0
local height      = 0
local font        = nil
local helpers     = require("OldSchoolDemo/Modules/libHelpers")
local libStrings  = require("OldSchoolDemo/Modules/libStrings")
local libTex      = require("OldSchoolDemo/Modules/libTextures")
local libText     = require("OldSchoolDemo/Modules/libText")

local shader_blur     = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Scroller/Shaders/shd_GaussianBlur.fs")
local shader_mirror   = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Scroller/Shaders/shd_Mirror.fs")
local shader_outline  = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Scroller/Shaders/shd_Outline.fs")
local shader_wave     = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Scroller/Shaders/shd_Wave.fs")
local shader_BG       = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Scroller/Shaders/shd_MainBGScroller.fs")
local shader_pixelate = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Scroller/Shaders/shd_Pixelate.fs")
local shader_persp    = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Scroller/Shaders/shd_Perspective.fs")

local surf_BG     = nil
local surf_mirror = nil
local surf_scroll = nil
local vignette    = nil

local shadow_intensity = 0.5
local shadow_offset    = 4

-- images
local img_glasses = love.graphics.newImage("OldSchoolDemo/Scenes/Scene_Scroller/Images/spr_Glasses.png")
      img_glasses:setFilter("nearest", "nearest", 16)
      
local glasses_x   = 0
local glasses_y   = 0
local glassesDisplacement = 32.0

-- scroll text -------------------
local scrollTime  = 0
local bgTime      = 0
local logoTime    = 0
local text        = "@@ greetings! @@ ...          ... first of all, lets start with a scroller ...          because #every demo needs a scroller# !!!           ... thats pretty much the vanilla flavor of demos, or was ...          ... i could have gone straight with the sine scroller shader but i wanted some reflections first ...          ... this is my first real demo btw, drop me a message or a comment if you liked it, contact: https://ced30.itch.io/           ... excuse me if i broke some conventions, but @#  im having too much fun with that thing #@                                 "
local scrollText  = {}
local scrollX     = 0
local scrollY     = 0

local sineScroller = nil


-- helpers
function ccos(a) return math.cos(math.rad(a)) end
function csin(a) return math.sin(math.rad(a)) end

-- parallax
parallax={}

function scene:initialize()
  width          = screenWidth
  height         = screenHeight
  font           = love.graphics.newImageFont('OldSchoolDemo/Scenes/Scene_Scroller/SpriteFonts/spr_Kromasky.png',' abcdefghijklmnopqrstuvwxyz0123456789!?:;,è./+%ç@à#')
  surf_BG        = love.graphics.newCanvas(width,height)
  surf_mirror    = love.graphics.newCanvas(width,height)
  surf_scroll    = love.graphics.newCanvas(width,height)
  vignette       = love.graphics.newImage("OldSchoolDemo/Scenes/Scene_Scroller/Images/spr_Vignette.png")
  
  surf_BG:setFilter("linear", "linear", 16)
  surf_mirror:setFilter("linear", "linear", 16)
  surf_scroll:setFilter("linear", "linear", 16)
  
  -- initialize canvas
  love.graphics.setCanvas(surf_BG)
    love.graphics.setColor(0,0,0,0)
    love.graphics.rectangle("fill",0,0,width,height)
  love.graphics.setCanvas()
  
  love.graphics.setCanvas(surf_mirror)
    love.graphics.setColor(0,0,0,0)
    love.graphics.rectangle("fill",0,0,width,height)
  love.graphics.setCanvas()
  
  love.graphics.setCanvas(surf_scroll)
    love.graphics.setColor(0,0,0,0)
    love.graphics.rectangle("fill",0,0,width,height)
  love.graphics.setCanvas()
  
  -- init glasses coordinates
  glasses_x = (width/2)-(img_glasses:getWidth()/2)
  glasses_y = (height*(1/10))
  
  -- init scroll text
  scrollText = libStrings:toArray(text)
  scrollX    = width/2
  scrollY    = (height*2.2/3)
  
  -- parallax
  local prefix="OldSchoolDemo/Scenes/Scene_Scroller/Images/Parallax/"
  local layers={prefix.."spr_layer6d.png", prefix.."spr_layer5d.png", prefix.."spr_layer4d.png", prefix.."spr_layer3d.png", prefix.."spr_layer2d.png", prefix.."spr_layer1d.png"}
  
  parallax = libTex:newParallax(layers, 0, height*1.8/3)
  
  -- sine scroller -----------
  sineScroller = libText:newScroller(text, font, width, scrollY)
  
  --music = love.audio.newSource("OldSchoolDemo/Scenes/Scene_Scroller/Music/4Mat-AnarchyMenu01.mp3", "stream")
  --music:setLooping(false)
  --music:play()
end

function scene:update(dt)
  
  -- parallax
  for i=1, #parallax do
    local p=parallax[i]
    p.x=p.x-(1*i*0.75)
    if p.x+p.w < 0 then
      p.x = width
    end
  end  
  
  sineScroller:update(dt)
  
  scrollX    = (width/2) + (40 * ((csin(scrollTime)/2)+0.5))  
  bgTime     = bgTime+0.1
  logoTime   = logoTime+1
  scrollTime = scrollTime+1 
  
  -- change scene ---------------
  --if not music:isPlaying() then self.isFinished = true end
end


function scene:draw()  
  
  -- draw BG -------------------------------------------------
  love.graphics.setShader(shader_BG)
    shader_BG:send("time", bgTime)
    love.graphics.draw(surf_BG,0,0)
  love.graphics.setShader()
  
  
  -- draw parallax shadows -------------------------------------------------
  local pOffset=9
  love.graphics.setColor(0,0,0,shadow_intensity)
  
  for i=1, #parallax do      
      love.graphics.draw(parallax[i].img, parallax[i].x + shadow_offset, parallax[i].y-parallax[i].offset_y + shadow_offset)
    if parallax[i].x<0 then
        love.graphics.draw(parallax[i].img, parallax[i].x+parallax[i].w + shadow_offset, parallax[i].y-parallax[i].offset_y + shadow_offset)
    end      
    if parallax[i].x>=0 then
        love.graphics.draw(parallax[i].img, parallax[i].x-parallax[i].w + shadow_offset, parallax[i].y-parallax[i].offset_y + shadow_offset)
    end
  end
  
  
  -- draw parallax ---------------------------------------------------------
  love.graphics.setColor(1,1,1,1)
  love.graphics.setShader(shader_blur)
  shader_blur:send("blurSize", 0.001)    
    for i=1, #parallax do
      love.graphics.draw(parallax[i].img, parallax[i].x, parallax[i].y-parallax[i].offset_y)      
      if parallax[i].x<0 then
        love.graphics.draw(parallax[i].img, parallax[i].x+parallax[i].w, parallax[i].y-parallax[i].offset_y)
      end    
      if parallax[i].x>=0 then
        love.graphics.draw(parallax[i].img, parallax[i].x-parallax[i].w, parallax[i].y-parallax[i].offset_y)
      end
    end
    
  love.graphics.setShader()
  
 
  -- draw logo ---------------------------------------------------------------------
  
  love.graphics.setShader(shader_persp)
  -- draw logo shadow
  love.graphics.setColor(0,0,0,shadow_intensity)
  love.graphics.draw(img_glasses, glasses_x+shadow_offset, glasses_y+shadow_offset)
  
  -- draw logo texture
  love.graphics.setColor(1,1,1,1 )
  love.graphics.draw(img_glasses, glasses_x, glasses_y)
  love.graphics.setShader()
  
  -- draw logo outline
  love.graphics.setShader(shader_outline)
    shader_outline:send( "stepSize", {1/img_glasses:getWidth(), 1/img_glasses:getHeight()})
    shader_outline:send( "time", 0)
  love.graphics.draw(img_glasses, glasses_x, glasses_y)
  love.graphics.setShader()
  
  sineScroller:draw()
  
  love.graphics.setShader(shader_mirror)
    shader_mirror:send("_tex", sineScroller.surface)
    love.graphics.draw(surf_mirror,0,height*2.6/4)
  love.graphics.setShader() 
end

return scene
