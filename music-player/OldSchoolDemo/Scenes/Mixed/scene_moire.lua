local scene = {}

scene.isLoaded   = false
scene.isFinished = false

-- scene specific declarations
local width       = 0
local height      = 0
local font        = nil

-- libs -------------
local helpers     = require("OldSchoolDemo/Modules/libHelpers")
local libStrings  = require("OldSchoolDemo/Modules/libStrings")
local libText     = require("OldSchoolDemo/Modules/libText")

-- shaders ----------------------------------------
local shader_wave  = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Moire/Shaders/shd_Scanlines_ext.fs")
local shader_moire = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Moire/Shaders/shd_Moire.fs")
local shader_bg1   = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Moire/Shaders/shd_Moire_bg1.fs")
local shader_alpha = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Moire/Shaders/shd_Radial_alpha.fs")

-- surfaces ------------------------------
local surf_moire  = nil
local surf_moire2 = nil
local surf_bg1    = nil
local surf_bg2    = nil

local t           = 0
local str1        = "heres a < moire pattern > and a funky background shader,                     ... another oldie but goodie! ...                    ... first time i seen this pattern, it was on amiga ( a long time ago )...                    ... ive spent a bit of time, figuring how its done ...                   ... and < voila > ...                    ... i hope you like what you see! ...                    ... it took me a while getting a background i liked ...                    ... im not toally satisfied, but itll do! ...                    "
local scrollText1 = {}

local music = nil

-- helpers
function ccos(a) return math.cos(math.rad(a)) end
function csin(a) return math.sin(math.rad(a)) end

function scene:initialize()
  width           = screenWidth
  height          = screenHeight
  font            = love.graphics.newImageFont('OldSchoolDemo/Scenes/Scene_Moire/SpriteFonts/spr_fontOldSchool_32_30.png',' abcdefghijklmnopqrstuvwxyz0123456789?=+-/%(),.é#@$^<>')
  
  -- surfaces ----------------------------------------------
  local moire_w = width*0.8
  surf_moire      = love.graphics.newCanvas(moire_w, height)
  surf_moire2     = love.graphics.newCanvas(moire_w,height)
  surf_bg1        = love.graphics.newCanvas(width, height)
  surf_bg2        = love.graphics.newCanvas(width, height)
  
  -- initialize canvas
  surf_moire:setFilter("nearest", "nearest", 16)
  surf_moire2:setFilter("nearest", "nearest", 16)
  surf_bg1:setFilter("nearest", "nearest", 16)
  surf_bg2:setFilter("nearest", "nearest", 16)
  
  love.graphics.setCanvas(surf_moire)
    love.graphics.setColor(0,0,0,1)
    love.graphics.rectangle("fill",0,0,width*0.8,height)
  love.graphics.setCanvas()
  
  love.graphics.setCanvas(surf_moire2)
    love.graphics.setColor(0,0,0,1)
    love.graphics.rectangle("fill",0,0,width*0.8,height)
  love.graphics.setCanvas()
  
  love.graphics.setCanvas(surf_bg1)
    love.graphics.setColor(0,0,0,1)
    love.graphics.rectangle("fill",0,0,width,height)
  love.graphics.setCanvas()
  
  love.graphics.setCanvas(surf_bg2)
    love.graphics.setColor(0,0,0,1)
    love.graphics.rectangle("fill",0,0,width,height)
  love.graphics.setCanvas()
  
  -- init scroll text
  scrollText1      = libText:newRibbonText(str1, font, width, height*2.9/3)  
  
  --music = love.audio.newSource("Scenes/Scene_Moire/Music/JogeirLiljedahl-HybrisLight.mp3", "stream")
  --music:setLooping(false)
  --music:play()
end


function scene:update(dt)
  scrollText1:update(dt)
  t=t+1
  -- change scene ---------------
  if not music:isPlaying() then self.isFinished = true end
end


function scene:draw()
  
  love.graphics.setColor(1, 1, 1, 1)
  
  love.graphics.setShader(shader_bg1)
    shader_bg1:send("time", t/100)
    love.graphics.draw(surf_bg1, 0, 0)
  love.graphics.setShader()
  
  -- draw canvas
  love.graphics.push()
  love.graphics.setCanvas(surf_moire2)
    love.graphics.scale(0.5, 0.5)
    love.graphics.clear()
    love.graphics.setShader(shader_moire)
      shader_moire:send("time", t/100)
      love.graphics.draw(surf_moire,0,0)
    love.graphics.setShader()
  love.graphics.setCanvas()
  love.graphics.pop()
  
  love.graphics.setShader(shader_alpha)
    love.graphics.draw(surf_moire2, width*0.1, 0)
  love.graphics.setShader()
  
  scrollText1:draw()
  
  -- set color back to white
  love.graphics.setColor(1,1,1,1)
end

return scene
