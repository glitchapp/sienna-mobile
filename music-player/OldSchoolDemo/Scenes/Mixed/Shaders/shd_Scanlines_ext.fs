//extern number time;
    vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords ){
      vec4 pixel = Texel(texture, texture_coords );//This is the current pixel color
      
      //Creates time-dependent sine modulation effect
      number noise = 1;
      number height = floor(texture_coords.y * 720);
      
      //Divide the image into two scan regions
      if (mod(height, 2) != 0){
        
        return Texel(texture, texture_coords) * color;
        } else {
        pixel.r = pixel.r * abs(floor(sin(0) * 4) / 4);
        pixel.b = 0.5 * pixel.b;
        return pixel;
      }
    }