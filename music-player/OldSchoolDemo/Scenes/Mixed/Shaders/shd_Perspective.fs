

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{    
  float size = 2.0;
  texture_coords.x *= max(1, size / screen_coords.y);
  //texture_coords.x -= lerp(1/size,0,texture_coords.y);
  vec4 pixel = Texel(texture, texture_coords);
  return pixel * color;
}