extern float time;
extern float displacement;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{  
  texture_coords.x += (sin(texture_coords.y*displacement+time/10)*0.05)*(1-((texture_coords.y+1)))*0.5;
  vec4 tex_color = Texel(texture, texture_coords);
  
  float alpha = tex_color.a;
  
  float maxColor=(sin(texture_coords.y*displacement+time/10)/3)+0.5;  
  if(alpha!=0.0)
  {
    float alpha1 = (sin(texture_coords.x*displacement)/3)+0.5;
    float alpha2 = 1-(sin(texture_coords.y*displacement)/3)+0.5;
    alpha  = (alpha1 + alpha2)/2;
  }
  
  return vec4(tex_color.r*maxColor, tex_color.g*maxColor, tex_color.b*maxColor, alpha);  
}