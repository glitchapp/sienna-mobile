extern number blurSize;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{
  vec4 tex_color = Texel(texture, texture_coords);
    vec4 sum = vec4(0.0);    
    // take nine samples, with the distance blurSize between them
    sum += texture2D(texture, vec2(texture_coords.x - 4.0*blurSize, texture_coords.y)) * 0.05;
    sum += texture2D(texture, vec2(texture_coords.x - 3.0*blurSize, texture_coords.y)) * 0.09;
    sum += texture2D(texture, vec2(texture_coords.x - 2.0*blurSize, texture_coords.y)) * 0.12;
    sum += texture2D(texture, vec2(texture_coords.x - blurSize, texture_coords.y)) * 0.15;
    sum += texture2D(texture, vec2(texture_coords.x, texture_coords.y)) * 0.16;
    sum += texture2D(texture, vec2(texture_coords.x + blurSize, texture_coords.y)) * 0.15;
    sum += texture2D(texture, vec2(texture_coords.x + 2.0*blurSize, texture_coords.y)) * 0.12;
    sum += texture2D(texture, vec2(texture_coords.x + 3.0*blurSize, texture_coords.y)) * 0.09;
    sum += texture2D(texture, vec2(texture_coords.x + 4.0*blurSize, texture_coords.y)) * 0.05;  
  return sum;
}