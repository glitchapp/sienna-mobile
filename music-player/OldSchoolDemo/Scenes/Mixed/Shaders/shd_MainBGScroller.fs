extern float time;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{
  vec4 tex_color = Texel(texture, texture_coords);
  float a  = 1;
  float r  = 0;
  float g  = 0;
  float b  = 0.0;
  float pi = 3.14159;
  float h  = 0.70;
  
  if(( texture_coords.y<=0.006) ||
     (texture_coords.y>0.995))
  {  
    r = (sin(texture_coords.y+time)/2)+0.5;
    g = 1.0-(sin(texture_coords.x+time)/2)+0.5;
    b = 0.5;
  }
  else if(texture_coords.y>h)
  {
    r = 1.0-((sin(texture_coords.y-h)/2)+0.5)*1.6;
    g = (sin ( cos( (texture_coords.y/5)*90-(time/10))))/2.0;
    b = 1.0-sin(texture_coords.y-h)*(pi*1.8);
  }
  else
  {    
    r = mod((sin(texture_coords.y*10)/2)+0.5, 0.2);
    g = (sin ( cos( (texture_coords.y/5)*60+(time/10))))/2.0;
    b = sin(texture_coords.y*texture_coords.y)*pi;
    a = (sin(texture_coords.y*4));
    
    //g = (sin ( cos( (texture_coords.y/5)*90+(time/10))))/2.0;
    
    //g = (sin(texture_coords.y*(texture_coords.y*sin(cos(texture_coords.y*time*10.0)))));
    //g = (sin(texture_coords.y*texture_coords.y)+sin(cos(texture_coords.y*time*1.0)))/2.0;
    //g = (sin(cos(texture_coords.y/5*90-time)))/2.0;
    
    // sin(texture_coords.x*texture_coords.x)+
    // *time*100.0
  }
  
  //if(mod(texture_coords.y, 0.01)<0.002)
  //{
  
  //a = 0.99;
  //}
  
  return vec4(r, g, b, a);
}