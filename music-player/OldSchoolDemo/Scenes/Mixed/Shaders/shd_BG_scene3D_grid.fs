extern float time = 1200;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{  
  
  float d = distance(texture_coords, vec2(0.5, 0.5));
  float v = (sin(distance(texture_coords, vec2(0.5, 0.5))+time/500)/2)+0.5; // -90
  float a = 1-(texture_coords.y);
  
  return vec4(1-d, sin(v), 1-v, a);  
}