extern Image _tex;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{  
  float h = 0.75;
  vec2 newCoords = vec2(texture_coords.x,1.0-(texture_coords.y));
  
  vec4 tex_color = Texel(_tex, newCoords);
  
  float new_a = 0.0;
  if(tex_color.a!=0)
  {
    new_a = tex_color.a - 0.6;
  } 
  
  return vec4(tex_color.r, tex_color.g, tex_color.b, new_a);
}