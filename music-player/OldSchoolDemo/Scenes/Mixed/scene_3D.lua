local scene = {}

scene.isLoaded   = false
scene.isFinished = false

-- canvases ---------------------------------
local surf_bg    = nil
local surf_stars = nil

-- scene specific declarations
local width       = 0
local height      = 0
local font        = nil

-- libs -------------------------------------
local lib3D = require("OldSchoolDemo/Modules/lib3D")
local helpers     = require("OldSchoolDemo/Modules/libHelpers")
local libStrings  = require("OldSchoolDemo/Modules/libStrings")
local libText     = require("OldSchoolDemo/Modules/libText")


-- fade -------------------------------------
local fade    = nil

-- scene specific declarations --------------
local width   = 0
local height  = 0
local helpers = require("OldSchoolDemo/Modules/libHelpers")
local font    = nil

require("OldSchoolDemo/LuaFFT/luafft")

-- shaders ----------------------------------
local shader_bg      = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_3D/Shaders/shd_BG_scene3D.fs")
local shader_bg_grid = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_3D/Shaders/shd_BG_scene3D_grid.fs")
local shader_bg_vizu = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_3D/Shaders/shd_BG_scene3D_vizu.fs")
local shader_stars   = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_3D/Shaders/shd_Starfield.fs")

-- shaders ----------------------------------------
local shader_wave  = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Moire/Shaders/shd_Scanlines_ext.fs")
local shader_moire = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Moire/Shaders/shd_Moire.fs")
local shader_bg1   = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Moire/Shaders/shd_Moire_bg1.fs")
local shader_alpha = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Moire/Shaders/shd_Radial_alpha.fs")

-- libs -------------
local helpers     = require("OldSchoolDemo/Modules/libHelpers")
local libStrings  = require("OldSchoolDemo/Modules/libStrings")
local libText     = require("OldSchoolDemo/Modules/libText")

-- surfaces ------------------------------
local surf_moire  = nil
local surf_moire2 = nil
local surf_bg1    = nil
local surf_bg2    = nil

-- objects ----------------------------------
local cube1 = {}
local grid1 = {}
local vizu  = {}

-- state -------------------------------------
local state = "cube"

-- time --------------------------------------
local time        = 0
local currentTime = 0

-- music 
local music = nil
local musicData = nil


local t           = 0
local str1        = "heres a < moire pattern > and a funky background shader,                     ... another oldie but goodie! ...                    ... first time i seen this pattern, it was on amiga ( a long time ago )...                    ... ive spent a bit of time, figuring how its done ...                   ... and < voila > ...                    ... i hope you like what you see! ...                    ... it took me a while getting a background i liked ...                    ... im not toally satisfied, but itll do! ...                    "
local scrollText1 = {}

local music = nil

-- helpers
function ccos(a) return math.cos(math.rad(a)) end
function csin(a) return math.sin(math.rad(a)) end

--scroller

local scene = {}

scene.isLoaded   = false
scene.isFinished = false

-- scene specific declarations
local width       = 0
local height      = 0
local font        = nil
local helpers     = require("OldSchoolDemo/Modules/libHelpers")
local libStrings  = require("OldSchoolDemo/Modules/libStrings")
local libTex      = require("OldSchoolDemo/Modules/libTextures")
local libText     = require("OldSchoolDemo/Modules/libText")

local shader_blur     = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Scroller/Shaders/shd_GaussianBlur.fs")
local shader_mirror   = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Scroller/Shaders/shd_Mirror.fs")
local shader_outline  = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Scroller/Shaders/shd_Outline.fs")
local shader_wave     = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Scroller/Shaders/shd_Wave.fs")
local shader_BG       = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Scroller/Shaders/shd_MainBGScroller.fs")
local shader_pixelate = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Scroller/Shaders/shd_Pixelate.fs")
local shader_persp    = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Scroller/Shaders/shd_Perspective.fs")

local surf_BG     = nil
local surf_mirror = nil
local surf_scroll = nil
local vignette    = nil

local shadow_intensity = 0.5
local shadow_offset    = 4

-- images
local img_glasses = love.graphics.newImage("OldSchoolDemo/Scenes/Scene_Scroller/Images/spr_Glasses.png")
      img_glasses:setFilter("nearest", "nearest", 16)
      
local glasses_x   = 0
local glasses_y   = 0
local glassesDisplacement = 32.0

-- scroll text -------------------
local scrollTime  = 0
local bgTime      = 0
local logoTime    = 0
--local text        = "@@ greetings! @@ ...          ... first of all, lets start with a scroller ...          because #every demo needs a scroller# !!!           ... thats pretty much the vanilla flavor of demos, or was ...          ... i could have gone straight with the sine scroller shader but i wanted some reflections first ...          ... this is my first real demo btw, drop me a message or a comment if you liked it, contact: https://ced30.itch.io/           ... excuse me if i broke some conventions, but @#  im having too much fun with that thing #@                                 "
local text        = "@@ greetings fellow gamers and developers! @@ ...          ... this is a fork of the great old school demo created by ced 30, https://ced30.itch.io/oldscool-demo-lua-love2d-glsl...	          ...I wanted to see how would several scene mix ...  			...it turns out it looks nice and beautiful!...				...I've been playing with some parameters and mixing shaders and scenes...  		... suprisingly it performs well besides the many shaders and elements running simultaneously...				..I hope you enjoy it as much as I did mixing the elements..                                   "
local scrollText  = {}
local scrollX     = 0
local scrollY     = 0

local sineScroller = nil


-- helpers
function ccos(a) return math.cos(math.rad(a)) end
function csin(a) return math.sin(math.rad(a)) end

-- parallax
parallax={}

function scene:initialize()
  width=screenWidth
  height=screenHeight
  font= love.graphics.newImageFont('OldSchoolDemo/Scenes/Scene_3D/SpriteFonts/spr_Kromasky.png',' abcdefghijklmnopqrstuvwxyz0123456789!?:;,è./+%ç@à#')
  
  -- load canvases ----------------------------------------------
  surf_bg = love.graphics.newCanvas(width, height)
  surf_bg:setFilter("linear", "linear", 16)
  
  surf_stars = love.graphics.newCanvas(width, height)
  surf_stars:setFilter("linear", "linear", 16)
  
  -- init canvases -----------------------------------------------  
  love.graphics.setCanvas(surf_bg)
    love.graphics.setColor(0,0,0,1)
    love.graphics.rectangle("fill",0,0,width,height)
  love.graphics.setCanvas()
  
  --musicData = love.sound.newSoundData("Scenes/Scene_3D/Music/Romeo_Knight-Enigma Gun_Ced_edit.mp3")
  --musicData = love.sound.newSoundData("Scenes/Scene_Moire/Music/JogeirLiljedahl-HybrisLight.mp3")
  --music     = love.audio.newSource(musicData, "stream")
  --music:setLooping(false)
  --music:play()   
  
  grid1 = lib3D:newTextureGrid(80, 80, width, height, "OldSchoolDemo/Scenes/Scene_3D/images/spr_heightmap.png")
  cube1 = lib3D:newCube(width, height)
  vizu  = lib3D:newAudioVisualizer(500, 8,  width, height, music, musicData)
  fade  = require("OldSchoolDemo/OldSchoolDemo/Modules/libFade"):newFade(width, height)
  
  width           = screenWidth
  height          = screenHeight
  font            = love.graphics.newImageFont('OldSchoolDemo/Scenes/Scene_Moire/SpriteFonts/spr_fontOldSchool_32_30.png',' abcdefghijklmnopqrstuvwxyz0123456789?=+-/%(),.é#@$^<>')
  
  -- surfaces ----------------------------------------------
  local moire_w = width*0.8
  surf_moire      = love.graphics.newCanvas(moire_w, height)
  surf_moire2     = love.graphics.newCanvas(moire_w,height)
  surf_bg1        = love.graphics.newCanvas(width, height)
  surf_bg2        = love.graphics.newCanvas(width, height)
  
  -- initialize canvas
  surf_moire:setFilter("nearest", "nearest", 16)
  surf_moire2:setFilter("nearest", "nearest", 16)
  surf_bg1:setFilter("nearest", "nearest", 16)
  surf_bg2:setFilter("nearest", "nearest", 16)
  
  love.graphics.setCanvas(surf_moire)
    love.graphics.setColor(0,0,0,1)
    love.graphics.rectangle("fill",0,0,width*0.8,height)
  love.graphics.setCanvas()
  
  love.graphics.setCanvas(surf_moire2)
    love.graphics.setColor(0,0,0,1)
    love.graphics.rectangle("fill",0,0,width*0.8,height)
  love.graphics.setCanvas()
  
  love.graphics.setCanvas(surf_bg1)
    love.graphics.setColor(0,0,0,1)
    love.graphics.rectangle("fill",0,0,width,height)
  love.graphics.setCanvas()
  
  love.graphics.setCanvas(surf_bg2)
    love.graphics.setColor(0,0,0,1)
    love.graphics.rectangle("fill",0,0,width,height)
  love.graphics.setCanvas()
  
  -- init scroll text
  scrollText1      = libText:newRibbonText(str1, font, width, height*2.9/3)  
  
  --music = love.audio.newSource("Scenes/Scene_Moire/Music/JogeirLiljedahl-HybrisLight.mp3", "stream")
  --music:setLooping(false)
  --music:play()
  
  --scroller
  
   width          = screenWidth
  height         = screenHeight
  font           = love.graphics.newImageFont('OldSchoolDemo/Scenes/Scene_Scroller/SpriteFonts/spr_Kromasky.png',' abcdefghijklmnopqrstuvwxyz0123456789!?:;,è./+%ç@à#')
  surf_BG        = love.graphics.newCanvas(width,height)
  surf_mirror    = love.graphics.newCanvas(width,height)
  surf_scroll    = love.graphics.newCanvas(width,height)
  vignette       = love.graphics.newImage("OldSchoolDemo/Scenes/Scene_Scroller/Images/spr_Vignette.png")
  
  surf_BG:setFilter("linear", "linear", 16)
  surf_mirror:setFilter("linear", "linear", 16)
  surf_scroll:setFilter("linear", "linear", 16)
  
  -- initialize canvas
  love.graphics.setCanvas(surf_BG)
    love.graphics.setColor(0,0,0,0)
    love.graphics.rectangle("fill",0,0,width,height)
  love.graphics.setCanvas()
  
  love.graphics.setCanvas(surf_mirror)
    love.graphics.setColor(0,0,0,0)
    love.graphics.rectangle("fill",0,0,width,height)
  love.graphics.setCanvas()
  
  love.graphics.setCanvas(surf_scroll)
    love.graphics.setColor(0,0,0,0)
    love.graphics.rectangle("fill",0,0,width,height)
  love.graphics.setCanvas()
  
  -- init glasses coordinates
  glasses_x = (width/2)-(img_glasses:getWidth()/2)
  glasses_y = (height*(1/10))
  
  -- init scroll text
  scrollText = libStrings:toArray(text)
  scrollX    = width/2
  scrollY    = (height*2.2/3)
  
  -- parallax
  local prefix="OldSchoolDemo/Scenes/Scene_Scroller/Images/Parallax/"
  local layers={prefix.."spr_layer6d.png", prefix.."spr_layer5d.png", prefix.."spr_layer4d.png", prefix.."spr_layer3d.png", prefix.."spr_layer2d.png", prefix.."spr_layer1d.png"}
  
  parallax = libTex:newParallax(layers, 0, height*1.8/3)
  
  -- sine scroller -----------
  sineScroller = libText:newScroller(text, font, width, scrollY)
end

function round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end

function scene:update(dt)
  
  if state == "cube" then
    
    if currentTime==42 then
      cube1.time  = 0
      cube1.state = "zoom" 
    end
    
    if currentTime==52.7 then fade:start("out", "in") end
    if currentTime==53 then
      time = 0
      state = "grid"
    end
    
    cube1:update(dt)
  end
  
  if state == "grid" then
    
    if currentTime==91.7 then fade:start("out", "in") end
    if currentTime==92 then
      time = 0
      state = "vizu"
    end
    
    grid1:update(dt)
  end
  
  if state == "vizu" then
    
    if currentTime==145.7 then fade:start("out", "in") end
    if currentTime==146 then
      time = 0
      state = "cube"
    end
    
    vizu:update(dt)
  end
  
  currentTime=round(music:tell("seconds"), 1)
  time = time + 1
  fade:update()
  
  -- change scene ---------------
  if not music:isPlaying() then self.isFinished = true end
  
   scrollText1:update(dt)
  t=t+1
  -- change scene ---------------
  if not music:isPlaying() then self.isFinished = true end
  
  --scroller
  
   -- parallax
  for i=1, #parallax do
    local p=parallax[i]
    p.x=p.x-(1*i*0.75)
    if p.x+p.w < 0 then
      p.x = width
    end
  end  
  
  sineScroller:update(dt)
  
  scrollX    = (width/2) + (40 * ((csin(scrollTime)/2)+0.5))  
  bgTime     = bgTime+0.1
  logoTime   = logoTime+1
  scrollTime = scrollTime+1 
  
end

function scene:drawBG1()
  love.graphics.push()
    love.graphics.setCanvas(surf_bg)
      love.graphics.clear()
      love.graphics.scale(0.5, 0.5)      
      love.graphics.setColor(0, 0, 0, 1)      
        love.graphics.rectangle("fill", 0, 0, width, height)      
    love.graphics.setCanvas()
  love.graphics.pop()  
  
  love.graphics.setShader(shader_bg)
    love.graphics.draw(surf_bg, 0, 0)
  love.graphics.setShader()
end

function scene:drawBG2()
  love.graphics.push()
    love.graphics.setCanvas(surf_bg)
      love.graphics.clear()
      love.graphics.scale(0.5, 0.5)      
      love.graphics.setColor(0, 0, 0, 1)      
        love.graphics.rectangle("fill", 0, 0, width, height)      
    love.graphics.setCanvas()
  love.graphics.pop()  
  
  love.graphics.setShader(shader_bg_grid)
    shader_bg_grid:send("time", time)
    love.graphics.draw(surf_bg, 0, 0)
  love.graphics.setShader()
end

function scene:drawBG3()
  love.graphics.push()
    love.graphics.setCanvas(surf_bg)
      love.graphics.clear()
      love.graphics.scale(0.5, 0.5)      
      love.graphics.setColor(0, 0, 0, 1)      
        love.graphics.rectangle("fill", 0, 0, width, height)      
    love.graphics.setCanvas()
  love.graphics.pop()  
  
  love.graphics.setShader(shader_bg_vizu)
    shader_bg_vizu:send("time", time)
    love.graphics.draw(surf_bg, 0, 0)
  love.graphics.setShader()
end

function scene:draw() 

 love.graphics.setColor(1, 1, 1, 1)
  
  love.graphics.setShader(shader_bg1)
    shader_bg1:send("time", t/100)
    love.graphics.draw(surf_bg1, 0, 0)
  love.graphics.setShader()
  
  -- draw canvas
  love.graphics.push()
  love.graphics.setCanvas(surf_moire2)
    love.graphics.scale(0.5, 0.5)
    love.graphics.clear()
    love.graphics.setShader(shader_moire)
      shader_moire:send("time", t/100)
      love.graphics.draw(surf_moire,0,0)
    love.graphics.setShader()
  love.graphics.setCanvas()
  love.graphics.pop()
  
  love.graphics.setShader(shader_alpha)
    love.graphics.draw(surf_moire2, width*0.1, 0)
  love.graphics.setShader()
  
  --scrollText1:draw()
  
  -- set color back to white
  love.graphics.setColor(1,1,1,1)
  
  if state == "cube" then
    
   scene:drawBG1()
   
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.setShader(shader_stars)
      shader_stars:send("time", time/2)
      shader_stars:send("resolution", {1, 1})
      love.graphics.draw(surf_stars, 0, 0)
    love.graphics.setShader()
    
    cube1:draw()  
    
    if math.abs(cube1.zoom) <= 2 then
      love.graphics.setColor(1, 1, 1, 1)
      love.graphics.rectangle("fill", 0, 0, width, height)
    end
     
  elseif state == "grid" then
    
    scene:drawBG2()
    
    grid1:draw()
    
  elseif state == "vizu" then
    
    scene:drawBG3()
    
    vizu:draw()
    
  end
  
  fade:draw()
  
   -- draw BG -------------------------------------------------
  love.graphics.setShader(shader_BG)
    shader_BG:send("time", bgTime)
    love.graphics.draw(surf_BG,0,0)
  love.graphics.setShader()
  
  
  -- draw parallax shadows -------------------------------------------------
  local pOffset=9
  --love.graphics.setColor(0,0,0,shadow_intensity)
  love.graphics.setColor(1,1,1,0.2)
  for i=1, #parallax do      
      --love.graphics.draw(parallax[i].img, parallax[i].x + shadow_offset, parallax[i].y-parallax[i].offset_y + shadow_offset)
    if parallax[i].x<0 then
        --love.graphics.draw(parallax[i].img, parallax[i].x+parallax[i].w + shadow_offset, parallax[i].y-parallax[i].offset_y + shadow_offset)
    end      
    if parallax[i].x>=0 then
        --love.graphics.draw(parallax[i].img, parallax[i].x-parallax[i].w + shadow_offset, parallax[i].y-parallax[i].offset_y + shadow_offset)
    end
  end
  
  
  -- draw parallax ---------------------------------------------------------
  love.graphics.setColor(1,1,1,0.2)
  love.graphics.setShader(shader_blur)
  shader_blur:send("blurSize", 0.001)    
    for i=1, #parallax do
      --love.graphics.draw(parallax[i].img, parallax[i].x, parallax[i].y-parallax[i].offset_y)      
      if parallax[i].x<0 then
--        love.graphics.draw(parallax[i].img, parallax[i].x+parallax[i].w, parallax[i].y-parallax[i].offset_y)
      end    
      if parallax[i].x>=0 then
  --      love.graphics.draw(parallax[i].img, parallax[i].x-parallax[i].w, parallax[i].y-parallax[i].offset_y)
      end
    end
    
  love.graphics.setShader()
  
 
  -- draw logo ---------------------------------------------------------------------
  --[[
  love.graphics.setShader(shader_persp)
  -- draw logo shadow
  love.graphics.setColor(0,0,0,shadow_intensity)
  love.graphics.draw(img_glasses, glasses_x+shadow_offset, glasses_y+shadow_offset)
  
  -- draw logo texture
  love.graphics.setColor(1,1,1,1 )
  love.graphics.draw(img_glasses, glasses_x, glasses_y)
  love.graphics.setShader()
  
  -- draw logo outline
  love.graphics.setShader(shader_outline)
    shader_outline:send( "stepSize", {1/img_glasses:getWidth(), 1/img_glasses:getHeight()})
    shader_outline:send( "time", 0)
  love.graphics.draw(img_glasses, glasses_x, glasses_y)
  love.graphics.setShader()
  --]]
  sineScroller:draw()
  
  love.graphics.setShader(shader_mirror)
    shader_mirror:send("_tex", sineScroller.surface)
    love.graphics.draw(surf_mirror,0,height*2.6/4)
  love.graphics.setShader() 
  
end

return scene
