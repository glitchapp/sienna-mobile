local lib = {}


function lib:newFade(pW, pH)
  local f      = {}
  
        f.width      = pW
        f.height     = pH
        f.isFinished = false
        f.state      = "in"
        f.state_pre  = "none"
        f.state_next = "none"
        f.alpha      = 1
        f.speed      = 0.2
  
  function f:lerp(initial_value, target_value, speed)
    local result = (1-speed) * initial_value + speed*target_value
    return result
  end
  
  function f:start(pState, pNext)
    if self.isFinished then
      if pState == "in" or pState == "out" then
        self.state_next = pNext
        self.isFinished = false
        self.state      = pState
      end
    end
  end
  
  function f:update()
    
    if self.state == "none" then
      if self.state_next == "in" or self.state_next == "out" then
        self:start(self.state_next, "none")        
      end
    end
    
    if self.state == "in" then
      self.alpha = self:lerp(self.alpha, 0, self.speed/10)
      if self.alpha < 0.01 then
        self.alpha      = 0
        self.isFinished = true
        self.state      = "none"
      end
    end
    
    if self.state == "out" then
      self.alpha = self:lerp(self.alpha, 1, self.speed)
      if self.alpha > 0.99 then
        self.alpha      = 1
        self.isFinished = true
        self.state      = "none"        
      end
    end
  end

  function f:draw()
    love.graphics.setColor(0, 0, 0, self.alpha)
    love.graphics.rectangle("fill", 0, 0, self.width, self.height)
  end

  return f
end

return lib