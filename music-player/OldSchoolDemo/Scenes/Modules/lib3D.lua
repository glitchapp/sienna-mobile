local lib3D={}

-- functions
function lib3D:newVertex(pX,pY,pZ)
  local v={}
        v.x=pX
        v.y=pY
        v.z=-pZ
        v.ox=v.x
        v.oy=v.y
        v.oz=v.z
        v.rx=0
        v.ry=0
        v.rz=0
        v.projX=0
        v.projY=0
        v.offset=0
        v.c=0
  return v
end

function lib3D:rotate(pX, pY, pZ, pAZ, pAY, pAX)
  --print(pAZ)
  -- Z axis
  local xx=(lib3D:_cos(pAZ)*pX)-(lib3D:_sin(pAZ)*pY)
  local yy=(lib3D:_sin(pAZ)*pX)+(lib3D:_cos(pAZ)*pY)  
  
  -- Y axis
  local xxx = (lib3D:_cos(pAY)*xx) + (lib3D:_sin(pAY)*pZ)
  local  zz = (lib3D:_cos(pAY)*pZ) - (lib3D:_sin(pAY)*xx)
  
  -- X axis
  local yyy = (lib3D:_cos(pAX)*yy) - (lib3D:_sin(pAX)*zz)
  local zzz = (lib3D:_sin(pAX)*yy) + (lib3D:_cos(pAX)*zz)  
  return xxx,yyy,zzz
end

function lib3D:setColor(pP1,pP2)
  local b = 1-lib3D:lerp(math.min(pP1.z, pP2.z), math.max(pP1.z, pP2.z), 0.075)
  --print(b)
  love.graphics.setColor(0,0,b,1)
end

function lib3D:lerp(initial_value, target_value, speed)
	local result = (1-speed) * initial_value + speed*target_value
	return result
end

function lib3D:round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end

function lib3D:_cos(pA) return math.cos(math.rad(pA)) end
function lib3D:_sin(pA) return math.sin(math.rad(pA)) end

function lib3D:newCube(pWidth, pHeight)
  local c        = {}
        c.baseZoom     = 720
        c.zoom         = c.baseZoom*0.2
        c.screenWidth  = pWidth
        c.screenHeight = pHeight
        c.origin_x     = pWidth/2
        c.origin_y     = pHeight/2
        c.zOffset      = 2.5
        c.time         = 0
        c.vertices     = {}
        c.surface      = love.graphics.newCanvas(pWidth, pHeight)
        c.surface:setFilter("linear", "linear", 16)
        c.angle_x      = 0
        c.angle_y      = 0
        c.angle_z      = 0
        c.alpha        = 1
        c.shader       = love.graphics.newShader("Modules/Shaders/shd_Cube_glow.fs")
        c.state        = "none"
  
  -- initialize -----------------------------
  
  -- face
  table.insert(c.vertices, lib3D:newVertex(-1,-1,-1))
  table.insert(c.vertices, lib3D:newVertex(1,-1,-1))
  table.insert(c.vertices, lib3D:newVertex(1,1,-1))
  table.insert(c.vertices, lib3D:newVertex(-1,1,-1))
  
  -- back
  table.insert(c.vertices, lib3D:newVertex(-1,-1,1))
  table.insert(c.vertices, lib3D:newVertex(1,-1,1))
  table.insert(c.vertices, lib3D:newVertex(1,1,1))
  table.insert(c.vertices, lib3D:newVertex(-1,1,1))
  
  -------------------------------------------
  
  function c:update(dt)
    for i=1, #self.vertices do
      local v=self.vertices[i]      
            v.rx,v.ry,v.rz = lib3D:rotate(v.x, v.y, v.z, self.angle_x, self.angle_y, self.angle_z)
            v.projX        = self.origin_x + ((v.rx*self.zoom) / (v.rz + self.zOffset))
            v.projY        = self.origin_y + ((v.ry*self.zoom) / (v.rz + self.zOffset))
    end
    
    self.angle_x = self.angle_x + dt*50
    self.angle_y = self.angle_y + dt*40
    self.angle_z = self.angle_z + dt*30 +lib3D:_sin(self.time)
    
    -- *((self.baseZoom/10)*lib3D:_cos(self.time)/3)+self.baseZoom
    
    if self.state == "zoom" then
      local z = (self.baseZoom*(lib3D:_sin(lib3D:_cos(self.time*.2)*10)/2)+0.5)*3.14159
      self.zoom = lib3D:lerp(self.zoom, z, 0.01)
    else
      self.zoom = lib3D:lerp(self.zoom, self.baseZoom*0.2, 0.01)
    end
    
    
    self.time = self.time + dt*50
  end
  
  function c:draw()
    love.graphics.push()
      love.graphics.setColor(1,1,1,self.alpha)
      love.graphics.setCanvas(self.surface)
      love.graphics.clear()
      love.graphics.scale(0.5, 0.5)
        -- draw edges
        love.graphics.setColor(0,0.25,0.75,self.alpha)
        -- v1
        love.graphics.line(self.vertices[1].projX,self.vertices[1].projY,self.vertices[2].projX,self.vertices[2].projY)
        love.graphics.line(self.vertices[1].projX,self.vertices[1].projY,self.vertices[4].projX,self.vertices[4].projY)
        love.graphics.line(self.vertices[1].projX,self.vertices[1].projY,self.vertices[5].projX,self.vertices[5].projY)
        
        -- v3
        love.graphics.line(self.vertices[3].projX,self.vertices[3].projY,self.vertices[2].projX,self.vertices[2].projY)
        love.graphics.line(self.vertices[3].projX,self.vertices[3].projY,self.vertices[4].projX,self.vertices[4].projY)
        love.graphics.line(self.vertices[3].projX,self.vertices[3].projY,self.vertices[7].projX,self.vertices[7].projY)
        
        -- v6
        love.graphics.line(self.vertices[6].projX,self.vertices[6].projY,self.vertices[2].projX,self.vertices[2].projY)
        love.graphics.line(self.vertices[6].projX,self.vertices[6].projY,self.vertices[5].projX,self.vertices[5].projY)
        love.graphics.line(self.vertices[6].projX,self.vertices[6].projY,self.vertices[7].projX,self.vertices[7].projY)
        
        --v8
        love.graphics.line(self.vertices[8].projX,self.vertices[8].projY,self.vertices[4].projX,self.vertices[4].projY)
        love.graphics.line(self.vertices[8].projX,self.vertices[8].projY,self.vertices[5].projX,self.vertices[5].projY)
        love.graphics.line(self.vertices[8].projX,self.vertices[8].projY,self.vertices[7].projX,self.vertices[7].projY)
        
        -- draw points
        love.graphics.setColor(1,0,0,self.alpha)
        for i=1, #self.vertices do
          local v=self.vertices[i]
          love.graphics.circle("fill", v.projX,v.projY,1)
        end
        
      love.graphics.setCanvas()    
    love.graphics.pop() 
    
    love.graphics.setColor(1,1,1,self.alpha)
    
    love.graphics.setShader(self.shader)
      self.shader:send("size", {self.screenWidth, self.screenHeight})
      love.graphics.draw(self.surface, 0, 0)
    love.graphics.setShader()
    
  end
  
  return c
end

function lib3D:newTextureGrid(pGridW, pGridH, pScreenW, pScreenH, pTexturePath)
  local g       = lib3D:newCube(pScreenW, pScreenH)
        g.vertices    = {}
        g.origin_x    = pScreenW  / 2
        g.origin_y    = 0
        g.shapeWidth  = pGridW
        g.shapeHeight = pGridH
        g.nbVertice_x = 1/(pGridW/2)
        g.nbVertice_y = 1/(pGridH/2)
        g.textureData = love.image.newImageData(pTexturePath)
        g.texture     = love.graphics.newImage(g.textureData)
        g.textureW    = g.texture:getWidth()
        g.textureH    = g.texture:getHeight()
        g.heightmap   = {}
        g.time        = 0
        g.timeCycle   = 0
        g.timeRotation = 360
        g.baseZoom    = 540
        g.zoom        = g.baseZoom
        g.angle_z     = -70
  
  function g:initialize()    
    -- extract texture data
    for yy=0,self.textureH-1 do
      for xx=0,self.textureW-1 do
        local r,g,b,a=self.textureData:getPixel(xx,yy)
        local c=(r+g+b)/3
        table.insert(self.heightmap,1-c)
      end    
    end
    
    local counter=0
    local col=1
    local lig=1
    for y=-1,1,self.nbVertice_y do
      for x=-1,1,self.nbVertice_x do
        local v=lib3D:newVertex(x,y,0)
        v.offset=counter*(x+y)/2
        table.insert(self.vertices,v)
        col=(col%self.shapeWidth)+1
      end
      lig=lig+1
    end
  end
  
  g:initialize()
  
  function g:update(dt)
    for i=1, #self.vertices do    
      local v=self.vertices[i]    
      
      -- convert 1D to 2D
      local lig=math.max(1,math.floor(i/self.shapeWidth))
      local col=math.max(1,i%self.shapeWidth)      
      
      -- Cycle heightmap
      local heightCol = math.floor((col/self.shapeWidth)*(self.textureW-1))
      local heightlig = math.floor((lig/self.shapeHeight)*(self.textureH-1)-self.timeCycle)%self.textureH-1
          
      v.oz=(self.heightmap[(heightlig+1)*self.textureW+heightCol]/2)
      local newZ=v.oz
      v.z=lib3D:lerp(v.z, newZ,.075)
      
      -- rotate
      v.rx,v.ry,v.rz=lib3D:rotate(v.x,v.y,v.z, self.angle_x, self.angle_y, self.angle_z)
      
      -- project
      v.projX=self.origin_x + ((v.rx*self.zoom) / (v.rz + self.zOffset))
      v.projY=self.origin_y + ((v.ry*self.zoom) / (v.rz + self.zOffset))
    end
    -- zoom in/out
    self.zoom = self.baseZoom+(self.baseZoom/3*lib3D:_sin(lib3D:_cos(self.timeRotation)/6))
    self.origin_y = 1-((self.screenHeight/3*lib3D:_cos(self.timeRotation/6)))
    
    -- update angles
    self.time         = self.time + 0.5
    self.timeRotation = self.timeRotation + 0.5
    self.timeCycle    = self.timeCycle + 1
    
    
    self.angle_z = -70+(-25*lib3D:_sin(self.timeRotation/3))
    
    if(love.keyboard.isDown("left")) then self.angle_x = self.angle_x-1 end
    if(love.keyboard.isDown("right")) then self.angle_x = self.angle_x+1 end
    
    if(love.keyboard.isDown("up")) then self.angle_z = self.angle_z-1 end
    if(love.keyboard.isDown("down")) then self.angle_z = self.angle_z+1 end
  end
  
  function g:draw()
    love.graphics.setColor(1, 1, 1, 1)
    local l=#self.vertices
    for i=1, l do
      local v   = self.vertices[i]
      local lig = math.max(1,math.floor(i/self.shapeWidth))
      local col = math.max(1, i%self.shapeWidth)
      
      local p1 = v
      if i%self.shapeWidth~=0 then        
        local p2 = self.vertices[i+1]
        lib3D:setColor(p1,p2)
        love.graphics.line(p1.projX,p1.projY,p2.projX,p2.projY)
        if lig<self.shapeHeight-1 then
          local p4 = self.vertices[i+self.shapeWidth]
          lib3D:setColor(p1,p4)
          love.graphics.line(p1.projX,p1.projY,p4.projX,p4.projY)
        end
      elseif lig<self.shapeHeight then
        local p4 = self.vertices[i+self.shapeWidth]
        lib3D:setColor(p1,p4)
        love.graphics.line(p1.projX,p1.projY,p4.projX,p4.projY)
      end
            
      local a = (1-v.oz)+0.2
      
      love.graphics.setColor(a,0,0,a)
      love.graphics.points(v.projX,v.projY)
    end
  end

  return g
end

function lib3D:newAudioVisualizer(pGridW, pGridH, pScreenW, pScreenH, pMusic, pMusicData)
  local lib      = require("Modules/libFFTData")
  local g        = {}
        g.vertices     = {}
        g.origin_x     = pScreenW  / 2
        g.origin_y     = pScreenH  / 2
        g.screenWidth  = pWidth
        g.screenHeight = pHeight
        g.shapeWidth   = pGridW
        g.shapeHeight  = pGridH
        g.nbVertice_x  = 1/(pGridW/2)
        g.nbVertice_y  = 1/(pGridH/2)
        g.heightmap    = {}
        g.time         = 0
        g.timeCycle    = 0
        g.timeRotation = 0
        g.baseZoom     = 3200
        g.zoom         = g.baseZoom
        g.angle_y      = 0
        g.angle_x      = 0
        g.angle_z      = -70
        g.zOffset      = 2.5
        g.surface      = love.graphics.newCanvas(pWidth, pHeight)
        g.surface:setFilter("linear", "linear", 16)
        g.state        = "none"
        
        g.data         = lib:newData(pMusic, pMusicData)
        g.shader       = love.graphics.newShader("Modules/Shaders/shd_Alpha_from_center.fs")
  
  function g:initialize()    
    local counter=0
    local col=1
    local lig=1
    for y=-1,1,self.nbVertice_y do
      for x=-1,1,self.nbVertice_x do
        local v=lib3D:newVertex(x,y,0)
        v.offset=counter*(x+y)/2
        table.insert(self.vertices,v)
        col=(col%self.shapeWidth)+1
      end
      lig=lig+1
    end
  end
  
  g:initialize()
  
  function g:update(dt)
    
    self.data:update(dt)
    for i=1, #self.vertices do    
      local v=self.vertices[i]
      
      -- convert 1D to 2D
      local lig=math.max(1,math.floor(i/self.shapeWidth))
      local col=math.max(1,i%self.shapeWidth)
      
       -- Cycle heightmap
      local heightCol = math.floor((col/self.shapeWidth)*(#self.data.spectrum))
      local heightlig = 1
      
      if col>self.shapeWidth/2 then
        heightCol = #self.data.spectrum - (heightCol - (#self.data.spectrum/2))
      else
        heightCol = (#self.data.spectrum/2) - heightCol+1
      end
      
      local amplitude = 1
      v.oz = 0-(self.data.spectrum[heightCol]/amplitude):abs()
      local c = 0-(v.oz)*10
      v.oz=math.max(v.oz, -0.09)
      
      v.c  = lib3D:lerp(v.c, c, 0.01)
      
      local newZ=v.oz   
      
      v.z=lib3D:lerp(v.z, newZ,.05)
      
      -- rotate
      v.rx,v.ry,v.rz=lib3D:rotate(v.x,v.y,v.z, self.angle_x, self.angle_y, self.angle_z)
      
      local ox, oy, oz=lib3D:rotate(v.x,v.y,0, self.angle_x, self.angle_y, self.angle_z)
      
      -- project
      v.projX=self.origin_x + ((v.rx*self.zoom) / (v.rz + self.zOffset))
      v.projY=self.origin_y + ((v.ry*self.zoom) / (v.rz + self.zOffset))
      
      v.projOX=self.origin_x + ((ox*self.zoom) / (oz + self.zOffset))
      v.projOY=self.origin_y + ((oy*self.zoom) / (oz + self.zOffset))
    end
    -- zoom in/out
    self.zoom = self.baseZoom+(self.baseZoom*0.8 * (lib3D:_sin(self.timeRotation)/2) +0.5 ) + (self.baseZoom/2 * (lib3D:_sin(self.timeRotation/3)/2) +0.5 )
    
    -- update angles
    self.time         = self.time + 0.5
    self.timeRotation = self.timeRotation + 0.25
    self.timeCycle    = self.timeCycle + 1
    
    local rot = 135*lib3D:_cos(self.timeRotation)
    
    self.angle_x = rot
        
    if(love.keyboard.isDown("left")) then self.angle_x = self.angle_x-1 end
    if(love.keyboard.isDown("right")) then self.angle_x = self.angle_x+1 end
    
    if(love.keyboard.isDown("up")) then self.angle_z = self.angle_z-1 end
    if(love.keyboard.isDown("down")) then self.angle_z = self.angle_z+1 end    
  end
  
  function g:draw()
    love.graphics.setColor(1, 1, 1, 1)
    local l=#self.vertices
    for i=1, l do
      local v   = self.vertices[i]
      local lig = math.max(1,math.floor(i/self.shapeWidth))
      local col = math.max(1, i%self.shapeWidth)
      local p1 = v
      if i%self.shapeWidth~=0 then        
        local p2 = self.vertices[i+1]
        lib3D:setColor(p1,p2)
        
        love.graphics.line(p1.projX,p1.projY,p2.projX,p2.projY)
      end
      
      love.graphics.setColor(v.c, 1-v.c, 0, 1)
      love.graphics.line(v.projOX,v.projOY, v.projX,v.projY)
      
      love.graphics.setColor(1,0,0,1)
      love.graphics.points(v.projX,v.projY)
    end
  end
  
  return g
end

return lib3D