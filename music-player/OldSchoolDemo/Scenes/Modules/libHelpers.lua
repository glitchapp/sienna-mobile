local libHelpers={}

function libHelpers:lerp(initial_value, target_value, speed)
	local result = (1-speed) * initial_value + speed*target_value
	return result
end

function libHelpers:round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end

return libHelpers