local libStrings={}

function libStrings:toArray(pString)
  local l   = string.len(pString)
  local str = {}  
  for i=1, l do    
    table.insert(str,pString:sub(i, i))
  end
  return str
end

return libStrings