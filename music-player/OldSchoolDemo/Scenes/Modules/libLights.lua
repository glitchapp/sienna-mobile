local lib={}
-- functions
function lib:newLight(pX, pY, pRadius, pIntensity)
  local l={}
        l.x         =pX
        l.y         =pY
        l.radius    = pRadius
        l.intensity = pIntensity
        l.color     = {}
  return l
end

return lib