local libSprites = {}

function libSprites:newSprite(pX, pY, pPath)
  local s={}
        s.x            = pX
        s.y            = pY
        s.angle        = 0
        s.scale_x      = 1
        s.scale_y      = 1
        s.animations   = {}
        s.currentAnim  = nil
        s.currentFrame = 1
        s.frameCounter = 0;
  
  function s:addAnimation(pID, pW, pH, pPath)
  local anim         = {}
  anim.img           = love.graphics.newImage(pPath)
  anim.quadData      = {}
  anim.frameWidth    = pW
  anim.frameHeight   = pH
  anim.textureWidth  = anim.img:getWidth()
  anim.textureHeight = anim.img:getHeight()
  
  local nbframes = math.max(2, anim.textureWidth/anim.frameWidth)
  
  for i=1, nbframes-1 do
    local quad = love.graphics.newQuad(i*anim.frameWidth, 0, anim.frameWidth, anim.frameHeight, anim.textureWidth, anim.textureHeight)
    table.insert(anim.quadData, quad)
  end
  
  --print(#anim.quadData)
  self.animations[pID] = anim
  self:setAnim(pID)
end
  
  function s:animate(dt)
    if self.currentAnim~=nil then
      self.frameCounter = self.frameCounter + (dt * 20)
      if self.frameCounter >= 1 then
        self.frameCounter = 0
        self.currentFrame = self.currentFrame+1
        if self.currentFrame > #self.currentAnim.quadData then
          self.currentFrame = 1
        end
      end
    end   
    
    --print(self.currentFrame)
  end
  
  function s:setAnim(pID)
    self.currentAnim = self.animations[pID]
  end
  
  function s:setPos(pX, pY)
    self.x = pX
    self.y = pY
  end
  
  function s:draw()
    if self.currentAnim ~= nil then      
      
      if #self.currentAnim.quadData <=1 then
        love.graphics.draw(self.currentAnim.img, 
                           self.x, 
                           self.y, 
                           self.angle, 
                           self.scale_x, 
                           self.scale_y, 
                           self.currentAnim.frameWidth/2,
                           self.currentAnim.frameHeight/2)
      end
      
      love.graphics.draw(self.currentAnim.img, 
                         self.currentAnim.quadData[self.currentFrame], 
                         self.x, 
                         self.y,
                         self.angle,
                         self.scale_x,
                         self.scale_y,
                         self.currentAnim.frameWidth/2,
                         self.currentAnim.frameHeight/2)
    end
  end
  
  function s:update(dt)
    self:animate(dt)
  end
  
  return s
end

function libSprites:newAnimation(pSprite, pID, pCol, pLine, pFrameWidth, pFrameHeight)
  local img      = pSprite.img
  local quadData = {}
  for i=pCol, math.floor(pSprite.width/pFrameWidth) do
    local quad = love.graphics.newQuad((i-1)*pFrameWidth, pLine*pFrameHeight, pFrameWidth, pFrameHeight, pSprite.width, pSprite.height)
    table.insert(quadData, pID, quad)
  end
  return quadData
end

return libSprites