extern float thickness = 0.05;
extern float time = 0;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{
  vec4 tex_color = Texel(texture, texture_coords);
  
  float t      = time*0.5;
  
  vec2 offset1 = vec2((0.5*cos(5.0*t))/2.0*cos(t), (0.5*sin(3.0*t))/2.0)/2.0;
  vec2 offset2 = vec2(0.6*cos(3.0*t)*1.5*sin(t/3), 0.4*sin(2.0*t)/2.0)/2.0;   //0.4*sin(2.0*t) //(0.5*0-sin(3.0*t)
  
  //vec2 offset1 = vec2( 0.2*cos(t),  0.1*sin(t));
  //vec2 offset2 = vec2( 0.3*cos(t+90),  0);
  
  
  vec2 p1 = (texture_coords-0.5)-offset1;
  vec2 p2 = p1-offset2;
  
  float radius1 = sqrt((p1.x*p1.x)+(p1.y*p1.y))*2;
  float radius2 = sqrt((p2.x*p2.x)+(p2.y*p2.y))*2;
  
  float thck = thickness + sin(t)/100;
  
  bool toggle1 = mod(radius1, 0.1)>thck;
  bool toggle2 = mod(radius2, 0.1)>thck;
  
  //xor via if statements
  float col = 0.0;
  if (toggle1) col = 1.0;    
  if (toggle2) col = 1.0;
  if ((toggle1) && (toggle2)) col = 0.0;
  
  return vec4(col-(sin(time*0.3)/2)+0.5,col+sin(time*0.5),col+sin(cos(time*0.7)),1.0);  
}