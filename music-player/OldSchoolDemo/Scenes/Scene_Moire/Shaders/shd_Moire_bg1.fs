extern float time = 0;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{  
  vec4 tex_color = Texel(texture, texture_coords);
  
  if(texture_coords.y < 0.8)
  {
    tex_color.r = (sin(cos(texture_coords.x*100)+time)/2)+0.5;
    tex_color.g = (cos(sin(texture_coords.x*100)+90+time)/2)+0.5;
    tex_color.b = (cos(sin(texture_coords.x*100)+time)/2)+0.5;
  }
  else
  {
    tex_color.r = (sin(cos(texture_coords.y*60)+90+time)/2)+0.5;
    tex_color.g = (cos(sin(texture_coords.y*50)+90+time)/2)+0.5;
    tex_color.b = (cos(sin(texture_coords.y*40)+90+time)/2)+0.5;
  }
  
  float d = distance(texture_coords, vec2(0.5, 0.45))*2.5;
  tex_color.a *= smoothstep(tex_color.a, tex_color.r/d, d*(sin(time*0.1)/2)+0.5);
  tex_color.r *= smoothstep(tex_color.r, tex_color.r/1-d, d*(cos(time*0.3)/2)+0.5);
  tex_color.g *= smoothstep(tex_color.g, tex_color.g/d, d);
  tex_color.b *= smoothstep(tex_color.b, tex_color.b/d, d*(cos(time*0.1)/2)+0.5);
  
  
  //if(d<0.3)
  //{
  
    //tex_color.a = smoothstep(tex_color.a, tex_color.a/d, 1-d);
    //tex_color.r = smoothstep(tex_color.r, tex_color.r/d, 1-d);
    //tex_color.b = smoothstep(tex_color.b, 0, 1-d);
  //}
  return tex_color;  
}