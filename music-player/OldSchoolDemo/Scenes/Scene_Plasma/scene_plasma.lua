local scene = {}

scene.isLoaded   = false
scene.isFinished = false

-- scene specific declarations
local width   = 0
local height  = 0
local font    = nil

-- libs -----------------------------------------
local helpers = require("OldSchoolDemo/Modules/libHelpers")
local fade    = nil
local libText = require("OldSchoolDemo/Modules/libText")

-- canvas ---------------------------------------
local surf_plasma  = nil
local surf_loading = nil
local surf_final   = nil

-- image data ------------------------------
local data         = {}
local currentData  = nil

-- music ----------------------------------------
local music        = nil

-- shaders --------------------------------------
local shader_plasma   = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Plasma/Shaders/shd_Plasma.fs")
local shader_intro1   = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Plasma/Shaders/shd_Plasma_intro1.fs")
local shader_intro2   = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Plasma/Shaders/shd_Plasma_intro2.fs")
local shader_intro3   = love.graphics.newShader("OldSchoolDemo/Scenes/Scene_Plasma/Shaders/shd_Plasma_intro3.fs")
--local shader_rotoZoom = love.graphics.newShader("Scenes/Scene_Plasma/Shaders/shd_RotoZoom_normal.fs ")

-- time ------------------------------------
local time        = 0
local time_roto   = 0

local currentTime = 0

local sineScroller = nil
local textToScroll = "a classic again, the plasma!         ... the first version used a lookup table, it was working great but too slow on a medium window ...         ... ok, stop with the text, let's check the roto-plasma now! ..."

-- state ----------------------------------------
local state = "intro1"

function scene:initialize()
  width  = screenWidth
  height = screenHeight
  font   = love.graphics.newImageFont('OldSchoolDemo/Scenes/Scene_Plasma/spriteFonts/spr_font3D_34_34.png',' abcdefghijklmnopqrstuvwxyz0123456789?!,-.é')
  
  -- create canvas --------------------------------------------------------------
  surf_plasma  = love.graphics.newCanvas(width, height)
  surf_loading = love.graphics.newCanvas(width, height)
  surf_final   = love.graphics.newCanvas(width, height)
  
  surf_plasma:setFilter("nearest", "nearest")
  surf_loading:setFilter("nearest", "nearest")
  surf_final:setFilter("nearest", "nearest")
  
  -- init canvas ---------------------------------------------------------------
  love.graphics.setCanvas(surf_plasma)
    love.graphics.setColor(0, 0, 0, 1)
    love.graphics.rectangle("fill", 0, 0, width, height)
  love.graphics.setCanvas()
    
  sineScroller = libText:newSineScroller(textToScroll, font, width, height*2.2/3)
  fade = require("OldSchoolDemo/Modules/libFade"):newFade(width, height)
  
  --music = love.audio.newSource("OldSchoolDemo/Scenes/Scene_Plasma/Music/Fred-IlyadTitle&Ingame.mp3", "stream")
  --music:setLooping(false)
  --music:play()
end

function round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end

function scene:update(dt)
  
  if state == "intro1" then
    if currentTime==30.4 then fade:start("out", "in") end
      if currentTime==30.7 then      
        time = 0
        state = "intro2"
      end
  end
  
  if state == "intro2" then
    if currentTime==76.2 then fade:start("out", "in") end
    if currentTime==76.5 then
      time = 0
      state = "normal"
    end
  end
  
  if state == "normal" then
    if currentTime==137.9 then fade:start("out", "in") end
    if currentTime==138.2 then
      time = 0
      state = "rotoZoom"
    end
  end
  
  if state == "rotoZoom" then
    if currentTime==168.7 then fade:start("out", "in") end
    if currentTime==169 then
      time = 0
      state = "normal"
    end
  end
  
  
  if state == "loading" then
    if data ~= nil then
      currentData = data[1]
      state = "loaded"
    end
  end
  
  --currentTime=round(music:tell("seconds"), 1)
  currentTime=math.floor(mod:get_position_seconds() % 60)
  time = time + dt/2
  time_roto = time_roto + dt*0.1
  
  if currentTime >= 76.5  and currentTime < 138 then
    sineScroller:update(dt)
  end
  
  fade:update()
  
  -- change scene ---------------
  --if not music:isPlaying() then self.isFinished = true end
end

function scene:draw()  
  if state == "intro1" then
    love.graphics.push()
      love.graphics.setCanvas(surf_final)  
      love.graphics.clear()
      love.graphics.scale(.5, .5)
        love.graphics.setShader(shader_intro1)
          love.graphics.setColor(1,1,1,1)
          shader_intro1:send("time", time)
          love.graphics.draw(surf_plasma, 0, 0)
        love.graphics.setShader()
    love.graphics.setCanvas()
    love.graphics.pop()
    
    love.graphics.draw(surf_final, 0, 0)
    
  elseif state == "intro2" then
    love.graphics.push()
      love.graphics.setCanvas(surf_final)  
      love.graphics.clear()
      love.graphics.scale(.5, .5)
        love.graphics.setShader(shader_intro2)
          love.graphics.setColor(1,1,1,1)
          shader_intro2:send("time", time)
          love.graphics.draw(surf_plasma, 0, 0)
        love.graphics.setShader()
    love.graphics.setCanvas()
    love.graphics.pop()
    
    love.graphics.draw(surf_final, 0, 0)
    
  elseif state == "normal" then 
    love.graphics.push()
      love.graphics.setCanvas(surf_final)  
      love.graphics.clear()
      love.graphics.scale(.5, .5)
        love.graphics.setShader(shader_plasma)
          love.graphics.setColor(1, 1, 1, 1)
          shader_plasma:send("time", time)
          love.graphics.draw(surf_plasma, 0, 0)
        love.graphics.setShader()
    love.graphics.setCanvas()
    love.graphics.pop()
    
    love.graphics.draw(surf_final, 0, 0)
    
  elseif state == "rotoZoom" then    
    love.graphics.push()
      love.graphics.setCanvas(surf_final)  
      love.graphics.clear()
      love.graphics.scale(.5, .5)
        love.graphics.setShader(shader_intro3)
          love.graphics.setColor(1, 1, 1, 1)
          shader_intro3:send("time", time)
          love.graphics.draw(surf_plasma, 0, 0)
        love.graphics.setShader()
        love.graphics.setColor(0,0,0,1)
        love.graphics.rectangle("line", 0, 0, width, height)
      love.graphics.setCanvas()
    love.graphics.pop()
    
    love.graphics.setShader(shader_rotoZoom)
      shader_rotoZoom:send("time", time_roto)    
      love.graphics.draw(surf_final, 0, 0)
    love.graphics.setShader()
    
  else
    love.graphics.draw(surf_loading, 0, 0)
    state = "init"
  end
  
  
  if currentTime == 76.3 or currentTime == 168.8 then
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.rectangle("fill", 0, 0, width, height)
  end
  
  if currentTime >= 76.5  and currentTime < 138 then
    sineScroller:draw()
  end
  
  fade:draw()
end

return scene
