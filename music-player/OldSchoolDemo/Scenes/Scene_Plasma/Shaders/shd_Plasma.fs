extern float time = 0;

vec4 effect( vec4 color, Image texture, vec2 tc, vec2 screen_coords )
{  
  float PI   = 3.14;
  float d1 = (40.0*sin(time/2)/2)+0.8;
  float d2 = (40.0*sin(time/4)/2)+0.8;
  
  float w1  = sin(d1*tc.x+time);
  float w2  = sin(d2*(tc.x*sin(time/2.0)+tc.y*sin(time/3.0))+time);
  vec2 c    = vec2(tc.x+sin(time*0.5), tc.y+cos(time*0.33));
  float w3  = sin(sqrt(100.0*(c.x*c.x+c.y*c.y)+1.0)+time);
  
	float w   = (w1+w2+w3)/2.0;
  
  vec3 col  = vec3(sin(PI*w), sin(PI*w+2.0*PI/(3.0+sin(time))), cos(PI*w+4.0*PI/(4.0+sin(time))));
  
  return vec4(col*0.5 + 0.5,1.0);
}