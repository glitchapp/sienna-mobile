extern float time = 0;

vec4 effect( vec4 color, Image texture, vec2 tc, vec2 screen_coords )
{  
  float pi = 3.14159;
  vec3 palette = vec3( sin( pi*(tc.x/(1/3.2))  /2 )+0.5, 
                       sin( pi*(tc.x/(1/6.4))  /2 )+0.5, 
                       sin( pi*(tc.x/(1/12.8)) /2 )+0.5);

  vec3 col = vec3( .5*sin((palette.r)*pi/4.+time)+.5,
                   .5*sin((palette.g+3.)*pi/4.+time)+.5,
                   .5*sin((palette.b-3.)*pi/4.+time)+.5);
  

  //float c1=(sin(/(1/16))/2)+0.5;
  //float c2=(sin(tc.x/(1/8))/2)+0.5;
  //float c3=(sin((tc.x+tc.y)/(1/16))/2)+0.5;
  //float c4=(sin(sqrt( (tc.x*tc.x) + (tc.y*tc.y) )/8)/2)+0.5;
  //float plasma = (c1+c2+c3+c4)/4;

  //vec4 tex_color = vec4(plasma, plasma, plasma, 1);
  
  float result = distance(tc, vec2(0.5*(cos(time)/2)+0.5, 0.5*(sin(time)/2)+0.5))*4;
  
  return vec4((col/result), 1);
}