float zoom = 1.2;
float zoomIn = 2.0;
float zoomOut = 2.0;
float frequency = 2.0;

float distance = 1.5;

extern float time=0;

vec4 effect( vec4 color, Image texture, vec2 t_c, vec2 screen_coords )
{   
  float t = time*2.0;
  vec2 newCoords = vec2( t_c.x*cos(t)  + t_c.y*sin(t),
                         t_c.x*-sin(t) + t_c.y*cos(t));
  
  float z = cos(t / 2.0) + 1.5;
  newCoords-=vec2( (distance*cos(time*3.14159))/z, 
                  -(distance*sin(time/3.14159))/z);
  
  newCoords *= zoomOut+(zoomIn*sin(t*frequency))/zoom;
  
  newCoords.x = mod(newCoords.x, 1.0);
  newCoords.y = mod(newCoords.y, 1.0);
  
  vec4 tex_color = Texel(texture, newCoords);
  
  
  return tex_color;  
}