io.stdout:setvbuf('no')
--if arg[#arg] == "-debug" then require("mobdebug").start() end
love.graphics.setDefaultFilter("nearest")
love.window.maximize(true)
love.window.setTitle("CED - OldSchoolDemo")

screenScale  = 2
screenWidth  = 1080
screenHeight = 640
windowWidth  = screenWidth *screenScale
windowHeight = screenHeight*screenScale

screenSize = screenWidth*screenHeight
windowSize = windowWidth*windowHeight

-- scenes -----------------------------------------------------------------
currentScene=nil
local prevScene=nil
local scenes={}
local sceneIndex = 7

-- canvas -----------------------------------------------------------------
local surface = nil

local fade = require("OldSchoolDemo/Modules/libFade"):newFade(screenWidth, screenHeight)

local state = "play"

-- shaders -----------------------------------------------------------------
local shader_scanlines= love.graphics.newShader("OldSchoolDemo/Scenes/Mixed/Shaders/shd_Scanlines.fs")

-- time -----------------------------------------------------------------
local time = 0

-- vignette -----------------------------------------------------------------
local vignette = nil

function OldSchoolLoad()
  --success = love.window.setMode(windowWidth, windowHeight, {fullscreen=false})
  
  surface = love.graphics.newCanvas(screenWidth, screenHeight)
  surface:setFilter("nearest", "nearest")
  
  love.graphics.setCanvas(surface)
    love.graphics.setColor(0, 0, 0, 0)
  love.graphics.setCanvas()
  
  -- load vignette texture ---------------------------------
  vignette = love.graphics.newImage("OldSchoolDemo/Scenes/Mixed/Images/spr_Vignette.png")
  
  -- load scenes
  table.insert(scenes, require("OldSchoolDemo/Scenes/Scene_Intro/scene_intro"))
  table.insert(scenes, require("OldSchoolDemo/Scenes/Scene_Scroller/scene_scroller"))
  table.insert(scenes, require("OldSchoolDemo/Scenes/Scene_Moire/scene_moire"))
  table.insert(scenes, require("OldSchoolDemo/Scenes/Scene_RotoZoom/scene_rotoZoom"))
  table.insert(scenes, require("OldSchoolDemo/Scenes/Scene_Plasma/scene_plasma"))  
  table.insert(scenes, require("OldSchoolDemo/Scenes/Scene_3D/scene_3D"))
  table.insert(scenes, require("OldSchoolDemo/Scenes/Scene_Credits/scene_credits"))
  table.insert(scenes, require("OldSchoolDemo/Scenes/Mixed/scene_3D"))
  
  SetScene(6) 
end

------------------------------ Helpers ------------------------------------
function SetScene(pIndex)
  prevScene    = currentScene
  currentScene = scenes[pIndex]
  currentScene:initialize()
end

------------------------------ Update -------------------------------------
function OldSchoolupdate(dt)
  
  if state == "play" then
    if currentScene~=nil then
      currentScene:update(dt)
    end
    time=time+0.1
    
    if currentScene.isFinished then    
      if sceneIndex<#scenes then
        sceneIndex = sceneIndex + 1
        fade:start("out", "in") 
        state = "wait"
      else
        fade:start("out", "none")
        state = "quit"
      end
    end
  end
  
  if state == "wait" then
    if fade.isFinished then 
      SetScene(sceneIndex)      
      state = "play"
    end
  end
  
  if state == "quit" then
    if fade.isFinished then
      love.event.quit()
    end
  end
  
  fade:update()
end

------------------------------ Draw ---------------------------------------
function OldSchooldraw() 
  if currentScene ~= nil then
    love.graphics.push()
      love.graphics.setColor(1,1,1,1)
      love.graphics.scale(screenScale,screenScale)      
      currentScene:draw()
      love.graphics.setColor(1,1,1,0.6)
      love.graphics.draw(vignette,0,0)
      fade:draw()
    love.graphics.pop()
  end  
  
  love.graphics.draw(surface)  
end
