closeicon= love.graphics.newImage("art/radialMenu/conftranswhite.png")
levelcontrol1= love.graphics.newImage("art/radialMenu/level1.png")
confcontrol1= love.graphics.newImage("/art/radialMenu/conftranswhite.png")
restarticon= love.graphics.newImage("/art/radialMenu/restart.png")
loadicon= love.graphics.newImage("/art/radialMenu/load.png")
saveicon= love.graphics.newImage("/art/radialMenu/save.png")

axiscircle = { x = 0, y = 0 }

-- State flags to track trigger input and radial menu status

local isClosingMenu = false  -- Prevent multiple closures during one trigger release cycle
local triggerThreshold = 0.5  -- Threshold for trigger activation (for both left and right triggers)
local previousTriggerState = false  -- To track the previous state of the trigger
local previousRadialFocus = "none"  -- To track previous radial focus, prevent multiple triggers for the same focus


function love.gamepadaxis(joystick, axis, value)
    if axis == 'triggerleft' or axis == 'triggerright' then
        local triggerleft = joystick:getGamepadAxis("triggerleft")
        local triggerright = joystick:getGamepadAxis("triggerright")

        -- Set vibration (optional)
        success = joystick:setVibration(left, right)
        
        -- Check if either trigger is pressed past the threshold (e.g., trigger activation above 0.5)
        local isTriggerActive = triggerleft > triggerThreshold or triggerright > triggerThreshold


        -- Check if radial menu is focused
        if radialfocus == "levelmenu" or radialfocus == "options" or radialfocus == "restart" then
            -- Ensure menu is only closed once per cycle (prevent multiple triggers)
            if not isClosingMenu then
                if triggerleft < 0.5 or triggerright < 0.5 then
                    isClosingMenu = true  -- Prevent multiple closures

                    -- Handle the closing of the radial menu
                    if radialfocus == "levelmenu" then
                      
                    elseif radialfocus == "options" then
                       
                    elseif radialfocus == "restart" then
                       
                    end
                end
            end
        end

        -- Reset closing flag when triggers are back up
        if triggerleft > 0.5 or triggerright > 0.5 then
            --radialmenu = true
            --isClosingMenu = false  -- Reset the flag when triggers are released
        else
			--processRadialMenuAction()  -- Process the radial menu action
			--radialmenu = false
			
        end
        --print(isTriggerActive)
         
         -- Only process the action when there is a state change in trigger activation
        if isTriggerActive ~= previousTriggerState then
            previousTriggerState = isTriggerActive  -- Update the previous state

            if isTriggerActive then
                -- When the trigger goes above the threshold, we can activate the radial menu
                radialmenu = true
            else
                -- When the trigger goes below the threshold, we can deactivate the radial menu
                --if not isClosingMenu then  -- Ensure that the closing action only happens once
                    --isClosingMenu = true  -- Mark that the menu is in the process of closing
                    processRadialMenuAction()  -- Process the radial menu action
                    love.timer.sleep(0.3)  -- Small delay to prevent immediate re-triggering
                --end
                radialmenu = false
            end
        end
        
    end
end

-- Function to process the radial menu action based on the current radial focus
function processRadialMenuAction()
    if radialfocus == "levelmenu" then
        -- Trigger level menu action
        mainmenu.enter()
        playSound("confirm")
        if config.assets == "NEW" then
            playMusic("OpeningEDM")
        elseif config.assets == "CLASSIC" then
            playMusic("opening")
        end
    elseif radialfocus == "options" then
        -- Toggle assets (options menu)
        inputtime = 0
        --toggleAssets()
			if config.assets == "NEW" then config.assets = "CLASSIC"
        elseif config.assets == "CLASSIC" then config.assets = "NEW"
        end
			if config.assets == "NEW" 		then playMusic(table.random({"bundesligaMix","scooterfestMix","rockerronni"}))
		elseif config.assets == "CLASSIC" 	then playMusic(table.random({"bundesliga","scooterfest","rockerronni"}))
		end
        print("toggle assets")
    elseif radialfocus == "restart" then
        -- Handle restarting (e.g., toggling retro mode)
        print("toggle retro mode")
        if retroShader == "no" then
            retroShader = "c64"
            playSound("blip")
            if config.assets == "NEW" then
                toggleAssets()
            end
        elseif retroShader == "c64" then
            retroShader = "speccy"
            playSound("blip")
            if config.assets == "NEW" then
                toggleAssets()
            end
        elseif retroShader == "speccy" then
            retroShader = "no"
            playSound("blip")
        end
    end

    -- Reset closing flag after action is processed
    isClosingMenu = false
end



-- Function to update joystick axis and control emulated mouse movement
function updategetjoystickaxis(dt)
    gamepadTimer = gamepadTimer + dt
    for i, player in ipairs(players) do
        local joystick = player.joystick
        if joystick then
            -- Get values of joystick axes
            local leftxaxis = joystick:getGamepadAxis("leftx")
            local leftyaxis = joystick:getGamepadAxis("lefty")
            local tright = joystick:getGamepadAxis("triggerright")

            -- Check for movement and handle accordingly
            if math.abs(leftxaxis) > 0.2 then
                if leftxaxis > 0.2 then
                    send('right', i)
                    love.timer.sleep(0.3)
                elseif leftxaxis < -0.2 then
                    send('left', i)
                    love.timer.sleep(0.3)
                end
            end

            if math.abs(leftyaxis) > 0.2 then
                if leftyaxis > 0.4 then
                    send('down', i)
                    love.timer.sleep(0.3)
                elseif leftyaxis < -0.2 then
                    send('up', i)
                    love.timer.sleep(0.3)
                end
            end

            -- Trigger-based zoom (example placeholder)
            if tright > 0.2 then
                -- Handle zoom action for this player
            end

            -- Pass the joystick to the radial menu update
            updateRadialMenu(joystick, i)
        end
    end
end

-- Function to update the radial menu focus based on joystick input
function updateRadialMenu(joystick, playerIndex)
    if not joystick then return end -- Ensure joystick exists

    -- Get the left joystick axes
    local axis1 = joystick:getGamepadAxis("leftx")
    local axis2 = joystick:getGamepadAxis("lefty")

    -- Apply deadzone to joystick axes (prevents small fluctuations from triggering unwanted changes)
    local deadzone = 0.2
    local leftxaxis = math.abs(axis1) < deadzone and 0 or axis1
    local leftyaxis = math.abs(axis2) < deadzone and 0 or axis2

    -- Debugging to confirm axes values
    --print(string.format("Player %d - LeftX: %.2f, LeftY: %.2f", playerIndex, leftxaxis, leftyaxis))

    -- Determine which radial option is currently focused based on joystick position
    if leftxaxis > -deadzone and leftxaxis < deadzone and leftyaxis > -deadzone and leftyaxis < deadzone then
        radialfocus = "close"  -- Center
    elseif leftxaxis > -0.4 and leftxaxis < 0.4 and leftyaxis < -0.6 then
        radialfocus = "levelmenu"  -- Up icon
    elseif leftxaxis < -0.6 and leftyaxis < 0 then
        radialfocus = "options"  -- Left icon
    elseif leftxaxis > 0.6 and leftyaxis < 0 then
        radialfocus = "restart"  -- Right icon
    elseif leftxaxis > 0.6 and leftyaxis > 0 then
        radialfocus = "return"  -- Right down icon
    elseif leftxaxis < -0.6 and leftyaxis > 0 then
        radialfocus = "load"  -- Left down icon
    elseif leftxaxis > 0.6 and leftyaxis > 0 then
        radialfocus = "save"  -- Right down icon
    else
        radialfocus = "none"  -- No focus
    end

    -- If the radial focus has changed, prevent multiple triggers for the same focus
    if radialfocus ~= previousRadialFocus then
        print(string.format("Player %d - Radial Focus Changed: %s", playerIndex, radialfocus))
        previousRadialFocus = radialfocus
        -- We may want to reset certain states or actions if the radial focus changes
    end
end


function drawLanguagesRadialemnu()



end

function drawradialmenu()
			-- Draw radial background
			if retroShader == "no" then
				love.graphics.setColor(0.5,0.5,0.5,0.5)
				love.graphics.circle("fill", love.graphics.getWidth()/4+50, (love.graphics.getHeight()/2)+100, 300)
			end
			
		--[[	
			--left shoulder
			love.graphics.rectangle("fill", love.graphics.getWidth()/4-250, (love.graphics.getHeight()/2)-250, love.graphics.getWidth()/16,love.graphics.getWidth()/16)

			--right shoulder
			love.graphics.rectangle("fill", love.graphics.getWidth()/4+200, (love.graphics.getHeight()/2)-250, love.graphics.getWidth()/16,love.graphics.getWidth()/16)		
			
			love.graphics.setColor(1,1,1,1)

			-- draw radial shoulder tabs selection
			love.graphics.draw (leftshoulder,  love.graphics.getWidth()/4-250, (love.graphics.getHeight()/2)-300, 0, 0.3,0.3)
			love.graphics.draw (rightshoulder,  love.graphics.getWidth()/4+200, (love.graphics.getHeight()/2)-300, 0, 0.3,0.3)
			
			--center of tabs
			love.graphics.draw (helpicon,  love.graphics.getWidth()/4, (love.graphics.getHeight()/2)-30, 0, 0.20,0.20)
--]]		
			love.graphics.setColor(1,1,1,1)
			
			--draw center circle
			love.graphics.draw (closeicon,  love.graphics.getWidth()/3.95, love.graphics.getWidth()/3.3,0,0.1,0.1)	-- center
			
			
									
			
			--draw radial menu depending on the radialfocustab state
			if radialfocustab==nil or radialfocustab=="mainradialmenu" then drawmainradialemnu()
			end
				
				--vibration when hovering an option
				if lastradialfocus==radialfocus then
			elseif not (lastradialfocus==radialfocus) then
					if vibration==true and axistouchedonce and level~=lastgamepadtouchedlevel then
						joystickvibration = joystick:setVibration(0.05, 0.05,0.05)
						lastradialfocus=radialfocus
					end
			end
			
			
			love.graphics.setColor(1,1,1,1)
			
				love.graphics.circle("line", axiscircle.x+love.graphics.getWidth()/4+50, (axiscircle.y+love.graphics.getHeight()/2)+100, 50)
			
end

function drawmainradialemnu()
			
		
			--draw center edge circles
			--love.graphics.setShader()
			love.graphics.setColor(1,1,1,1)
				
					love.graphics.draw (levelcontrol1,  love.graphics.getWidth()/4.2, love.graphics.getWidth()/5,0,0.2,0.2)	-- up
					love.graphics.print ("Return to main menu",  love.graphics.getWidth()/4.2, love.graphics.getWidth()/5)	-- up
			
			
				love.graphics.draw (confcontrol1,  love.graphics.getWidth()/6, love.graphics.getWidth()/4,0,0.2,0.2)	-- left				options
				love.graphics.print ("Toggle assets",  love.graphics.getWidth()/6, love.graphics.getWidth()/4)	-- left				options
			
				love.graphics.draw (restarticon,  love.graphics.getWidth()/3, love.graphics.getWidth()/4,0,0.2,0.2)		-- right			restart
				love.graphics.print ("Toggle retro mode",  love.graphics.getWidth()/3, love.graphics.getWidth()/4)		-- right			restart
			
				--love.graphics.draw (loadicon,  love.graphics.getWidth()/5.3, love.graphics.getWidth()/3,0,0.15,0.15)		-- down left
				--love.graphics.print ("This options is not programed and does nothing",  love.graphics.getWidth()/5.3, love.graphics.getWidth()/3)
				--love.graphics.draw (saveicon,  love.graphics.getWidth()/3.1, love.graphics.getWidth()/3,0,0.15,0.15)		-- down right
				--love.graphics.print ("This options is not programed and does nothing",  love.graphics.getWidth()/3.1, love.graphics.getWidth()/3)
			
				--if radialfocus==nil then radialfocus="none" end	-- prevents the game from crashing when triggering the radial menu on level selection menu
				--love.graphics.print(radialfocus,love.graphics.getWidth()/4, love.graphics.getWidth()/2.65,0,1)
			
				-- focus selection circles
				love.graphics.setColor(0.5,0.5,0.5,0.4)
				
				if radialfocus=="close" 	then love.graphics.circle("fill", love.graphics.getWidth()/3.7, (love.graphics.getHeight()/1.8), 50,50)
			elseif radialfocus=="levelmenu" then love.graphics.circle("fill", love.graphics.getWidth()/3.8, (love.graphics.getHeight()/2.5), 100,100)
			elseif radialfocus=="options" 	then love.graphics.circle("fill", love.graphics.getWidth()/5.2, (love.graphics.getHeight()/2), 100,100)
			elseif radialfocus=="restart" 	then love.graphics.circle("fill", love.graphics.getWidth()/2.8, (love.graphics.getHeight()/2), 100,100)
			elseif radialfocus=="return" 	then love.graphics.circle("fill", love.graphics.getWidth()/2.8, (love.graphics.getHeight()/2), 100,100)
			elseif radialfocus=="load" 		then love.graphics.circle("fill", love.graphics.getWidth()/5, (love.graphics.getHeight()/1.5), 100,100)
			elseif radialfocus=="save" 		then love.graphics.circle("fill", love.graphics.getWidth()/2.8, (love.graphics.getHeight()/1.5), 100,100)
			end

		-- if a is pressed
			--if joystick:isGamepadDown("a")  then
				--	if radialfocus=="close" then radialmenu=false
				--	love.timer.sleep( 0.3 )
	

end
