-- TO DO
-- o align
--
-- **********************************
-- Demo variables
-- *********************************

local soundThreshold = 50  -- Threshold distance for sound (e.g., when boids cross a region of 50 pixels)
local soundCooldown = 10  -- Minimum cooldown time between sounds (in seconds)
local lastSoundTime = 0  -- Track the time when the last sound was played

function playBatSound()
    -- Check if enough time has passed since the last sound
    if love.timer.getTime() - lastSoundTime >= soundCooldown then
        -- Choose a random bat sound
        local randSound = math.random(1, 3)
        
        if randSound == 1 then
            snd.bat01:setVolume(0.3)
            snd.bat01:play()
        elseif randSound == 2 then
			snd.bat02:setVolume(0.3)
            snd.bat02:play()
        elseif randSound == 3 then
			snd.bat03:setVolume(0.3)
            snd.bat03:play()
        end
        
        print(lastSoundTime)
        -- Update the last sound time
        lastSoundTime = love.timer.getTime()
    end
end


-- Timer for pattern activation
local patternActivationTime = 35 -- Time in seconds to activate the pattern
local elapsedTime = 0
local patternActive = false
TARGET_ATTRACTION = 100 -- strength of attraction to target points
local extraBoidsCount = 0 -- Track the number of extra boids for the pattern

local targetPoints = {
    {x = 1400, y = 200}, -- top of arrow
    {x = 1350, y = 300}, -- left middle
    {x = 1450, y = 300}, -- right middle
    {x = 1300, y = 400}, -- bottom left
    {x = 1400, y = 400}, -- middle bar left
    {x = 1400, y = 300}, -- middle of arrow
    {x = 1500, y = 400}, -- bottom right
    {x = 1400, y = 400}  -- middle bar right
} 

local shaderCode = [[

extern vec3 lightDirection;
extern vec3 cameraPosition;

vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords) {
    vec4 texColor = Texel(texture, texture_coords);

    // Make sure it's a silhouette
    if(texColor.a < 0.1) discard;
    vec3 silhouetteColor = vec3(0.0, 0.0, 0.0);

    // Calculate ambient occlusion (simplified)
    float ambient = 0.3;

    // Calculate diffuse lighting
    vec3 norm = normalize(vec3(0.0, 0.0, 1.0)); // Assuming the normal is pointing out of the screen
    float diffuse = max(dot(norm, lightDirection), 0.0);

    // Calculate specular highlight
    vec3 viewDir = normalize(cameraPosition - vec3(screen_coords.x, screen_coords.y, 0.0));
    vec3 reflectDir = reflect(-lightDirection, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 16.0);

    // Combine the lighting effects
    vec3 finalColor = silhouetteColor * (ambient + diffuse) + vec3(1.0) * spec * 0.5;

    return vec4(finalColor, texColor.a) * color;
}
]]

local ddwidth, ddheight = love.window.getDesktopDimensions( display )
	--width, height = love.graphics.getDimensions( )

-- Constants
W_WIDTH = ddwidth
--W_HEIGHT = ddheight
W_HEIGHT = ddheight/2


-- Define initial light direction and camera position
local lightDirection = {0.0, -1.0, 0.5}
local cameraPosition = {W_WIDTH / 2, W_HEIGHT / 2, 1.0}
local boidsTime = 0

W_LIMIT = 40

N_BOIDS = 30
CVISUAL_RANGE = 60 -- could be an individual boid property
DEAD_ANGLE = 60
V_TURN = 2 -- could be an individual boid property
MINDISTANCE = 20
VMAX = 100

AVOIDANCE = 30 
COHESION = 2 
CENTERING = 270

-- Constants for z-axis
Z_MIN = 0.2
Z_MAX = 1.2
Z_SPEED = 10 -- reduce the z-speed for smoother movement
Z_DAMPING = 0.7 -- damping factor to smooth transitions

boid={}
-- boids table
boids = {}
boids.list = {}
boids.img = love.graphics.newImage('/love-boids/src/pix/boid.png')
boids.img2 = love.graphics.newImage('/love-boids/src/pix/boid2.png')
boids.img3 = love.graphics.newImage('/love-boids/src/pix/boid3.png')

boids.imgNormal = love.graphics.newImage('/love-boids/src/pix/normal/boid.png')
boids.img2Normal = love.graphics.newImage('/love-boids/src/pix/normal/boid2.png')
boids.img3Normal = love.graphics.newImage('/love-boids/src/pix/normal/boid3.png')

boids.imgHeight = love.graphics.newImage('/love-boids/src/pix/height/boid.png')
boids.img2Height = love.graphics.newImage('/love-boids/src/pix/height/boid2.png')
boids.img3Height = love.graphics.newImage('/love-boids/src/pix/height/boid3.png')

boids.imgfront = love.graphics.newImage('/love-boids/src/pix/boid.png')
boids.imgfish = love.graphics.newImage('/love-boids/src/pix/boid.png')
--boids.img = loadAsset("images/boid.webp", "Image") 
--boids.img = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("images/boid.webp")))
--boids.imgx16 = loadAsset("/externalassets/levels/level3/boid.webp", "Image") 
boids.w = boids.img:getWidth()
boids.h = boids.img:getHeight()

-- *****************
-- Fonctions
-- *****************

function distance(pBoid1, pBoid2) 

  return math.sqrt((pBoid1.x - pBoid2.x)^2 + (pBoid1.y - pBoid2.y)^2)

end

-- Determine if a boid is in the dead angle of the first boid 
function deadAngle(pBoid1, pBoid2)

  -- pBoid1 direction = -arctan(vx, vy)

end

-- Boids 
function createBoid()

  local boid = {}
   boid.x = math.random(W_LIMIT, W_WIDTH - W_LIMIT) 
  boid.y = math.random(W_LIMIT, W_HEIGHT - W_LIMIT)
  boid.z = math.random() * (Z_MAX - Z_MIN) + Z_MIN -- initialize z
  boid.vx = math.random(-VMAX, VMAX)  
  boid.vy = math.random(-VMAX, VMAX)
  boid.vz = math.random(-Z_SPEED, Z_SPEED) -- velocity in z direction

  return boid

end


function cohesion(pBoid, pVisualRange)

  local delta = {}
  local dVx = 0
  local dVy = 0
  local nearBoids = {}
  local sumX = 0
  local sumY = 0
  local sumVx = 0
  local sumVy = 0
  local n = 0

  for index, otherBoid in ipairs(boids.list) do

    if distance(pBoid, otherBoid) < pVisualRange then
      sumX = sumX + otherBoid.x
      sumY = sumY + otherBoid.y
      sumVx = sumVx + otherBoid.vx
      sumVy = sumVy + otherBoid.vy
      n = n + 1
    end
  end

  delta.dx = sumX/n - pBoid.x
  delta.dy = sumY/n - pBoid.y
  delta.dVx = sumVx/n - pBoid.vx 
  delta.dVy = sumVy/n - pBoid.vy
  
  return delta

end


function keepDistance(pBoid, pMinDistance)

  local dist = {}
  dist.dx = 0
  dist.dy = 0
  
  for index, otherBoid in ipairs(boids.list) do
    if pBoid ~= otherBoid then
      if distance(otherBoid, pBoid) < pMinDistance then
        dist.dx = dist.dx + (pBoid.x - otherBoid.x)
        dist.dy = dist.dy + (pBoid.y - otherBoid.y)
      end
    end
  end

  return dist 

end


function keepInside(pBoid, pVTurn, pLimit)

  local turn = {}
  turn.dVx = 0
  turn.dVy = 0


  if pBoid.x < pLimit then
    turn.dVx = pVTurn
  end

  if pBoid.x > W_WIDTH - pLimit then
    turn.dVx = - pVTurn
  end

  if pBoid.y < pLimit then
    turn.dVy = pVTurn 
  end

  if pBoid.y > W_HEIGHT - pLimit then
    turn.dVy = - pVTurn 
  end

  return turn 

end

-- ****************************
-- INITIALISATION
-- ****************************

function initDemo()
  
  for n = 1, N_BOIDS do
    table.insert(boids.list, createBoid())
  end

end

function initDemox12()
	for i=0,1,12 do
		initDemo()
	end
end


function loadBoids()

	birdsShader = love.graphics.newShader(shaderCode)
  --love.window.setMode(W_WIDTH, W_HEIGHT)
  --love.window.setTitle('Reynolds’ Boids')
  
  --police = love.graphics.newFont('fonts/police.ttf', 20)
  --love.graphics.setFont(police)

  initDemo()

end


-- ******************
-- UPDATE
-- ******************
local playBirdsSoundOnce=false
local boidframe = {}
local bonusStageActivated = false
local bonusStageProximity = 50  -- Adjust the proximity value as needed


function updateBoids(dt)    -- Update light direction and camera position over boidsTime for dynamic effects
    lightDirection[1] = math.sin(boidsTime) * 0.5
    lightDirection[2] = math.cos(boidsTime) * 0.5
    lightDirection[3] = 0.5

    cameraPosition[1] = W_WIDTH / 2 + math.sin(boidsTime) * 50
    cameraPosition[2] = W_HEIGHT / 2 + math.cos(boidsTime) * 50
    cameraPosition[3] = 1.0

    birdsShader:send("lightDirection", lightDirection)
    birdsShader:send("cameraPosition", cameraPosition)

    for index, boid in ipairs(boids.list) do
    
      -- Check distance to player
        --XBoidsPlayerDistance = boid.x - player.x
        --YBoidsPlayerDistance = boid.y - player.y
        --boidDistancePlayer = math.sqrt(XBoidsPlayerDistance + YBoidsPlayerDistance)
        
        --print("boidDistancePlayer" .. boidDistancePlayer)
        --print("bonusStageProximity" .. bonusStageProximity)
        
        --if boidDistancePlayer < bonusStageProximity and bonusStageUnlocked==true then
        
            --bonusStageActivated = true
            
            --if not (world.maptitle == "Bonus stage 1") or not (mapname == "bonusStage1.lua") then
				--bonusStageActivated = false
				--print("Bonus stage activated")
				--world.map = "bonusStage1.lua"
				--transitions:fadeoutmode((mode == 0 and "game" or "editing"))
				--sound:play(sound.effects["start"])
				
			--end
        --end
        
         -- Check if the boid crosses the screen edge and trigger sound
        if boid.x < soundThreshold or boid.x > W_WIDTH - soundThreshold or boid.y < soundThreshold or boid.y > W_HEIGHT - soundThreshold then
            playBatSound()
        end
        
        
    
        -- align position and speed with that of others
        local cohesionForce = cohesion(boid, CVISUAL_RANGE)
        -- boids avoid each other
        local avoidanceForce = keepDistance(boid, MINDISTANCE)
        -- boids return to the center when approaching window’s edges
        local centeringForce = keepInside(boid, V_TURN, W_LIMIT)

        -- boids speed adjustment according to all forces
        boid.vx = boid.vx + (avoidanceForce.dx * AVOIDANCE +
                              centeringForce.dVx * CENTERING +
                              (cohesionForce.dx +
                               cohesionForce.dVx) * COHESION) * dt

        boid.vy = boid.vy + (avoidanceForce.dy * AVOIDANCE +
                             centeringForce.dVy * CENTERING +
                             (cohesionForce.dy +
                              cohesionForce.dVy) * COHESION) * dt

        -- control the boid speed
        local speed = math.sqrt(boid.vx^2 + boid.vy^2)
        if speed > VMAX then
            boid.vx = (boid.vx / speed) * VMAX
            boid.vy = (boid.vy / speed) * VMAX
        end

        -- move the boid
        boid.x = boid.x + boid.vx * dt
        boid.y = boid.y + boid.vy * dt

        -- add target attraction when the pattern is active
        if patternActive then
            local target = targetPoints[(index % #targetPoints) + 1]
            local targetForceX = (target.x - boid.x) * TARGET_ATTRACTION
            local targetForceY = (target.y - boid.y) * TARGET_ATTRACTION

            boid.vx = boid.vx + targetForceX * dt
            boid.vy = boid.vy + targetForceY * dt
        end
    end

		if 		patternActive then elapsedTime = elapsedTime + dt *4
	elseif not 	patternActive then elapsedTime = elapsedTime + dt
	end
    if elapsedTime >= patternActivationTime then
        patternActive = not patternActive
        elapsedTime = 0

        --[[if patternActive then
            for i = 1, 100 do
                table.insert(boids.list, createBoid())
                extraBoidsCount = extraBoidsCount + 1
            end
        else
            for i = 1, extraBoidsCount do
                table.remove(boids.list)
            end
            extraBoidsCount = 0
        end
        --]]
    end
    
     for index, boid in ipairs(boids.list) do
		boidframe[index] = boidframe[index] or 0
		boidframe[index] = boidframe[index] + dt * index / 10
		if boidframe[index] > 3 then boidframe[index] = 0 end
    
    end

    boidsTime = boidsTime + dt
end

function attractToTarget(pBoid)
    local target = pBoid.target
    if target then
        return {dx = target.x - pBoid.x, dy = target.y - pBoid.y}
    else
        return {dx = 0, dy = 0}
    end
end

if patternActive then
    for i, boid in ipairs(boids.list) do
        boid.target = targetPoints[(i % #targetPoints) + 1]
    end
else
    for i, boid in ipairs(boids.list) do
        boid.target = nil
    end
end

function atan2(y, x)
    if x > 0 then
        return math.atan(y / x)
    elseif x < 0 then
        if y >= 0 then
            return math.atan(y / x) + math.pi
        else
            return math.atan(y / x) - math.pi
        end
    else -- x == 0
        if y > 0 then
            return math.pi / 2
        elseif y < 0 then
            return -math.pi / 2
        else -- y == 0
            return 0 -- Undefined, but return 0 as a default
        end
    end
end

-- ***************
-- DRAWING 
-- ***************
--local yAngle = 0

function drawBoids()
    
    --love.graphics.setShader(birdsShader)
    
    		if retroShader == true then
				local pixelSize = 20.0  -- Example pixel size for strong pixelation
				blueNoiseZxDitherShader:send("pixelSize", pixelSize)
				love.graphics.setShader(blueNoiseZxDitherShader)
			else
			
			end

	--love.graphics.setShader(normalsWarp)
	
    for index, boid in ipairs(boids.list) do
        local scale = boid.z -- Use z to determine scale
        yAngle = math.abs(boid.vy / 80)
        
         -- Determine which image to draw based on boidframe[index]
        local image
    if currentBackgroundIndex == 16 or currentBackgroundIndex == 17 then
        image = boids.imgfish	-- draw fish instead of birds on underwater stages
    else
        if boidframe[index] == nil then boidframe[index] = 0 end
        if boidframe[index] > 0 and boidframe[index] < 1 then
			vectorNormalsNoiseShader:send("u_normals", boids.imgNormal)
			vectorNormalsNoiseShader:send("u_heightmap", boids.imgHeight)
            image = boids.img
        elseif boidframe[index] > 1 and boidframe[index] < 2 then
        	vectorNormalsNoiseShader:send("u_normals", boids.img2Normal)
			vectorNormalsNoiseShader:send("u_heightmap", boids.img2Height)
            image = boids.img2
        elseif boidframe[index] > 2 and boidframe[index] < 3.5 then
        	vectorNormalsNoiseShader:send("u_normals", boids.img3Normal)
			vectorNormalsNoiseShader:send("u_heightmap", boids.img3Height)
			

            image = boids.img3
        else
            image = boids.img -- Default image
        end
	end
        -- Calculate position with camera offset
        local drawX = boid.x
        local drawY = boid.y

        love.graphics.draw(image, drawX, drawY, -math.atan2(boid.vx, boid.vy), scale * 0.67, scale * 0.67 * yAngle)
        

    
    end

    --love.graphics.setShader()

    if patternActive then
        drawPatternLines()
    end
end

-- The `atan2` function is normally included in Lua's `math` library as `math.atan2`.
-- This custom implementation has been created to address an issue on Sailfish OS
-- where `math.atan2` is not available or fails to work.
-- On systems where `math.atan2` is available, this function is unnecessary.
local function atan2(y, x)
  if x > 0 then
    return math.atan(y / x)
  elseif x < 0 then
    if y >= 0 then
      return math.atan(y / x) + math.pi
    else
      return math.atan(y / x) - math.pi
    end
  elseif y > 0 then
    return math.pi / 2
  elseif y < 0 then
    return -math.pi / 2
  else
    return 0 -- Undefined case (x = 0, y = 0), return 0 as default
  end
end

-- Define a variable for controlling the gap size between segments
local gapSize = 0
local maxGapSize = 30  -- Maximum gap size between segments

function drawPatternLines()
    love.graphics.setColor(1, 0, 0, 0.5) -- Set color and transparency for lines
    love.graphics.setLineWidth(3) -- Set line width for better visibility

    -- Calculate current gap size based on time or any other factor
    gapSize = math.sin(love.timer.getTime() * 2) * maxGapSize  -- Example: sinusoidal animation

    -- Draw segments of the arrow with gaps
    for i = 1, #targetPoints - 1 do
        local point1 = targetPoints[i]
        local point2 = targetPoints[i + 1]

        -- Calculate direction vector
        local dx = point2.x - point1.x
        local dy = point2.y - point1.y

        -- Calculate segment length
        local segmentLength = math.sqrt(dx * dx + dy * dy)

        -- Calculate normalized direction vector
        dx = dx / segmentLength
        dy = dy / segmentLength

        -- Calculate gap vector
        local gapX = dx * gapSize
        local gapY = dy * gapSize

    -- Adjust points to draw with gaps and consider camera offset
        local drawX1 = point1.x
        local drawY1 = point1.y
        local drawX2 = point2.x
        local drawY2 = point2.y

        -- Draw the line segment
        love.graphics.line(drawX1, drawY1, drawX2, drawY2)
    end

    -- Draw arrow shape 
    --drawArrow(targetPoints[1].x, targetPoints[1].y, targetPoints[4].x, targetPoints[4].y)

    --love.graphics.setColor(1, 1, 1, 1) -- Reset color to default white
    --love.graphics.setColor(blendedColor)
    love.graphics.setLineWidth(1) -- Reset line width to default
end

function drawArrow(x1, y1, x2, y2)
    local arrowHeadLength = 20 -- Length of arrowhead (adjust as needed)
    local arrowHeadAngle = math.rad(30) -- Angle of arrowhead sides

    -- Calculate vector from (x1, y1) to (x2, y2)
    local dx = x2 - x1
    local dy = y2 - y1

    -- Calculate the length of the line
    local length = math.sqrt(dx^2 + dy^2)

    -- Normalize the vector
    dx = dx / length
    dy = dy / length

    -- Calculate points for arrowhead
    local arrowX1 = x2 - arrowHeadLength * dx + arrowHeadLength * math.cos(arrowHeadAngle - math.atan2(dx, -dy))
    local arrowY1 = y2 - arrowHeadLength * dy + arrowHeadLength * math.sin(arrowHeadAngle - math.atan2(dx, -dy))
    local arrowX2 = x2 - arrowHeadLength * dx + arrowHeadLength * math.cos(-arrowHeadAngle - math.atan2(dx, -dy))
    local arrowY2 = y2 - arrowHeadLength * dy + arrowHeadLength * math.sin(-arrowHeadAngle - math.atan2(dx, -dy))

    -- Draw the main line
    love.graphics.line(x1, y1, x2, y2)

    -- Draw the arrowhead
    love.graphics.line(x2, y2, arrowX1, arrowY1)
    love.graphics.line(x2, y2, arrowX2, arrowY2)
end


-- ******************
-- Quit demo
-- ******************
--[[
function love.keypressed(key)

  if key == 'escape' then
    love.event.quit()
  end

end
--]]
