warpshader = love.graphics.newShader([[
    #define P 0.0001 // derivative precision
    #define SIZE vec2(8.0, 60.0) // function size/precision
    #define WATER vec4(1.0) // water color

    // Simplified wave function to improve mobile performance
    lowp float func(vec2 coords, float offset) {
      return sin(dot(coords, vec2(1.0, 1.0)) + offset) * 0.05 + 0.05;
    }

    uniform lowp float time = 0.0;
    uniform lowp float xDisplacementScale = 2.0; // Scale for x displacement

    vec4 effect(vec4 color, Image image, vec2 texCoords, vec2 scrCoords) {
      lowp float waveHeight = func(texCoords * SIZE, time);

      // Calculate derivatives using central difference
      lowp float xDeriv = (func(texCoords * SIZE + vec2(P, 0.0), time) - func(texCoords * SIZE - vec2(P, 0.0), time)) / (2.0 * P);
      lowp float yDeriv = (func(texCoords * SIZE + vec2(0.0, P), time) - func(texCoords * SIZE - vec2(0.0, P), time)) / (2.0 * P);

      vec2 off;
      off.x = xDeriv * waveHeight / SIZE.x * xDisplacementScale; // Apply scale to x displacement
      off.y = yDeriv * waveHeight / SIZE.y;

      return Texel(image, texCoords + off) * WATER;
    }
]])
