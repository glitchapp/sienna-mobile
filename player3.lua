Player3 = {}
Player3.__index = Player3

local PLAYER_SPEED = 150
local MAX_SPEED = 200
local GRAVITY = 1000
local JUMP_POWER = 200
local MAX_JUMP = 32
local INVUL_TIME = 1
local BRAKE_POWER = 1
local COL_OFFSETS = {{-5.5,  2}, {5.5,  2},
					 {-5.5, 10}, {5.5, 10},
					 {-5.5, 20}, {5.5, 20}}
local STATE_WAIT = 0
local STATE_RUNNING = 1
local STATE_BURNING = 2
local STATE_WON = 3

local floor = math.floor
local min = math.min
local lk = love.keyboard

function Player3.create(x,y,dir,player)
	local self = {}
	setmetatable(self,Player3)

	self.player = player or 1

		self.img = imgPlayer2
		self.img_orig = imgPlayer2_orig


	return self
end

function Player3:respawn(x,y,dir)
	self.x = x or map.startx
	self.y = y or map.starty
	self.dir = dir or map.startdir or 1 -- -1 = left, 1 = right

	self.frame = 0
	self.state = STATE_WAIT

	self.xspeed = 0
	self.yspeed = 0
	self.onGround = false
	self.jump = 0
	self.invul = INVUL_TIME
	self.onWall = false
	
	self.hasGhosts = false
	self.ghosts = {}
end

function slowMotion2(dt)
 self.frame = self.frame + dt * 13
    if self.invul > 0 then
        self.invul = self.invul - dt
    end

    if self.state == STATE_RUNNING then
        -- Set horizontal speed according to direction
        self.xspeed = self.dir * PLAYER_SPEED

        self.yspeed = self.yspeed + GRAVITY * dt
        self.yspeed = min(self.yspeed, MAX_SPEED)

        -- Keep vertical speed if still jumping
        if self.jump > 0 then
            self.yspeed = -JUMP_POWER
        end

        -- move in X and Y direction
        self.onGround = false
        self:moveX(self.xspeed * dt)
        self:moveY(self.yspeed * dt)

        -- check wall jump
        self.onWall = false
        if self.onGround == false then
            if self.dir == -1 and collidePoint(self.x - 6, self.y + 5) or collidePoint(self.x - 6, self.y + 15) then
                self.onWall = true
            elseif self.dir == 1 and collidePoint(self.x + 6, self.y + 5) or collidePoint(self.x + 6, self.y + 15) then
                self.onWall = true
            end
        end

        for i, v in ipairs(map.enemies) do
            if v.collidePlayer then
                if v:collidePlayer(self) and self.invul <= 0 then
                    love.audio.play(snd.Hurt)
                    self:kill()
                end
            end
        end

        for i, v in ipairs(map.entities) do
            if v.collidePlayer then
                v:collidePlayer(self)
            end
        end

        for i, v in ipairs(map.coins) do
            if v.taken == false then
                v:collidePlayer(self)
            end
        end

        self:checkTiles()

        if self.y > MAPH then
            self:kill() 
        end
        
        -- Trigger slow-motion if the player has three ghosts
        if self.hasGhosts and #self.ghosts >= 3 then
            love.timer.sleep(0.1) -- Adjust the sleep time for the desired slow-motion effect
        end

    elseif self.state == STATE_WAIT then
        self.frame = self.frame + dt
        if self.frame > 8 then
            self.state = STATE_RUNNING
            self.frame = 0
        end

    elseif self.state == STATE_BURNING then
        if self.frame >= 8 then
            self:kill()
        end
    end
end

function Player3:update(dt)
	self.frame = self.frame or 0
	self.invul = self.invul or 0
    self.frame = self.frame + dt * 13
    if self.invul > 0 then
        self.invul = self.invul - dt
    end
    

    if self.state == STATE_RUNNING then
        -- Set horizontal speed according to direction
        self.xspeed = self.dir * PLAYER_SPEED

        self.yspeed = self.yspeed + GRAVITY * dt
        self.yspeed = min(self.yspeed, MAX_SPEED)

        -- Keep vertical speed if still jumping
        if self.jump > 0 then
            self.yspeed = -JUMP_POWER
        end

        -- move in X and Y direction
        self.onGround = false
        self:moveX(self.xspeed * dt)
        self:moveY(self.yspeed * dt)

        -- check wall jump
        self.onWall = false
        if self.onGround == false then
            if self.dir == -1 and collidePoint(self.x - 6, self.y + 5) or collidePoint(self.x - 6, self.y + 15) then
                self.onWall = true
            elseif self.dir == 1 and collidePoint(self.x + 6, self.y + 5) or collidePoint(self.x + 6, self.y + 15) then
                self.onWall = true
            end
        end

        for i, v in ipairs(map.enemies) do
            if v.collidePlayer then
                if v:collidePlayer(self) and self.invul <= 0 then
                    love.audio.play(snd.Hurt)
                    self:kill()
                end
            end
        end

        for i, v in ipairs(map.entities) do
            if v.collidePlayer then
                v:collidePlayer(self)
            end
        end

        for i, v in ipairs(map.coins) do
            if v.taken == false then
                v:collidePlayer(self)
            end
        end

        self:checkTiles()

        if self.y > MAPH then
            self:kill() 
        end
        
      -- Trigger slow-motion if the player has three ghosts
        if self.hasGhosts and #self.ghosts >= 4 then
            timeScale=0.2
            myYSpeed = self.yspeed
            else
            timeScale=1
            
        end

    elseif self.state == STATE_WAIT then
        self.frame = self.frame + dt
        if self.frame > 8 then
            self.state = STATE_RUNNING
            self.frame = 0
        end

    elseif self.state == STATE_BURNING then
        if self.frame >= 8 then
            self:kill()
        end
    end
end


function Player3:kill(...)
	deaths = deaths + 1
	map.deaths = map.deaths + 1
	untakeCoins()
	self:respawn(...)
end

function Player3:checkTiles()
	local bx, by, tile
	for i=1, #COL_OFFSETS do
		bx = floor((self.x+COL_OFFSETS[i][1]) / TILEW)
		by = floor((self.y+COL_OFFSETS[i][2]) / TILEW)
		tile = fgtiles(bx,by)
		if tile ~= nil then
			-- Check collision with spikes
			if tile.id >= TILE_SPIKE_S and tile.id <= TILE_SPIKE_E then
				if collideSpike(bx,by,self) then
					addSparkle(self.x,self.y+20,32,COLORS.red)
					love.audio.play(snd.Hurt)
					self:kill()
					return
				end
			-- Check collision with lava
			elseif tile.id == TILE_LAVA_TOP then -- Don't check for TILE_LAVA, shouldn't be necessary
				if collideLava(bx,by,self) then
					self.frame = 0
					self.state = STATE_BURNING
					addSparkle(self.x,self.y+20,32,COLORS.red,1,-50)
					love.audio.play(snd.Burn)
					return
				end
			end
		end
	end
end

function Player3:keypressed(k)
	if self.state == STATE_RUNNING then
		if self.onGround == true then
			self.jump = MAX_JUMP
			self:addGhost()
			love.audio.play(snd.Jump)
			jumps = jumps + 1

		elseif self.onWall == true then
			self.jump = MAX_JUMP
			self:addGhost()
			if self.dir == 1 then
				self.dir = -1
			else
				self.dir = 1
			end
			love.audio.play(snd.Jump)
			jumps = jumps + 1
		end
	elseif self.state == STATE_WAIT then
		self.state = STATE_RUNNING
		self.frame = 0
	end
end

function Player3:keyreleased(k)
	if self.state == STATE_RUNNING then
		if self.jump > 0 then
			self.jump = 0
		end
	end
end

function Player3:moveY(dist)
	local bx, by
	local newy = self.y + dist
	local col = false

	-- Check the maximum y offset for each colliding tile
	for i=1, #COL_OFFSETS do
		bx = floor((self.x+COL_OFFSETS[i][1]) / TILEW)
		by = floor((newy+COL_OFFSETS[i][2]) / TILEW)

		if isSolid(bx, by) == true then
			col = true
			if dist > 0 and by*TILEW-20 < newy then
				newy = by*TILEW-20.0001
			elseif dist < 0 and (by+1)*TILEW-2 > newy then
				newy = (by+1)*TILEW-1.9999
			end
		end
	end
	-- Move allowed distance
	self.y = newy
	-- Set new state if colliding with ground
	if col == true then
		self.yspeed = 0

		if dist > 0 then
			self.onGround = true
			-- remove ghosts if any
			if self.hasGhosts then
				self:removeGhosts()
			end
		elseif dist < 0 then
			self.jump = 0
		end
	end
	-- Remove dist from jumping power
	if dist < 0 then
		self.jump = self.jump + dist
	end
end

function Player3:moveX(dist)
	local newx = self.x + dist
	local col = false

	for i=1, #COL_OFFSETS do
		local bx = floor((newx+COL_OFFSETS[i][1]) / TILEW)
		local by = floor((self.y+COL_OFFSETS[i][2]) / TILEW)

		if isSolid(bx, by) == true then
			col = true
			if dist > 0 and bx*TILEW-5.5 < newx then
				newx = bx*TILEW-5.5001
			elseif dist < 0 and (bx+1)*TILEW+5.5 > newx then
				newx = (bx+1)*TILEW+5.5001
			end
		end
	end
	self.x = newx
end

function Player3:addGhost()
	if self.onWall == true then
		table.insert(self.ghosts, {self.x,self.y,quads.player_wall,self.dir})
	else
		local frame = floor(self.frame % 6)
		table.insert(self.ghosts, {self.x,self.y,quads.player_run[frame],self.dir})
	end
	self.hasGhosts = true
end

function Player3:removeGhosts()
	for i,v in ipairs(self.ghosts) do
		addSparkle(v[1],v[2]+10,8,COLORS.yellow)
	end
	self.hasGhosts = false
	self.ghosts = {}
end


function Player3:draw()

  -- Light position calculation: move light to the right of the player (offset by 20 units in x direction)
    lightPos2 = {
        self.x / 7000  + 0.1,  -- Adjust the X position of the light (e.g., to the right of the player)
        self.y / 1000 + 0.1,  -- Slight Y offset, can adjust as needed (e.g., above the player)
        0.1 + math.sin(sunAngle)  -- Example Z value (light intensity, or other purpose)
    }
    
    vectorNormalsNoisePerformantShader:send("LightPos2", lightPos2)
    vectorNormalsNoisePerformantShader:send("LightColor2", {1.0, 1.0, 1.0, 1.0})  -- Set the light color


	local blink = false

	if self.state == STATE_RUNNING then
		-- Blink
		if self.invul > 0 then
			if floor(self.invul*INVUL_TIME*20) % 2 == 1 then
				blink = true
			end
		end
	
		-- draw ghosts
		if self.hasGhosts == true then
			love.graphics.setColor(COLORS.green)
			for i,v in ipairs(self.ghosts) do
				vectorNormalsNoisePerformantShader:send("u_normals", playerNormals)
				--vectorNormalsNoisePerformantShader:send("u_heightmap", playerHeight)
				love.graphics.draw(imgPlayerW, v[3], v[1], v[2], 0,v[4],1,6.5)
			end
			love.graphics.setColor(0,1,0,1)
		end
		-- Draw player
		if blink == false then
				vectorNormalsNoisePerformantShader:send("u_normals", playerNormals)
				--vectorNormalsNoisePerformantShader:send("u_heightmap", playerHeight)
			if self.onGround == true then
				if self.xspeed == 0 then
					if ASSETS == "new"  then
						love.graphics.draw(self.img, quads.player, self.x, self.y, 0,self.dir,1, 6.5)
				elseif ASSETS == "classic" then
							love.graphics.draw(self.img_orig, quads.player, self.x, self.y, 0,self.dir,1, 6.5)
				end
				else
					local frame = floor(self.frame % 6)
					if ASSETS == "new"  then
							love.graphics.draw(self.img, quads.player_run[frame], self.x, self.y, 0,self.dir,1, 6.5)
					elseif ASSETS == "classic" then
							love.graphics.draw(self.img_orig, quads.player_run[frame], self.x, self.y, 0,self.dir,1, 6.5)
					end
				end
			else
				if self.onWall == true then
					if ASSETS == "new"  then
						love.graphics.draw(self.img, quads.player_wall, self.x, self.y, 0,self.dir,1, 6.5)
					elseif ASSETS == "classic"  then
						love.graphics.draw(self.img_orig, quads.player_wall, self.x, self.y, 0,self.dir,1, 6.5)
					end
				else
					if ASSETS == "new"  then
						love.graphics.draw(self.img, quads.player_run[5], self.x, self.y, 0,self.dir,1, 6.5)
					elseif ASSETS == "classic"  then
						love.graphics.draw(self.img_orig, quads.player_run[5], self.x, self.y, 0,self.dir,1, 6.5)
					end
				end
			end
		end
	
	elseif self.state == STATE_WAIT then
			if ASSETS == "new"  then
				love.graphics.draw(self.img, quads.player_wait1, self.x, self.y, 0, self.dir, 1, 6.5)
			elseif ASSETS == "classic"  then
				love.graphics.draw(self.img_orig, quads.player_wait1, self.x, self.y, 0, self.dir, 1, 6.5)
			end

	elseif self.state == STATE_BURNING then
		local frame = floor(self.frame)
		if ASSETS == "new" then
			love.graphics.draw(self.img, quads.player_burn[frame], self.x, self.y, 0, self.dir, 1, 6.5)
		elseif ASSETS == "classic" then
			love.graphics.draw(self.img_orig, quads.player_burn[frame], self.x, self.y, 0, self.dir, 1, 6.5)
		end
	end
	
			vectorNormalsNoisePerformantShader:send("u_normals", smask) -- empty
			--vectorNormalsNoisePerformantShader:send("u_heightmap", smask) -- empty
			love.graphics.print("P3", self.x, self.y-20)
end
