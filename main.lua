ASSETS = "new"	--	Available assets: New & Classic
require("slam")
require("resources")
require("map")
require("player")
require("player2")
twoplayersMode = false
require("player3")
threeplayers = false
require("player4")
fourplayers = false
require("enemies")
require("spike")
require("particles")
require("checkpoint")
require("jumppad")
require("orb")
require("coin")
require("menu")
require("levelselection")
require("data")

local host = require('hostinfo')
--crbase(os)

--require ("shaders/combinedShader")
require ("shaders/clouds")
require ("shaders/waterwarp")
require ("shaders/NormalsAndNoise")
require ("shaders/NormalsAndNoisePerformant")
require ("shaders/NormalsAndNoiseAndShadowsandEnvmap")
require ("shaders/blueNoiseC64")
require ("shaders/blueNoiseEGAAMiga")
require ("shaders/blueNoiseSnes")
require ("shaders/blueNoiseZx")


 	require("NoobHub/main")	-- load NoobHub Client
require("chatHub/chatHub")	-- chat hud based on noobHUB
noobHubEnabled = false
chathudFlag = false

		
		--require ("shaders/wadingCaustics")
		require ("DayNightCycle")
		loadDayNightCycle()
		
		require("love-boids/boids")		-- Reynolds' boid library (https://github.com/Jehadel/love-boids)
	loadBoids()
	
	retroShader = false

local love = love
local min = math.min
local max = math.max
local floor = math.floor
local lg = love.graphics
local last_setScale

TILEW = 16
WIDTH = 355
HEIGHT = 200

scale_x, scale_y =0,0
timeScale = 1	-- variable used for slow motion


STATE_MAINMENU = 0
STATE_INGAME_MENU = 1
STATE_INGAME = 2
STATE_LEVEL_MENU = 3
STATE_LEVEL_COMPLETED = 4

local SETSCALE_COOLDOWN = 0.1

function love.load()

 --require("loadAssets")
	--WebP = require("love-webp")			-- webp library for love
	 LastOrientation="portrait"
 MobileOrientation="portrait"
 --love.window.setMode(1080, 2340, {resizable=true, borderless=false})

local success, error_message = pcall(function()
    joystick = love.joystick.getJoysticks()[1]
    require("gamepadinput")
    loadcontrollermappings()
    gamepadTimer = 0
    require ("gamepadRadialMenu")
	radialmenu = false
end)

if not success then
    print("Error:", error_message)
end

	loadSettings()
	loadData()

	lg.setDefaultFilter("nearest","nearest")
	lg.setBackgroundColor(COLORS.darkbrown)
	lg.setLineStyle("rough")

	loadImages()
	
	loadSounds()
	loadAScentOfEurope()
	
	createQuads()
	createMenus()

	gamestate = STATE_MAINMENU
	current_menu = main_menu

	player  = Player.create()
	player2 = Player2.create()
	player3 = Player3.create()
	player4 = Player4.create()
end

boidsUpdateTime = 0
  local timeAccumulator = 0
local updateInterval = 0.1  -- Adjust this value for your desired update speed
timeInt = 0
timeIntForward = true
local zoomFactor = 1.0
local smoothFactor = 0.05

--local lightColor2 = {1.0 - timeInt / 100, 0.0 + timeInt / 100, 1.0 - timeInt / 100, 3.0}


function love.update(dt)

 if noobHubEnabled == true then
		NoobHubClientUpdate(dt)			-- online multiplayer based on NoobHub (https://github.com/Overtorment/NoobHub)
	end
	
	if chathudFlag == true then
		chathudUpdate(dt)	-- this update a function that check gptCoroutine, a coroutine that works on a second thread to keep the game running 
	end				
--checkOrientation()
isjoystickbeingpressed(joystick,button)
updategetjoystickaxis(dt)
    -- Update main menu animations
    if gamestate == STATE_MAINMENU then
        timeAccumulator = timeAccumulator + dt
        if timeAccumulator >= updateInterval then
            if timeInt > 90 then timeIntForward = false
            elseif timeInt < 0 then timeIntForward = true end
            
            if timeIntForward == true then
                timeInt = timeInt + 1
            else
                timeInt = timeInt - 1
            end
            
            timeAccumulator = timeAccumulator - updateInterval
        end
        
        -- Update shader light positions
        --lightPos1 = {0.19, 0.48 - timeInt / 100, -0.10 - timeInt / 100}
        local lightColor = {0.0 + timeInt / 100.0, 1.0 - timeInt / 100, 1.0 - timeInt / 100, 3.0}
        local lightColor2 = {1.0 - timeInt / 100, 0.0 + timeInt / 100, 1.0 - timeInt / 100, 2.0}
	        
        

        
        local lightColor3 = {1.0, 1.0, 1.0, 0.7}
        
        --vectorNormalsNoisePerformantShader:send("LightPos1", lightPos1)
        vectorNormalsNoisePerformantShader:send("LightColor1", lightColor)
        --vectorNormalsNoisePerformantShader:send("LightPos2", lightPos2)
        vectorNormalsNoisePerformantShader:send("LightColor2", lightColor2)
        
        vectorNormalsNoisePerformantShader:send("AmbientColor", lightColor3)
    end

    if gamestate == STATE_INGAME then
        -- Update time for shaders
        timeAccumulator = timeAccumulator + dt
        if timeAccumulator >= updateInterval then
            timeInt = timeInt + 1
            timeAccumulator = timeAccumulator - updateInterval
        end

        vectorNormalsNoisePerformantShader:send("Time", timeInt * 10)

        -- Clamp frame time
        if dt > 0.06 then dt = 0.06 end
        if love.keyboard.isDown("s") then
            dt = dt / 100
        end

        boidsUpdateTime = boidsUpdateTime + dt
        updateBoids(dt)

        -- Progress game timer
        map.time = map.time + dt

        -- Update players
        player:update(dt * timeScale)
        if player.x > MAPW + 6 then
            gamestate = STATE_LEVEL_COMPLETED
        end

        -- Initialize bounding box
        local minX, maxX = player.x, player.x
        local minY, maxY = player.y, player.y

        -- Include other players in bounding box
        if twoplayersMode then
            player2:update(dt * timeScale)
            minX = math.min(minX, player2.x)
            maxX = math.max(maxX, player2.x)
            minY = math.min(minY, player2.y)
            maxY = math.max(maxY, player2.y)
            if player2.x > MAPW + 6 then gamestate = STATE_LEVEL_COMPLETED end
        end

        if threeplayers then
            player3:update(dt * timeScale)
            minX = math.min(minX, player3.x)
            maxX = math.max(maxX, player3.x)
            minY = math.min(minY, player3.y)
            maxY = math.max(maxY, player3.y)
            if player3.x > MAPW + 6 then gamestate = STATE_LEVEL_COMPLETED end
        end

        if fourplayers then
            player4:update(dt * timeScale)
            minX = math.min(minX, player4.x)
            maxX = math.max(maxX, player4.x)
            minY = math.min(minY, player4.y)
            maxY = math.max(maxY, player4.y)
            if player4.x > MAPW + 6 then gamestate = STATE_LEVEL_COMPLETED end
        end

        -- Update global entities
        Spike.globalUpdate(dt)
        Jumppad.globalUpdate(dt)
        Coin.globalUpdate(dt)

        -- Update enemies
        for i = #map.enemies, 1, -1 do
            local enem = map.enemies[i]
            if enem.alive then
                if enem.update then enem:update(dt) end
            else
                table.remove(map.enemies, i)
            end
        end

        -- Update particles
        for i = #map.particles, 1, -1 do
            local part = map.particles[i]
            if part.alive then
                part:update(dt)
            else
                table.remove(map.particles, i)
            end
        end

          -- Center the camera between the players
        local centerX = (minX + maxX) / 2
        local centerY = (minY + maxY) / 2
        
        
lightPos1 = {0.19, 0.18 + centerY/5000, 0.18}
lightPos2 = {0.50  , 0.78  + centerY/5000, 0.18}
vectorNormalsNoisePerformantShader:send("LightPos1", lightPos1)
vectorNormalsNoisePerformantShader:send("LightPos2", lightPos2)

     
-- Adjust camera Y position using a weighted focus
-- Weighted toward the players' Y positions instead of relying solely on the center
local weightedCenterY = (player.y 
    + (twoplayersMode and player2.y or 0) 
    + (threeplayers and player3.y or 0) 
    + (fourplayers and player4.y or 0)) 
    / (1 + (twoplayersMode and 1 or 0) + (threeplayers and 1 or 0) + (fourplayers and 1 or 0))

-- Add a margin to the bounding box
local margin = 20
local boxWidth = (maxX - minX) + margin
local boxHeight = (maxY - minY) + margin

-- Calculate zoom factor
local zoomX = WIDTH / boxWidth
local zoomY = HEIGHT / boxHeight
zoomFactor = math.min(zoomX, zoomY, 1.0) -- Limit zoom

-- Adjust camera position considering zoom and focus
tx = math.min(math.max(0, centerX - (WIDTH / (2 * zoomFactor))), MAPW - WIDTH / zoomFactor)
-- Smooth camera movement using linear interpolation
if twoplayersMode == false then
	smoothFactor = 0.05 -- Adjust this value for smoothness (lower = smoother)
else
	smoothFactor = 0.001 -- Adjust this value for smoothness (lower = smoother)
end
ty = ty + smoothFactor * (math.min(math.max(0, weightedCenterY - (HEIGHT / (2 * zoomFactor))), MAPH - HEIGHT / zoomFactor) - ty)



    end

    -- Update credits menu if active
    if current_menu == credits_menu then
        credits_menu:update(dt)
    end
end



	
	local timer = 0
	--waterwarp = require 'waterwarp'	--warp effect intensity 10
	
	
	
function love.draw()
    
	         if timeScale < 1 then
			zoomFactor = zoomFactor + (-myYSpeed) / 5000
			if zoomFactor<0.4 then zoomFactor=0.4 end
			lg.scale(scale_x/1.4+zoomFactor, scale_y+zoomFactor)
			love.graphics.translate(-50,-50)
			if (1/zoomFactor>0.6) then	-- prevent the game from crashing by checking that the pitch is not negative
				snd.Music:setPitch(1/zoomFactor) -- Adjust the speed factor as needed
			end
	
    else
        --lg.scale(scale_x/1.4, scale_y)
        snd.Music:setPitch(1)
        --zoomFactor=1
        lg.scale(scale_x/1.4*zoomFactor, scale_y*zoomFactor)
    end
	
	
		if MobileOrientation == "portrait" then
		love.graphics.rotate( 0 )
		love.graphics.translate(10,0)
	elseif MobileOrientation == "landscape" then
		love.graphics.rotate( 1.57 )
		love.graphics.translate(0,-510)
		love.graphics.scale(0.39,2.2)
	end
	
		--love.graphics.rotate( 1.57 )
		--love.graphics.translate(0,-400)
	-- STATE: In game
	--if current_map == 3 then
	    lightPos1 = {0.69, 0.48, 0.18}  -- Move light source to x=500, y=300 in world coordinates (adjust as needed)
        
        -- Increase the light intensity by boosting the alpha (4th value)
        local lightColor = {1.0, 1.0, 1.0, 2.0}  -- Light color with increased intensity (alpha > 1)
        
		 -- Set shader uniforms for primary light source
        vectorNormalsNoisePerformantShader:send("LightPos1", lightPos1)
        vectorNormalsNoisePerformantShader:send("LightColor1", lightColor)
         local ambientColor = {1.0, 1.0, 1.0, 0.7}  -- Light color with increased intensity (alpha > 1)
        
        vectorNormalsNoisePerformantShader:send("AmbientColor", ambientColor)
        --vectorNormalsNoisePerformantShader:send("GrainIntensity", 0.1)
         
        -- Set other shader parameters for normal mapping
        vectorNormalsNoisePerformantShader:send("Resolution", {love.graphics.getWidth(), love.graphics.getHeight()})
        vectorNormalsNoisePerformantShader:send("Falloff", {0.2, 1.0, 10.0})
        
        if current_map == 3 then
        --vectorNormalsNoiseShadowEnvmapShader
          vectorNormalsNoiseShadowEnvmapShader:send("LightPos1", lightPos1)
        vectorNormalsNoiseShadowEnvmapShader:send("LightColor1", {1.0, 1.0, 1.0, 1.0})
                 
        vectorNormalsNoiseShadowEnvmapShader:send("AmbientColor", ambientColor)
        
         
        -- Set other shader parameters for normal mapping
        vectorNormalsNoiseShadowEnvmapShader:send("Resolution", {love.graphics.getWidth(), love.graphics.getHeight()})
        vectorNormalsNoiseShadowEnvmapShader:send("Falloff", {0.2, 1.0, 1.0})
        vectorNormalsNoiseShadowEnvmapShader:send("Time", timeInt * 1)  -- Send time to shader
        
		-- Reflection intensity
		vectorNormalsNoiseShadowEnvmapShader:send("ReflectionIntensity", 0.2)

		vectorNormalsNoiseShadowEnvmapShader:send("u_envMap", envMap)
      end  	
        	
        	if retroShader == true then
				local pixelSize = 20.0  -- Example pixel size for strong pixelation
				blueNoiseZxDitherShader:send("pixelSize", pixelSize)
				love.graphics.setShader(blueNoiseZxDitherShader)
			else
				
				if current_map == 3 then
				
				
					love.graphics.setShader(vectorNormalsNoiseShadowEnvmapShader)
				else
					love.graphics.setShader(vectorNormalsNoisePerformantShader)
				end
			end
			--love.graphics.setShader(blueNoiseSnes)
			
	
	if gamestate == STATE_INGAME then
	
	         -- Calculate the primary light position based on sunAngle
        --lightPos1 = {-0.1 + math.cos(sunAngle), 0.2 + math.sin(sunAngle), 0.1 + math.sin(sunAngle)}
        --print(lightPos1[3])
        --0.89
        --0.28
        --0.18
        
        --lightPos1 = {1,1,1}-- Adjust the primary light position (move it closer to the scene center for better illumination)
        
     local width, height = 512, 512
        
--print(lightPos2[1])

		lg.push()
	
		
		drawIngame()
		
		love.graphics.setShader()
		
		lg.pop()
		drawIngameHUD()
	elseif gamestate == STATE_INGAME_MENU then
		lg.push()
		drawIngame()
		lg.pop()
		current_menu:draw()
	elseif gamestate == STATE_MAINMENU then
		--love.graphics.draw(imgTitle, quads.title, 0,0, 0, WIDTH/900)
		--love.graphics.draw(imgTitle, 0,0, 0, WIDTH/1800)
		if ASSETS == "new" then

        		
		vectorNormalsNoisePerformantShader:send("u_normals", imgTitleBckhHeight)
		--vectorNormalsNoisePerformantShader:send("u_heightmap", imgTitleBckhHeight)
		
		love.graphics.draw(imgTitleBck, 0,0, 0, WIDTH/1650)
		vectorNormalsNoisePerformantShader:send("u_normals", TitleCatHeight)
		--vectorNormalsNoisePerformantShader:send("u_heightmap", TitleCatNormals)
		love.graphics.draw(imgTitleCat, 0,0, 0, WIDTH/1650)
		
		if twoplayersMode == true then
			love.graphics.setColor(1,0,0,1)
			love.graphics.draw(imgTitleCat, 100,0, 0, WIDTH/1650)
		end
		
		if threeplayers == true then
			love.graphics.setColor(0,1,0,1)
			love.graphics.draw(imgTitleCat, 200,0, 0, WIDTH/1650)
		end
		
		if fourplayers == true then
			love.graphics.setColor(0,0,1,1)
			love.graphics.draw(imgTitleCat, 300,0, 0, WIDTH/1650)
		end
		
		vectorNormalsNoisePerformantShader:send("u_normals", imgTitleFrgNormals)
		--vectorNormalsNoisePerformantShader:send("u_heightmap", imgTitleFrghHeight)
		love.graphics.draw(imgTitleFrg, 0,0, 0, WIDTH/1650)
		
		vectorNormalsNoisePerformantShader:send("u_normals", titleTitleNormals)
		--vectorNormalsNoisePerformantShader:send("u_heightmap", titleTitleHeight)
	
		love.graphics.draw(imgTitleTitle, 0,0, 0, WIDTH/1650)
			
			vectorNormalsNoisePerformantShader:send("u_normals", smask) -- empty
			--vectorNormalsNoisePerformantShader:send("u_heightmap", smask) -- empty
		elseif ASSETS == "classic" then
		love.graphics.draw(imgTitle_orig, 0,0, 0, WIDTH/1650)
		end
		current_menu:draw()
		
		 -- Apply water warp shader only to imgTitleFrgWtr
		 timer=timer+0.01
		if timer>100 then timer=0 end
        love.graphics.setShader(warpshader)
        warpshader:send("time", timer)
        
        love.graphics.draw(imgTitleFrgWtr, 0, 0, 0, WIDTH / 1650)
        --love.graphics.setShader() -- Reset the shader to default
		
		
	elseif gamestate == STATE_LEVEL_MENU then
		LevelSelection.draw()
	elseif gamestate == STATE_LEVEL_COMPLETED then
		lg.push()
		drawIngame()
		lg.pop()
		drawCompletionHUD()
	end
	
	if chathudFlag == true then
		chathuddraw()
	end
end

numImages=20

function drawClouds(xOffset,xScale)
			
red,green,blue = 1,1,1			     
     --local cloudColor = {1.0, 1.0, 1.0} -- White clouds
local cloudColor = {red/1 ,green/1,blue/1} -- White clouds
--local skyColor = {0.5, 0.8, 1.0} -- Light blue sky
local skyColor = {0.5 , 0.5, 0.5}
local alpha = 0.07 -- 80% opacity
local cloudSpeed = 1
 
  -- Use cloud factor to affect the cloud movement (send time and speed to shader)
    cloudsShader:send("cloudColor", cloudColor)  -- Send cloud color (can be white or dynamic)
    cloudsShader:send("skyColor", skyColor)  -- Sky color (e.g., black for night, blue for day)
    cloudsShader:send("alpha", alpha)  -- Send cloud opacity

    -- Send other shader parameters as before
    cloudsShader:send("time", timeInt)  -- Time to control cloud movement
    cloudsShader:send("parallaxOffset", {cloudSpeed, cloudSpeed})  -- Horizontal movement speed of clouds
    cloudsShader:send("resolution", {love.graphics.getWidth(), love.graphics.getHeight()})

    -- Render the clouds using the updated shader settings
  love.graphics.setShader(cloudsShader)
    love.graphics.setColor(cloudColor)
    --love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
    --love.graphics.draw(imgFog, xOffset,200, 0, xScale, 0.038)
    --love.graphics.draw(imgFog, xOffset,-20, 0, xScale, 0.038)
    love.graphics.draw(imgFog, xOffset,50, 0, xScale, 0.138)
    love.graphics.setShader()	
end

function drawClouds2(xOffset,xScale)
			
red,green,blue = 1,1,1			     
     --local cloudColor = {1.0, 1.0, 1.0} -- White clouds
local cloudColor = {red/1 ,green/1,blue/1} -- White clouds
--local skyColor = {0.5, 0.8, 1.0} -- Light blue sky
local skyColor = {0.5 , 0.5, 0.5}
local alpha = 0.2 -- 80% opacity
local cloudSpeed = 2
 
  -- Use cloud factor to affect the cloud movement (send time and speed to shader)
    cloudsShader:send("cloudColor", cloudColor)  -- Send cloud color (can be white or dynamic)
    cloudsShader:send("skyColor", skyColor)  -- Sky color (e.g., black for night, blue for day)
    cloudsShader:send("alpha", alpha)  -- Send cloud opacity

    -- Send other shader parameters as before
    cloudsShader:send("time", timeInt)  -- Time to control cloud movement
    cloudsShader:send("parallaxOffset", {cloudSpeed, cloudSpeed})  -- Horizontal movement speed of clouds
    cloudsShader:send("resolution", {love.graphics.getWidth(), love.graphics.getHeight()})

    -- Render the clouds using the updated shader settings
  love.graphics.setShader(cloudsShader)
    love.graphics.setColor(cloudColor)
    --love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
  		
				love.graphics.draw(imgFog, xOffset,0, 0, xScale, 0.138)
    love.graphics.setShader()	
end

function drawIngame()

	-- Draw the background layer with slower movement
	lg.translate(-tx/4, -ty/2)
	if current_map < 5 then
	
     -- Draw deeper layer of the background parallax image to cover the screen width
    for i = 1, numImages do
        local xOffset = (i - 1) * bck2Image:getWidth() * 0.18
        local xScale = 0.18
        if i % 2 == 0 then
            -- Invert the image on the x-axis for even iterations
            xScale = -0.18
            xOffset = xOffset + bck2Image:getWidth() * 0.18
        end
        if ASSETS == "new" then
		
			
			if current_map == 3 then
					
					vectorNormalsNoiseShadowEnvmapShader:send("u_normals",bck2Extended2Normals)
					vectorNormalsNoiseShadowEnvmapShader:send("u_heightmap",bck2Extended2height)
					
				love.graphics.draw(bck2Image2, xOffset, 0, 0, xScale, 0.18)
					
			else
				vectorNormalsNoisePerformantShader:send("u_normals", bck1ExtendedNormals)
				love.graphics.draw(bck2Image, xOffset, 0, 0, xScale, 0.18)
			end
			
			vectorNormalsNoiseShadowEnvmapShader:send("u_normals", smask)
		end
		
    end
	end
	-- Compensate for accumulated translations before drawing the foreground layer
				lg.translate(tx/4, -ty/2)
	
	       -- Draw the Lamp layer with medium movement
		 	if current_map>4 then
				lg.translate(-tx/1, 0)
			else
				lg.translate(-tx/3, ty/2)
			end
    
if current_map > 4 then
       for i = 1, numImages do
            local xOffset = (i - 1) * templeWalls:getWidth() * 0.12 -- Adjust multiplier as needed
        local xScale = 0.18
        if i % 2 == 0 then
            -- Invert the image on the x-axis for even iterations
            xScale = -0.18
            --xOffset = xOffset + bck2Image:getWidth() * 0.18
        end
        if ASSETS == "new" then
			vectorNormalsNoisePerformantShader:send("u_normals", templeWallsNormals)
			--vectorNormalsNoisePerformantShader:send("u_heightmap", templeWallsHeight)
			
			
			love.graphics.draw(templeWalls, xOffset, 80, 0, xScale, -0.18)
			--vectorNormalsNoisePerformantShader:send("u_heightmap", smask) -- empty
			love.graphics.draw(templeWalls, xOffset, 80, 0, xScale, 0.18)
			--drawClouds2(xOffset,xScale)
	
		end
    end
		
    else
    	
	-- Draw Lamp layer of the background parallax image to cover the screen width
    for i = 1, numImages do
        local xOffset = (i - 1) * bck2Lamp:getWidth() * 0.28
        local xScale = 0.18
        if i % 2 == 0 then
            -- Invert the image on the x-axis for even iterations
            xScale = -0.18
            xOffset = xOffset + bck2Lamp:getWidth() * 0.28
        end
        if ASSETS == "new" then
			vectorNormalsNoisePerformantShader:send("u_normals", bck2LampNormals)
			--vectorNormalsNoisePerformantShader:send("u_heightmap", bck2Lampheight)
			love.graphics.draw(bck2Lamp, xOffset,80, 0, xScale, 0.18)
		end
    end
	
	end
	


    
    --compensate parallax
    	if current_map>4 then
			lg.translate(tx/1,  0)
    	else
			lg.translate(tx/3, -ty/2)
    	end

	if current_map > 4 then
		lg.translate(-tx/3, ty/2)
	else
		lg.translate(-tx/2, ty/2)
	end
	-- Draw Lamp layer of the background parallax image to cover the screen width
    local xOffset= 0
    for i = 1, numImages do
        if current_map > 4 then
			local xOffset = (i - 1) * imgFog:getWidth() * 0.18
        else
			local xOffset = (i - 1) * bckImage:getWidth() * 0.18
		end
        local xScale = 0.18
        if i % 2 == 0 then
            -- Invert the image on the x-axis for even iterations
            xScale = -0.18
            if current_map > 4 then
				xOffset = xOffset + imgFog:getWidth() * 0.18
            else
				xOffset = xOffset + bckImage:getWidth() * 0.18
            end
        end
        if current_map > 4 then
			--love.graphics.draw(imgFog, xOffset,80, 0, xScale, 0.18)
			
        else
			if ASSETS == "new" then
			--vectorNormalsNoisePerformantShader:send("u_normals", smask)
			--vectorNormalsNoisePerformantShader:send("u_heightmap", smask)
			
				love.graphics.draw(bckImage, xOffset,180, 0, xScale, 0.18)
				
					love.graphics.setShader(vectorNormalsNoisePerformantShader)
					vectorNormalsNoiseShadowEnvmapShader:send("u_normals",smask)
					vectorNormalsNoiseShadowEnvmapShader:send("u_heightmap",smask)
			end
		end
    end
	
	if ASSETS == "classic" then
		
	end
	 --compensate parallax	
	 if current_map > 4 then
		lg.translate(tx/3, -ty)
	 else
		lg.translate(tx/2, -ty)
	end

-- Draw the foreground layer with faster movement
	lg.translate(-tx, ty/2)
	
	 -- Draw background image
    --love.graphics.draw(bckImage, 0, 80, 0, 0.2,0.2)
    
     -- Calculate the number of background images needed to cover the screen width
    --local numImages = math.ceil(WIDTH / (bckImage:getWidth() * 0.2))
    

    -- Draw multiple instances of the background image to cover the screen width
    --for i = 1, numImages do
        --love.graphics.draw(bckImage, (i - 1) * bckImage:getWidth() * 0.2, 80, 0, 0.2, 0.2)
    --end
    

    
	--map:setDrawRange(tx-20,ty,WIDTH+150,HEIGHT+20)
	-- Calculate draw range for the map based on zoomFactor
local marginX = 20 / zoomFactor -- Scale margins inversely with zoom
local marginY = 20 / zoomFactor

-- Visible bounds adjusted for zoomFactor and margins
local drawX = tx - marginX
local drawY = ty - marginY
local drawWidth = (WIDTH * 1.3/ zoomFactor) + 2 * marginX
local drawHeight = (HEIGHT * 1.3/ zoomFactor) + 2 * marginY

-- Set the map's draw range
map:setDrawRange(math.floor(drawX), math.floor(drawY), math.ceil(drawWidth), math.ceil(drawHeight))



	map:draw()

	player:draw()
	
	if twoplayersMode == true then
			love.graphics.setColor(1,0,0,1)
			player2:draw()
		end
		
		if threeplayers == true then
			love.graphics.setColor(0,1,0,1)
			player3:draw()
		end
		
		if fourplayers == true then
			love.graphics.setColor(0,1,1,1)
			player4:draw()
		end

	for i,v in ipairs(map.entities) do
		v:draw()
	end

	for i,v in ipairs(map.coins) do
		v:draw()
	end

	for i,v in ipairs(map.enemies) do
		if v.draw then
			v:draw()
		end
	end

	for i,v in ipairs(map.particles) do
		v:draw()
	end
	
	    
	
	
    --compensate parallax
    	if current_map>4 then
			lg.translate(tx, -ty/2)
    	else
			--lg.translate(tx/3, -ty/2)
    	end
    	
    			if current_map > 4 then
		lg.translate(-tx*1.5, -ty*1)
	else
		--lg.translate(-tx/2, ty/2)
	end
	
	-- Draw front Temple layer
    local xOffset= 0
    for i = 1, numImages do
        if current_map > 4 then
			local xOffset = (i - 1) * imgFrontTemple:getWidth() * 1
        else
			--local xOffset = (i - 1) * bckImage:getWidth() * 0.18
		end
        local xScale = 0.3
        if i % 2 == 0 then
            -- Invert the image on the x-axis for even iterations
            xScale = -0.3
            if current_map > 4 then
				xOffset = xOffset + imgFrontTemple:getWidth() * 1
            else
				--xOffset = xOffset + bckImage:getWidth() * 0.18
            end
        end
        if current_map > 4 then

			vectorNormalsNoisePerformantShader:send("u_normals", imgFrontTempleNormals)
			--vectorNormalsNoisePerformantShader:send("u_heightmap", imgFrontTempleheight)
			love.graphics.draw(imgFrontTemple, xOffset,120, 0, xScale, 0.3)
        else
			--love.graphics.draw(bckImage, xOffset,180, 0, xScale, 0.18)
		end
    end
	
	 --compensate parallax	
	 if current_map > 4 then
		lg.translate(tx*1.5, ty*1)
	 else
		--lg.translate(tx, -ty)
	end
	
	
	if current_map > 4 then
		lg.translate(-tx/3, ty/2)
	else
		--lg.translate(-tx/2, ty/2)
	end
	-- Draw front Temple fog
    local xOffset= 0
    for i = 1, numImages do
        if current_map > 4 then
			local xOffset = (i - 1) * imgFog:getWidth() * 1
        else
			--local xOffset = (i - 1) * bckImage:getWidth() * 0.18
		end
        local xScale = 1
        if i % 2 == 0 then
            -- Invert the image on the x-axis for even iterations
            xScale = -1
            if current_map > 4 then
				xOffset = xOffset + imgFog:getWidth() * 1
            else
				--xOffset = xOffset + bckImage:getWidth() * 0.18
            end
        end
        if current_map > 4 then
			love.graphics.draw(imgFog, xOffset,-30, 0, xScale, 0.18*1)
			
        else
			--love.graphics.draw(bckImage, xOffset,180, 0, xScale, 0.18)
		end
		if current_map > 7 then
			drawClouds(xOffset,xScale)
		end
    end
	--vectorNormalsNoisePerformantShader:send("u_heightmap", smask)
	 --compensate parallax	
	 if current_map > 4 then
		lg.translate(tx/3, -ty)
	 else
		lg.translate(tx/2, -ty)
	end
	
drawBoids()

	if radialmenu == true then
		drawradialmenu()
	end
	
end



function drawIngameHUD()
	local time = getTimerString(map.time)

	-- Draw text
	lg.draw(imgHUD, quads.hud_coin, 9, 10)
	lg.draw(imgHUD, quads.hud_skull, 48, 10)
	lg.setColor(1,1,1,1)
	lg.print(map.numcoins.."/5", 21, 13)
	lg.print(map.deaths, 67, 13)
	lg.printf(time, WIDTH-120, 13, 100, "right")
end

function drawCompletionHUD()
	lg.setColor(COLORS.menu)
	lg.rectangle("fill", 0,0, WIDTH,HEIGHT)
	lg.setColor(1,1,1,1)
	lg.draw(imgHUD, quads.text_level, 48,40)
	lg.draw(imgHUD, quads.text_cleared, 140,40)

	lg.print("COINS:", 66,75)
	lg.print(map.numcoins.."/5", 130,75)
	lg.print("DEATHS:", 66,95)
	lg.print(map.deaths, 130,95)
	lg.print("TIME:", 66,115)
	lg.print(getTimerString(map.time), 130,115)

	-- Draw coins difference
	if map.numcoins > level_status[current_map].coins then
		lg.setColor(COLORS.green)
		lg.print("+"..map.numcoins-level_status[current_map].coins, 160, 75)
	end
	-- Draw deaths difference
	if level_status[current_map].deaths then
		if map.deaths > level_status[current_map].deaths then
			lg.setColor(COLORS.red)
			lg.print("+"..map.deaths-level_status[current_map].deaths, 148, 95)
		elseif map.deaths < level_status[current_map].deaths then
			lg.setColor(COLORS.green)
			lg.print("-"..level_status[current_map].deaths-map.deaths, 148, 95)
		end
	end
	-- Draw time difference
	if level_status[current_map].time then
		if map.time > level_status[current_map].time then
			lg.setColor(COLORS.red)
			lg.print("+"..getTimerString(map.time-level_status[current_map].time), 194, 115)
		elseif map.time < level_status[current_map].time then
			lg.setColor(COLORS.green)
			lg.print("-"..getTimerString(level_status[current_map].time-map.time), 194, 115)
		end
	end


	lg.setColor(1,1,1,1)
	lg.print("PRESS ANY KEY TO CONTINUE", 55, 165)
end

function getTimerString(time)
	local msec = math.floor((time % 1)*100)
	local sec = math.floor(time % 60)
	local min = math.floor(time/60)
	return string.format("%02d'%02d\"%02d",min,sec,msec)
end

function love.keypressed(k)
    if gamestate == STATE_INGAME then
        if k == "space" then
            player:keypressed(k)
        elseif k == "rshift" and twoplayersMode == true then
            player2:keypressed(k)
        elseif k == "rshift" and twoplayersMode == false then
			twoplayersMode = true
            player2:respawn(map.startx, map.starty, 1)
            print("player 2 join the game")
			
        elseif k == "x" and threeplayers == true then
            player3:keypressed(k)  -- Player 3 responds only if threeplayers is true
        elseif k == "x" and threeplayers == false then
            threeplayers = true
            player3:respawn(map.startx, map.starty, 1)
            print("player 3 join the game")
        elseif k == "0" and fourplayers == true then
            player4:keypressed(k)  -- Player 4 responds only when fourplayers is true
        elseif k == "0" and fourplayers == false then
            fourplayers = true
            player4:respawn(map.startx, map.starty, 1)
            print("player 4 join the game")
        elseif k == "escape" then
            gamestate = STATE_INGAME_MENU
            current_menu = ingame_menu
            ingame_menu.selected = 1
        elseif k == "r" then
            player:kill()
        elseif k == "return" then
            reloadMap()
        elseif k == "c" then
            gamestate = STATE_LEVEL_COMPLETED
        end
    elseif gamestate == STATE_INGAME_MENU or gamestate == STATE_MAINMENU then
        current_menu:keypressed(k)
    elseif gamestate == STATE_LEVEL_MENU then
        LevelSelection.keypressed(k)
    elseif gamestate == STATE_LEVEL_COMPLETED then
        levelCompleted()
    elseif gamestate == 0 then
			if k == "rshift" and twoplayersMode == false then
				twoplayersMode = true
				player2:respawn(map.startx, map.starty, 1)
				print("player 2 join the game")
        elseif k == "x" and threeplayers == false then
            threeplayers = true
            player3:respawn(map.startx, map.starty, 1)
            print("player 3 join the game")
        
        elseif k == "0" and fourplayers == false then
            fourplayers = true
            player4:respawn(map.startx, map.starty, 1)
            print("player 4 join the game")
        end
    end
    

			
    
end


function love.keyreleased(k)
	if gamestate == STATE_INGAME then
		if k ~= "escape" and k ~= "r" then
			player:keyreleased(k) -- Player 1
			if twoplayersMode and k == "rshift" then
				player2:keyreleased(k)
			end
			if threeplayers and k == "x" then
				player3:keyreleased(k)
			end
			if fourplayers and k == "0" then
				player4:keyreleased(k)
			end
		end
	end
end


local touches = {}

function love.touchpressed(id, x, y)
	table.insert(touches, {id = id, x = x, y = y})
	if gamestate == STATE_INGAME then
		player:keypressed(' ')
	end
end

function love.touchreleased(id, x, y)
	local ignore = false
	for i = #touches, 1, -1 do
		if touches[i].moved then
			ignore = true
		end
		if touches[i].id == id then
			table.remove(touches, i)
		end
	end
	if not ignore then
		if gamestate == STATE_INGAME then
			player:keyreleased(' ')
		elseif gamestate == STATE_INGAME_MENU or gamestate == STATE_MAINMENU then
			current_menu:keypressed('return')
		elseif gamestate == STATE_LEVEL_MENU then
			LevelSelection.keypressed('return')
		elseif gamestate == STATE_LEVEL_COMPLETED then
			levelCompleted()
		end
	end
end

function send(key)
				if gamestate == STATE_INGAME_MENU or gamestate == STATE_MAINMENU then
					current_menu:keypressed(key)
				elseif gamestate == STATE_LEVEL_MENU then
					LevelSelection.keypressed(key)
				end
			end

function love.touchmoved(id, x, y)
	for i, v in ipairs(touches) do
		if v.id == id and not v.moved then
			local xv = x - v.x
			local yv = y - v.y
			local axv = math.abs(xv)
			local ayv = math.abs(yv)

			-- Ignore touchmoves below a certain threshold
			local threshold = 0.08

			if axv < threshold and ayv < threshold then
				return
			end

			v.moved = true
			send(key)
			
			if MobileOrientation == "portrait" then
			
			if ayv > axv then
				if yv > 0 then
					send('down')
				elseif yv < 0 then
					send('up')
				end
			else
				if xv > 0 then
					send('right')
				elseif xv < 0 then
					send('left')
				end
			end
			elseif MobileOrientation == "landscape" then	-- invert swipe coordinates when in landscape mode
			 -- Swap axes for landscape mode
				if ayv > axv then
					if yv > 0 then
						send('right')  -- Swipe up becomes right
					elseif yv < 0 then
						send('left')   -- Swipe down becomes left
					end
				else
					if xv > 0 then
						send('up')   -- Swipe right becomes down
					elseif xv < 0 then
						send('down')     -- Swipe left becomes up
					end
				end
			end
			
			
			return
		end
	end
end

function love.focus(f)
	if f == false and gamestate == STATE_INGAME then
		gamestate = STATE_INGAME_MENU
		current_menu = ingame_menu
		ingame_menu.selected = 1
	end
end

function setScale(scale)
	if last_setScale and love.timer.getTime()-last_setScale < SETSCALE_COOLDOWN then return end
	if scale < 1 or scale == SCALE then return end

	last_setScale = love.timer.getTime()
	SCALE = scale

	if host.isTouchDevice() then
		scale_x = love.graphics.getWidth() / WIDTH
		scale_y = love.graphics.getHeight() / HEIGHT
	else
		scale_x = scale
		scale_y = scale
	end

	SCREEN_WIDTH  = WIDTH*SCALE
	SCREEN_HEIGHT = HEIGHT*SCALE

--[[
	love.window.setMode(SCREEN_WIDTH, SCREEN_HEIGHT, {
		fullscreen=false,
		vsync=true,
		borderless = host.isTouchDevice()
	})
	--]]
end

function setResolution(w,h)
--[[
	love.window.setMode(w, h, {
		fullscreen=false,
		vsync=true,
		borderless = host.isTouchDevice()
	})
	--]]

	if w == 0 and h == 0 then
		SCREEN_WIDTH = lg.getWidth()
		SCREEN_HEIGHT = lg.getHeight()
		--[[
		love.window.setMode(SCREEN_WIDTH, SCREEN_HEIGHT, {
			fullscreen=false,
			vsync=true,
			borderless = host.isTouchDevice()
		})
		--]]
	else
		SCREEN_WIDTH = w
		SCREEN_HEIGHT = h
	end

	SCALE = 7
	while (20*16*SCALE) < SCREEN_WIDTH or (16*16*SCALE) < SCREEN_HEIGHT do
		SCALE = SCALE + 1
	end
	WIDTH = SCREEN_WIDTH/SCALE
	HEIGHT = SCREEN_HEIGHT/SCALE
end

function love.quit()
	saveSettings()
	saveData()
end

function checkOrientation()
	local desktopWidth, desktopHeight = love.window.getDesktopDimensions()
	--print(desktopWidth)
	--MobileOrientation="landscape"
		--if desktopWidth<1100 then MobileOrientation="portrait"
		if desktopWidth<desktopHeight then MobileOrientation="portrait"
			if LastOrientation==MobileOrientation then 

			else	
				LastOrientation=MobileOrientation
				scale_x = love.graphics.getWidth() / WIDTH
				scale_y = love.graphics.getHeight() / HEIGHT
				
				
				
			end
	elseif desktopWidth>desktopHeight  then MobileOrientation="landscape"
			if LastOrientation==MobileOrientation then 

			else	
				LastOrientation=MobileOrientation
				scale_x = love.graphics.getWidth() / WIDTH
				scale_y = love.graphics.getHeight() / HEIGHT
				
				
				
			end
	end
end



