function loadcontrollermappings()
	if love.filesystem.getInfo("gamecontrollerdb.txt") then
		--love.joystick.loadGamepadMappings("gamecontrollerdb.txt")
	end
	lastgamepadtouchedlevel="none"	-- this allows the gamepad to vibrate in first init (check line 68 of game/mainfunctions/gamepad/gamepadRadialMenu.lua where the condition is checked)
	
	  lastgamepadtouchedlevel = "none"
    movementTimer = 0
    movementDelay = 0.1
    axisMouseAcceleration = 10

   
    -- Get connected joysticks and assign them to players
    joysticks = love.joystick.getJoysticks()
    players = {}

    for i = 1, 4 do
        if joysticks[i] then
            players[i] = { joystick = joysticks[i], heldTime = 0 }
        end
    end
	
end

	-- Initialize variables for the updateButtonHeld(dt) function
	local buttonAHeldTime = 0
	local vibrationDelay = 0.75 -- Delay in seconds before vibration starts
	local lastVibrationTime = -vibrationDelay
	local vibrationInterval = 0.5 -- Vibration interval in seconds
	
	--joysticks = love.joystick.getJoysticks()
	--joystick = joysticks[1]


function updateButtonHeld(dt)

   for i, player in ipairs(players) do
        local joystick = player.joystick
        if joystick and joystick:isGamepadDown("a") then
            player.heldTime = player.heldTime + dt
            -- Check if it's time to start vibrating
            if player.heldTime >= vibrationDelay and love.timer.getTime() - lastVibrationTime >= vibrationInterval then
                joystick:setVibration(0.2, 0.2, 0.1)
                lastVibrationTime = love.timer.getTime()
            end
        else
            player.heldTime = 0
        end
	
		if buttonAHeldTime > 3 and (languagehaschanged or musichaschanged or skinhaschanged or optionsmenupressed) then
			print("Saving changes")
			savemygame()
			languagehaschanged = false
			musichaschanged = false
			skinhaschanged = false
			optionsmenupressed = false
			if gamestatus=="gameplusoptions" then palette=1 end
			love.timer.sleep(0.3)
		end
    end

end





-- Function to update joystick axis and control emulated mouse movement
function updategetjoystickaxis(dt)
	 gamepadTimer = gamepadTimer + dt
    for i, player in ipairs(players) do
        local joystick = player.joystick
        if joystick then
            -- Get values of joystick axes
            local leftxaxis = joystick:getGamepadAxis("leftx")
            local leftyaxis = joystick:getGamepadAxis("lefty")
            local tright = joystick:getGamepadAxis("triggerright")

            -- Check for movement and handle accordingly
            if math.abs(leftxaxis) > 0.2 then
                if leftxaxis > 0.2 then send('right', i) love.timer.sleep(0.3)
                elseif leftxaxis < -0.2 then send('left', i) love.timer.sleep(0.3)
                end
            end

            if math.abs(leftyaxis) > 0.2 then
                if leftyaxis > 0.4 then send('down', i) love.timer.sleep(0.3)
                elseif leftyaxis < -0.2 then send('up', i) love.timer.sleep(0.3)
                end
            end

            -- Trigger-based zoom (example placeholder)
            if tright > 0.2 then
                -- Handle zoom action for this player
            end
        end
            -- Pass the joystick to the radial menu update
            updateRadialMenu(joystick, i)
    end
end




function isjoystickbeingpressed(joystick,button)

	if joystick and joystick:isGamepadDown("a") then
		--player:keypressed(k)
		
	if gamestate == STATE_INGAME then
		player:keypressed(' ')
	end
	
	if gamestate == STATE_INGAME_MENU or gamestate == STATE_MAINMENU then
			current_menu:keypressed('return')
			love.timer.sleep(0.3)
		elseif gamestate == STATE_LEVEL_MENU then
			LevelSelection.keypressed('return')
			love.timer.sleep(0.3)
		elseif gamestate == STATE_LEVEL_COMPLETED then
			levelCompleted()
			love.timer.sleep(0.3)
		end

		
	elseif joystick and joystick:isGamepadDown("b") then
		--reloadMap()
	elseif joystick and joystick:isGamepadDown("x") then
		player:kill()
	elseif joystick and joystick:isGamepadDown("y") then
		gamestate = STATE_LEVEL_COMPLETED
	elseif joystick and joystick:isGamepadDown("start") then		
		gamestate = STATE_INGAME_MENU
			current_menu = ingame_menu
			ingame_menu.selected = 1
	end
	
		
end

function love.gamepadreleased(joystick, button)
    if gamestate == STATE_INGAME then
        if button == "a" then
            player:keyreleased(' ')
        --[[    if twoplayersMode then
                player2:keyreleased(' ')
            end
            if threeplayers then
                player3:keyreleased(' ')
            end
            if fourplayers then
                player4:keyreleased(' ')
            end
        --]]
        end
    end
end




--[[
-- Callback function when a gamepad button is pressed
function PlayStateisjoystickbeingpressed(joystick,button)
  	if joystick and joystick:isGamepadDown("leftshoulder") and (not play.tilt) then
            aplay(sounds.leftFlipper)
            pinball:moveLeftFlippers()
            print("test")
  	
  	end
end
--]]

--[[
-- Callback function when a gamepad button is pressed
function love.gamepadpressed(joystick,button)
  	if joystick and joystick:isGamepadDown("a") then
	
				if (Player.state:on("main")) then  Player:menuAction()
			elseif (Player.state:on("config")) then  Player:menuAction()
			elseif (Player.state:on("about")) then   about:forward()
            elseif (Player.state:on("scores")) then  Player.state:set("main")
			end
	end
end
--]]
