cloudsShader = love.graphics.newShader([[
extern float time;           // Animation time
extern vec2 parallaxOffset;  // Parallax scrolling offset
extern vec2 resolution;      // Screen resolution

// New uniforms for cloud color and alpha
extern vec3 cloudColor;      // RGB color for the clouds
extern vec3 skyColor;        // RGB color for the background sky
extern float alpha;          // Overall alpha transparency

// Simplex/Perlin noise function (pseudo-random)
float hash(vec2 p) {
    return fract(sin(dot(p, vec2(127.1, 311.7))) * 43758.5453123);
}

float noise(vec2 p) {
    vec2 i = floor(p);
    vec2 f = fract(p);

    vec2 u = f * f * (3.0 - 2.0 * f);

    return mix(mix(hash(i + vec2(0.0, 0.0)), 
                   hash(i + vec2(1.0, 0.0)), u.x),
               mix(hash(i + vec2(0.0, 1.0)), 
                   hash(i + vec2(1.0, 1.0)), u.x), u.y);
}

// Fractal Brownian Motion for cloud-like texture
float fbm(vec2 p, float time) {
    float value = 0.0;
    float amplitude = 0.5;
    float frequency = 1.0;
    for (int i = 0; i < 8; i++) {
        value += amplitude * noise(p * frequency);
        frequency *= 2.0 + sin(time * 0.1) * 0.1; // Oscillate frequency
        amplitude *= 0.5 + cos(time * 0.1) * 0.05; // Oscillate amplitude
    }
    return value;
}

vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords) {
    // Normalize coordinates based on resolution
    vec2 uv = screen_coords / resolution + parallaxOffset;

    // Scale UV coordinates for the noise
    vec2 p = uv * 4.0 - vec2(2.0); // Adjust scale for detail
    p += vec2(sin(time * 0.05) * 0.2, cos(time * 0.03) * 0.1); // Turbulent motion

    // Generate cloud pattern using fbm with animated parameters
    float clouds = fbm(p, time);

    // Soft threshold for cloud opacity
    float opacity = smoothstep(0.3, 0.7, clouds);

    // Blend between sky and cloud colors based on opacity
    vec3 finalColor = mix(skyColor, cloudColor, opacity);

    // Apply overall alpha transparency
    return vec4(finalColor, alpha) * color; // Apply vertex color if needed
}


]])
