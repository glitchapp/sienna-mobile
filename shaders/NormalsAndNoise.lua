vectorNormalsNoiseShader = love.graphics.newShader([[
    // Vertex Shader
    vec4 position(mat4 transform_projection, vec4 vertex_position) {
        return transform_projection * vertex_position;
    }
]], [[
    // Fragment Shader
    uniform sampler2D u_texture;   // diffuse map
    uniform sampler2D u_normals;   // normal map
    uniform sampler2D u_heightmap; // heightmap (for depth-based shadowing)

    uniform vec2 Resolution;       // resolution of screen
    uniform vec3 LightPos1;        // light position, normalized
    uniform vec4 LightColor1;      // light RGBA -- alpha is intensity
    uniform vec3 LightPos2;        // second light position, normalized
    uniform vec4 LightColor2;      // second light RGBA -- alpha is intensity
    uniform vec4 AmbientColor;     // ambient RGBA -- alpha is intensity 
    uniform vec3 Falloff;          // attenuation coefficients
    uniform float GrainIntensity;   // intensity of grain effect
    uniform int Time;              // For animating noise (use int instead of float)


    // Simple noise function
    float noise(vec2 uv) {
        return fract(sin(dot(uv, vec2(12.9898, 78.233)) + float(Time) * 0.1) * 43758.5453);
    }

    // Height map function
    float height(vec2 coords) {
        return Texel(u_heightmap, coords).r;  // Assuming heightmap is a single-channel texture (grayscale)
    }

    // Shadow calculation function
    float calculateShadow(vec2 coords, vec2 lightDir) {
        float currentHeight = height(coords);
        float shadowFactor = 1.0;  // Default to no shadow

        // Step along the light direction to check for occlusions
        for (float i = 1.0; i <= 5.0; i += 1.0) {  // Sampling steps
            vec2 offset = lightDir * i * 0.051;  // Step offset
            float sampleHeight = height(coords + offset);

            // If sampled height is greater than current height, apply shadow
            if (sampleHeight > currentHeight + 0.05 * i) {  // Shadow threshold
                shadowFactor = 0.5;  // Shadow strength
                break;
            }
        }
        return shadowFactor;
    }

    vec4 effect(vec4 vColor, Image texture, vec2 vTexCoord, vec2 pixcoord) {
        vec4 DiffuseColor = Texel(texture, vTexCoord);
        vec3 NormalMap = Texel(u_normals, vTexCoord).rgb;

        // First light calculations with shadowing
        vec3 LightDir1 = vec3(LightPos1.xy - (pixcoord.xy / Resolution.xy), LightPos1.z);
        LightDir1.x *= Resolution.x / Resolution.y;
        float D1 = length(LightDir1);
        vec3 N = normalize(NormalMap * 2.0 - 1.0);
        vec3 L1 = normalize(LightDir1);
        float shadowFactor1 = calculateShadow(vTexCoord, LightDir1.xy);
        vec3 Diffuse1 = (LightColor1.rgb * LightColor1.a) * max(dot(N, L1), 0.0) * shadowFactor1;

        // Second light calculations with shadowing
        vec3 LightDir2 = vec3(LightPos2.xy - (pixcoord.xy / Resolution.xy), LightPos2.z);
        LightDir2.x *= Resolution.x / Resolution.y;
        float D2 = length(LightDir2);
        vec3 L2 = normalize(LightDir2);
        float shadowFactor2 = calculateShadow(vTexCoord, LightDir2.xy);
        vec3 Diffuse2 = (LightColor2.rgb * LightColor2.a) * max(dot(N, L2), 0.0) * shadowFactor2;

        // Ambient lighting
        vec3 Ambient = AmbientColor.rgb * AmbientColor.a;
        float Attenuation1 = 1.0 / (Falloff.x + (Falloff.y * D1) + (Falloff.z * D1 * D1));
        float Attenuation2 = 1.0 / (Falloff.x + (Falloff.y * D2) + (Falloff.z * D2 * D2));

        vec3 Intensity = Ambient + Diffuse1 * Attenuation1 + Diffuse2 * Attenuation2;
        vec3 FinalColor = DiffuseColor.rgb * Intensity;

        // Add noise for grain effect
        float grain = noise(vTexCoord * 10.0) * float(GrainIntensity); // Scale texture coords for grain size
        FinalColor += grain; // Add grain to final color

        return vColor * vec4(clamp(FinalColor, 0.0, 1.0), DiffuseColor.a);
    }
]]) 
