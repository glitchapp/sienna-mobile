blueNoiseZxDitherShader = love.graphics.newShader([[
    extern Image paletteTexture;
    extern float intensity;
    extern Image blueNoiseTexture;
    extern float pixelSize;  // External variable to control pixelation

    vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords) {
        // Apply pixelation by modifying the texture coordinates
        // This step reduces the texture resolution based on the pixelSize
        vec2 pixelatedCoords = floor(texture_coords * vec2(love_ScreenSize.xy) / pixelSize) * pixelSize / vec2(love_ScreenSize.xy);

        // Get the grayscale color value from the texture using the pixelated coordinates
        vec4 texColor = Texel(texture, pixelatedCoords);
        float gray = dot(texColor.rgb, vec3(0.299, 0.587, 0.114));

        // Calculate the dither threshold using the blue noise texture
        vec2 noiseCoords = screen_coords / vec2(love_ScreenSize.xy);
        float noiseValue = Texel(blueNoiseTexture, noiseCoords).r;
        float threshold = noiseValue * intensity;

        // Apply dithering
        float dithered = (gray + threshold) / (1.0 + intensity);

        // Define the ZX Spectrum color palette
        vec3 palette[16];
        palette[0] = vec3(0.0, 0.0, 0.0);        // Black
        palette[1] = vec3(0.0, 0.0, 1.0);        // Blue
        palette[2] = vec3(1.0, 0.0, 0.0);        // Red
        palette[3] = vec3(1.0, 0.0, 1.0);        // Magenta
        palette[4] = vec3(0.0, 1.0, 0.0);        // Green
        palette[5] = vec3(0.0, 1.0, 1.0);        // Cyan
        palette[6] = vec3(1.0, 1.0, 0.0);        // Yellow
        palette[7] = vec3(1.0, 1.0, 1.0);        // White
        palette[8] = vec3(0.5, 0.5, 0.5);        // Bright Black (Gray)
        palette[9] = vec3(0.0, 0.0, 0.5);        // Bright Blue
        palette[10] = vec3(0.5, 0.0, 0.0);       // Bright Red
        palette[11] = vec3(0.5, 0.0, 0.5);       // Bright Magenta
        palette[12] = vec3(0.0, 0.5, 0.0);       // Bright Green
        palette[13] = vec3(0.0, 0.5, 0.5);       // Bright Cyan
        palette[14] = vec3(0.5, 0.5, 0.0);       // Bright Yellow
        palette[15] = vec3(0.75, 0.75, 0.75);    // Bright White

        // Map the dithered grayscale value to the corresponding palette index
        int index = int(dithered * 15.0);
        vec3 finalColor = palette[index];

        // Output the final color with alpha from the original texture
        return vec4(finalColor, texColor.a);
    }
]])
