warpshader = love.graphics.newShader([[
#define P 0.051 // derivative precision
#define SIZE vec2(24.0, 16.0) // function size/precision
#define WATER vec4(0.9, 0.9, 1.0, 1.0) // water color (blue)

// Function to calculate the wave height at given coordinates
float func(vec2 coords, float offset) {
    return (sin(coords.x - offset) * sin(coords.y + offset) + 1.0) / 10.0;
}

// Function to calculate the x-derivative of the wave height function
float xDerivative(vec2 coords, float offset) {
    return (func(coords + vec2(P, 0.0), offset) - func(coords - vec2(P, 0.0), offset)) / (2.0 * P);
}

// Function to calculate the y-derivative of the wave height function
float yDerivative(vec2 coords, float offset) {
    return (func(coords + vec2(0.0, P), offset) - func(coords - vec2(0.0, P), offset)) / (2.0 * P);
}

uniform float time;

vec4 effect(vec4 color, Image image, vec2 texCoords, vec2 scrCoords) {
    float waveHeight = func(texCoords * SIZE, time);

    float dx = xDerivative(texCoords * SIZE, time);
    float dy = yDerivative(texCoords * SIZE, time);

    vec2 offset = vec2(dx, dy) * waveHeight;

    return Texel(image, texCoords + offset) * WATER;
}

]])
