vectorNormalsShader = love.graphics.newShader([[
    // Vertex Shader
    vec4 position(mat4 transform_projection, vec4 vertex_position) {
        return transform_projection * vertex_position;
    }
]], [[
    // Fragment Shader
    uniform sampler2D u_texture;   // diffuse map
    uniform sampler2D u_normals;   // normal map

    uniform vec2 Resolution;       // resolution of screen
    uniform vec3 LightPos1;        // light position, normalized
    uniform vec4 LightColor1;      // light RGBA -- alpha is intensity
    uniform vec3 LightPos2;        // second light position, normalized
    uniform vec4 LightColor2;      // second light RGBA -- alpha is intensity
    uniform vec4 AmbientColor;     // ambient RGBA -- alpha is intensity 
    uniform vec3 Falloff;          // attenuation coefficients

    vec4 effect(vec4 vColor, Image texture, vec2 vTexCoord, vec2 pixcoord) {
        vec4 DiffuseColor = Texel(texture, vTexCoord);
        vec3 NormalMap = Texel(u_normals, vTexCoord).rgb;
        
        // First light calculations
        vec3 LightDir1 = vec3(LightPos1.xy - (pixcoord.xy / Resolution.xy), LightPos1.z);
        LightDir1.x *= Resolution.x / Resolution.y;
        float D1 = length(LightDir1);
        vec3 N = normalize(NormalMap * 2.0 - 1.0);
        vec3 L1 = normalize(LightDir1);
        vec3 Diffuse1 = (LightColor1.rgb * LightColor1.a) * max(dot(N, L1), 0.0);
        
        // Second light calculations
        vec3 LightDir2 = vec3(LightPos2.xy - (pixcoord.xy / Resolution.xy), LightPos2.z);
        LightDir2.x *= Resolution.x / Resolution.y;
        float D2 = length(LightDir2);
        vec3 L2 = normalize(LightDir2);
        vec3 Diffuse2 = (LightColor2.rgb * LightColor2.a) * max(dot(N, L2), 0.0);
        
        vec3 Ambient = AmbientColor.rgb * AmbientColor.a;
        float Attenuation1 = 1.0 / (Falloff.x + (Falloff.y * D1) + (Falloff.z * D1 * D1));
        float Attenuation2 = 1.0 / (Falloff.x + (Falloff.y * D2) + (Falloff.z * D2 * D2));
        
        vec3 Intensity = Ambient + Diffuse1 * Attenuation1 + Diffuse2 * Attenuation2;
        vec3 FinalColor = DiffuseColor.rgb * Intensity;
        return vColor * vec4(FinalColor, DiffuseColor.a);
    }
]])
