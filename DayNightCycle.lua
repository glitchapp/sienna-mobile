--[[ Day night cycle algorithm

License:

 Copyright (C) 2024  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


License GPL-3.0.

--]]



function loadDayNightCycle()

sunAngle = 0.09
timeOfDay = 0
daynighttimer=80
dayDuration = 7200  -- Adjust to match the planet's day length in seconds
dayDurationSeconds = 7200  -- Adjust to match the planet's day length in seconds

sunriseDuration = 100 -- -- Define sunriseDuration (in seconds) and Adjust it


-- Define variables for temperature and temperature oscillation range
baseTemperature = 20 -- Base temperature
temperatureRange = 10 -- Oscillation range

	--vectorNormalsNoiseShader:send("LightPos", {0.1, 0.1, 0.075})  -- Set light position in top-left
	vectorNormalsNoiseShader:send("LightPos1", {0.1, 0.1, 0.075})  -- Set light position in top-left
    vectorNormalsNoiseShader:send("AmbientColor", {1.0, 1.0, 1.0, 0.2})  -- Example ambient color
    vectorNormalsNoiseShader:send("Falloff", {1.0, 0.1, 0.01})  -- Adjust these values for attenuation

red, green, blue, sunIntensity = 1,1,1, 1


end

function updateDayNightCycle(dt)

if daynighttimer<120 then
	daynighttimer=daynighttimer +dt
elseif daynighttimer>119 then
	daynighttimer=80
end



-- Calculate the time of day (0 to 1)
timeOfDay = daynighttimer / dayDuration
timeOfDay = math.fmod(timeOfDay, 1)  -- Wrap around for a continuous day/night cycle

-- Calculate the sun's position based on time of day
sunAngle = math.pi * 2 * timeOfDay



 -- Calculate sun intensity
    if timeOfDay < sunriseDuration / dayDurationSeconds then
        -- Sunrise transition (0 to sunriseDuration)
        local t = timeOfDay / (sunriseDuration / dayDurationSeconds)
        sunIntensity = math.sin(t * math.pi / 2)  -- Linearly interpolate from 0 to 1
    elseif timeOfDay > (1 - sunriseDuration / dayDurationSeconds) then
        -- Sunset transition (1 to 1 - sunriseDuration)
        local t = (timeOfDay - (1 - sunriseDuration / dayDurationSeconds)) / (sunriseDuration / dayDurationSeconds)
        sunIntensity = math.sin((1 - t) * math.pi / 2)  -- Linearly interpolate from 1 to 0
    else
        -- Daytime (sun at full intensity)
        sunIntensity = 1
    end
    

 -- Calculate the temperature based on daylight
  temperature = baseTemperature + math.sin(sunAngle) * temperatureRange

  -- Update the temperature display
  temperatureText = string.format("Temp: %.2f°C", temperature)
  

transitionFactor = 0.1  -- Adjust this value for the speed of the transition

    -- Color transition logic
    local t = timeOfDay / (sunriseDuration / dayDuration)  -- Normalize t for transitions


		  if timeOfDay < sunriseDuration / dayDuration then
        -- Simulate daytime to dusk transition
        red = math.min(1, 1 - t * 0.3)  -- Slightly decrease red
        green = math.min(1, 1 - t * 0.1)  -- Slight decrease in green
        blue = math.min(1, 1)  -- Keep blue at full intensity
    elseif timeOfDay >= (1 - sunriseDuration / dayDuration) then
        -- Simulate unique night to dawn transition
        local t = (timeOfDay - (1 - sunriseDuration / dayDuration)) / (sunriseDuration / dayDuration)
        red = math.min(1, 0.8 + t * 0.2)  -- Increase red slightly during dawn
        green = math.min(1, 0.8 + t * 0.2)  -- Increase green slightly during dawn
        blue = math.min(1, 0.8 + t * 0.2)  -- Increase blue slightly during dawn
    else
        -- Simulate nighttime
        red = 0.3  -- Light dark red
        green = 0.3  -- Light dark green
        blue = 0.4  -- Light dark blue
    end
		   --]]

end



