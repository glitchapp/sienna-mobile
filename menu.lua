local lg = love.graphics

Menu = {}
Menu.__index = Menu

function Menu.create(names, functions, escape_function)
	local self = {}
	setmetatable(self, Menu)

	self.names = names
	self.functions = functions
	self.length = #names
	self.escape_function = escape_function

	self.width = 0
	for i,v in ipairs(names) do
		local w = fontBold:getWidth("* "..v.." *")
		self.width = math.max(self.width, w)
	end

	self.height = 16*self.length-9

	self.selected = 1

	return self
end

function Menu:keypressed(k)
	if k == "down" then
		self.selected = self.selected + 1
		love.audio.play(snd.Blip)
		if self.selected > self.length then
			self.selected = 1
		end
	elseif k == "up" then
		self.selected = self.selected - 1
		love.audio.play(snd.Blip)
		if self.selected == 0 then
			self.selected = self.length
		end
	elseif k == "return" then
		love.audio.play(snd.Blip)
		if self.functions[self.selected] then
			self.functions[self.selected](self)
		end
	elseif k == "escape" then
		love.audio.play(snd.Blip)
		self.escape_function(self)
	end
end

function Menu:draw()
	local top = (HEIGHT-self.height)/2

	lg.setColor(COLORS.menu)
	if retroShader == true then
				local pixelSize = 1.0  -- Example pixel size for strong pixelation
				blueNoiseZxDitherShader:send("pixelSize", pixelSize)
	else
		lg.rectangle("fill",(WIDTH-self.width)/2-6, top-6, self.width+12, self.height+12)
		lg.setColor(1,1,1,1)
	end


	lg.setFont(fontBold)
	for i=1,self.length do
		if i == self.selected then
			lg.printf("* "..self.names[i].." *", 0, top+(i-1)*16, WIDTH, "center")
		else
			lg.printf(self.names[i], 0, top+(i-1)*16, WIDTH, "center")
		end
	end
end

local host = require('hostinfo')

function createMenus()
	-- INGAME MENU
	local menuItems = {"CONTINUE", "RESTART LEVEL", "OPTIONS", "EXIT LEVEL", "QUIT GAME"}

	if host.isTouchDevice() then
		table.remove(menuItems, #menuItems)
	end

	ingame_menu = Menu.create(
		menuItems,
		{function() gamestate = STATE_INGAME end,
		 function() reloadMap() gamestate = STATE_INGAME end,
		 function() current_menu = options_menu end,
		 function() gamestate = STATE_LEVEL_MENU end,
		 function() love.event.quit() end},

		 function() gamestate = STATE_INGAME end
	)

	-- OPTIONS MENU
	options_menu = Menu.create(
		{"SCALE: X", "ASSETS: new", "RETRO MODE:","CAMERA SPEED: |=======|",
		"MUSIC VOLUME: |==========|", "SOUND VOLUME: |==========|", "BACK"},
		{nil,nil,nil,nil,
		function() if gamestate == STATE_INGAME_MENU then current_menu = ingame_menu
			else current_menu = main_menu end end},

		function() if gamestate == STATE_INGAME_MENU then current_menu = ingame_menu
			else current_menu = main_menu end end
	)

	function options_menu:update()
		self.names = {
			"SCALE: ".. SCALE,
			"ASSETS: ".. ASSETS,
			"RETRO MODE: ",
			"CAMERA SPEED: |"..string.rep("=",SCROLL_SPEED-2)..string.rep("-",9-SCROLL_SPEED).."|",
			"MUSIC VOLUME: |"..string.rep("=",music_volume*10)..string.rep("-",10-music_volume*10).."|",
			"SOUND VOLUME: |"..string.rep("=",sound_volume*10)..string.rep("-",10-sound_volume*10).."|",
			"BACK"
		}
	end
	options_menu:update()
	
	options_menu.metakeypressed = options_menu.keypressed
	function options_menu:keypressed(k)
		if k == "left" then
			if self.selected == 1 then
				setScale(SCALE-1)
				love.audio.play(snd.Blip)
			elseif self.selected == 2 then
					if ASSETS == "new" then ASSETS = "classic"
				elseif ASSETS == "classic" then ASSETS = "new"
				end
				--loadImages()
				----print(ASSETS)
				elseif self.selected == 3 then
					if ASSETS == "new" then ASSETS = "classic"
				elseif ASSETS == "classic" then ASSETS = "classic"
				end
				loadImages()
					if retroShader == true then retroShader = false
				elseif retroShader == false then retroShader = true
				end
			elseif self.selected == 4 then
				if SCROLL_SPEED > 3 then
					SCROLL_SPEED = SCROLL_SPEED - 1
					love.audio.play(snd.Blip)
				else
					love.audio.play(snd.Blip2)
				end
			elseif self.selected == 5 then
				if music_volume >= 0.1 then
					music_volume = music_volume - 0.1
					updateVolumes()
					love.audio.play(snd.Blip)
				else
					love.audio.play(snd.Blip2)
				end
			elseif self.selected == 6 then
				if sound_volume >= 0.1 then
					sound_volume = sound_volume - 0.1
					updateVolumes()
					love.audio.play(snd.Blip)
				else
					love.audio.play(snd.Blip2)
				end
			end
			self:update()
		elseif k == "right" then
			if self.selected == 1 then
				setScale(SCALE+1)
				love.audio.play(snd.Blip)
			elseif self.selected == 2 then
					if ASSETS == "new" then ASSETS = "classic"
				elseif ASSETS == "classic" then ASSETS = "new"
				end
				loadImages()
			elseif self.selected == 3 then
					if ASSETS == "new" then ASSETS = "classic"
				elseif ASSETS == "classic" then ASSETS = "classic"
				end
				loadImages()
					if retroShader == true then retroShader = false
				elseif retroShader == false then retroShader = true
				end
				--print(ASSETS)
			elseif self.selected == 4 then
				if SCROLL_SPEED < 9 then
					SCROLL_SPEED = SCROLL_SPEED + 1
					love.audio.play(snd.Blip)
				else
					love.audio.play(snd.Blip2)
				end
			elseif self.selected == 5 then
				if music_volume <= 0.9 then
					music_volume = music_volume + 0.1
					updateVolumes()
					love.audio.play(snd.Blip)
				else
					love.audio.play(snd.Blip2)
				end
			elseif self.selected == 6 then
				if sound_volume <= 0.9 then
					sound_volume = sound_volume + 0.1
					updateVolumes()
					love.audio.play(snd.Blip)
				else
					love.audio.play(snd.Blip2)
				end
			end
			self:update()
		elseif k == "return" then
		 -- Check if the selected option is the "BACK" option
        if self.selected == self.length then
            -- Handle action for the "BACK" option
            if gamestate == STATE_INGAME_MENU then
                current_menu = ingame_menu
            else
                current_menu = main_menu
            end
        else
            -- Handle action for other options
            if self.functions[self.selected] then
                self.functions[self.selected](self)
            end
        end
		
		else
			self:metakeypressed(k)
		end
	end

	-- MAIN MENU
	--local menuItems = {"START GAME", "OPTIONS", "JUKEBOX", "STATS", "CREDITS", "QUIT GAME"}
	local menuItems = {"START GAME", "MULTIPLAYER", "OPTIONS", "STATS", "CREDITS", "QUIT GAME"}

	if host.isTouchDevice() then
		table.remove(menuItems, #menuItems)
	end

	main_menu = Menu.create(
		menuItems,
		{function() gamestate = STATE_LEVEL_MENU end,
		 function() gamestate = STATE_LEVEL_MENU twoplayersMode = true noobHubEnabled = true end,
		 function() current_menu = options_menu end,
		 --function() current_menu = jukebox_menu end,
		 function() current_menu = stats_menu end,
		 function() current_menu = credits_menu 
		 
		   if snd.Music and love.audio.getSourceCount() > 0 then
				love.audio.stop(snd.Music)
				print("Stopped previous music")
			end
        print("Stopped previous music")
        --print(unlocked)
        --if unlocked<9 then
			snd.Music = love.audio.newSource("sfx/a_path_c4_and_delayedChip_MIX_Extra_layer_ULTRALARGE.ogg", "stream")
		--else
			--snd.Music = love.audio.newSource("sfx/push_ahead_vocals.ogg", "stream")
		--end
        updateVolumes()
		love.audio.play(snd.Music)
		 end,
		 function() love.event.quit() end},

		 function() love.event.quit() end
	)

	function main_menu:draw()
		lg.setColor(1,1,1,1)
		for i=1,self.length do
			if i == self.selected then
				lg.printf("* "..self.names[i].." *", 123, 59+(i-1)*12, 166, "center")
			else
				lg.printf(self.names[i], 123, 59+(i-1)*12, 166, "center")
			end
			if self.selected == 2 then
				lg.printf("JOIN EXTRA PLAYERS ANYTIME WITH KEYS 'X','0' AND 'RIGHT SHIFT'", 123, 59+(2-1)*12*6, 166, "center")
				lg.printf("THE GAME WILL SEARCH FOR NOOBUHB SERVERS AND TRY TO CONNECT", 123, 59+(2-1)*12*9, 166, "center")
			end
		end
	end

	--[[-- credits menu
	credits_menu = Menu.create(
		{"GRAPHICS AND PROGRAMMING","SIMON LARSEN","TITLE SCREEN",
		 "LUKAS HANSEN","MUSIC","RUGAR - A SCENT OF EUROPE","SEE LICENCE.TXT FOR MORE INFO", "MOBILE PORT", "GLITCHAPP",},
		nil,nil
	)--]]
	-- credits menu
credits_menu = Menu.create(
    {
        "GRAPHICS AND PROGRAMMING",
        "SIMON LARSEN",
        "",
        "",
        "TITLE SCREEN",
        "LUKAS HANSEN",
        "",
        "",
        "MUSIC BY RUGAR:",
        "",
        "A SCENT OF EUROPE",
        "SEE LICENCE.TXT FOR MORE INFO",
        "",
        "",
        "MUSIC TRACKS BY CENTURION OF WAR:",
         "",
		"PUSH AHEAD",
		"UPBEAT BEAT",
		"UNSEEN",
		"MIDNIGHT RUSH",
		"A WISH TO FULFILL",
		"ON YOUR TOES",
		"A PATH WHICH LEADS TO SOMEWHERE",
		"HAIL THE ARBITER",
		"I AM NOT TRASH, OR AM I?",
        "",
        "",
        "MOBILE PORT",
        "GLITCHAPP",
        "",
        "",
        
      },
    nil,
    nil
)

    
local scroll_speed = 8 -- Adjust the scrolling speed as needed
local credits_scroll = 0 -- Variable to track the scroll position



-- Function to handle scrolling
function credits_menu:update(dt)
    -- Scroll down gradually
    credits_scroll = credits_scroll + scroll_speed * dt

    -- If the scroll position exceeds the total length of the credits content,
    -- reset it to keep scrolling continuously
    if credits_scroll > self.height then
        credits_scroll = 0
    end
    
    if current_menu == jukebox_menu then
		MusicPlayerUpdate(dt)
    end

    --print(credits_scroll)
end




function credits_menu:draw()
    local top = (HEIGHT - self.height) / 2 - 50
    local visible_lines = math.floor((HEIGHT - 16) / 18) -- Recalculate visible lines here

    --local visible_lines = math.floor((HEIGHT - 16) / 18) -- Number of lines visible on the screen

    lg.setColor(COLORS.menu)
    lg.rectangle("fill", (WIDTH - self.width) / 2 - 6, top - 6, self.width + 12, self.height + 12)
    lg.setColor(1, 1, 1, 1)

    lg.setFont(fontBold)
    local offset = top + 250 - credits_scroll

  -- Adjust offset based on scroll position
    local start_line = math.max(1, 1 + credits_scroll)
    local end_line = math.min(self.length, start_line + visible_lines - 1)

    for i=1,self.length do
		if self.names[i] then -- Check if the name exists
			lg.printf(self.names[i], 0, offset, WIDTH, "center")
        end
        offset = offset + 18 -- Increase offset by 18 to leave space between lines
    end

	
    
end

	function credits_menu:keypressed(k)
		love.audio.play(snd.Blip)
		current_menu = main_menu
		
				   if snd.Music and love.audio.getSourceCount() > 0 then
				love.audio.stop(snd.Music)
				print("Stopped previous music")
			end
        print("Stopped previous music")
        snd.Music = love.audio.newSource("sfx/rugar-a_scent_of_europe.ogg", "stream")
        snd.Music:addTags("music")
		snd.Music:setLooping(true)
		print("Loaded new music")
        updateVolumes()
		love.audio.play(snd.Music)
	end
	
		-- jukebox menu
	--[[jukebox_menu = Menu.create(
		{"TOTAL DEATHS: XXX", "TOTAL JUMPS: XXXX", "COINS COLLECTED: 00/45"},
		nil, nil
	)
	--]]
--[[	function jukebox_menu:draw()
		local top = (HEIGHT-self.height)/2


		lg.setColor(COLORS.menu)
		lg.rectangle("fill",(WIDTH-self.width)/2-6, top-6, self.width+12, self.height+12)
		lg.setColor(1,1,1,1)

		lg.setFont(fontBold)
	
	end
	--[[
	function jukebox_menu:keypressed(k)
		love.audio.play(snd.Blip)
		current_menu = main_menu
		musicmenu=false
	end
--]]
	
	-- stats menu
	stats_menu = Menu.create(
		{"TOTAL DEATHS: XXX", "TOTAL JUMPS: XXXX", "COINS COLLECTED: 00/45"},
		nil, nil
	)
	function stats_menu:draw()
		local top = (HEIGHT-self.height)/2

		lg.setColor(COLORS.menu)
		lg.rectangle("fill",(WIDTH-self.width)/2-6, top-6, self.width+12, self.height+12)
		lg.setColor(1,1,1,1)

		lg.setFont(fontBold)
		lg.printf("TOTAL DEATHS: "..deaths, 0, top, WIDTH, "center")
		lg.printf("TOTAL JUMPS: "..jumps, 0, top+16, WIDTH, "center")
		lg.printf("COINS COLLECTED: "..coins.."/45", 0, top+32, WIDTH, "center")
	end

	function stats_menu:keypressed(k)
		love.audio.play(snd.Blip)
		current_menu = main_menu
	end
end
